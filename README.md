# Javali Compiler #

Javali is a simplified subset of the Java language designed for educational purposes. The compiler was developed for the Compiler Design lecture at ETH in a team of two people. It parses the source Javali code using an ANTLR grammar and generates x86 assembly instruction that can be compiled and executed on any compatible processor.