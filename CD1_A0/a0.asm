; Simple assembly program

; Compile with:    nasm -g -f elf a0.asm
; Link with:       ld -g -m elf_i386 -o a0 a0.o

%define size 20

section .data

section .bss

section .text
	global _start
	
_start:
	sub		esp,	size+1		; Reserve space for input string
	mov		ecx,	esp
	mov		al,		'1'			; Put '1' before the beginning of the string to handle '999' case
	mov		[esp],	al
	inc 	ecx					; Exclude '1' from the string
	mov 	eax, 	3			; UNIX sys_read call
	mov		ebx,	0			; Reading from stdin
	mov		edx,	size		; At most 'size' characters
	int		80h					; Interrupt to the kernel
	
	mov		esi,	ecx			; Loop variable
	add		esi,	size-1
	jmp 	loop				; Enter loop

carry:
	mov		al,		'0'			; Incrementing '9' we get the ASCII code for a special character.
	mov		[esi],	al			; so write '0' manually
	cmp		esi,	ecx			; If we have a carry on the first digit then we have to add an extra '1'
	jne		empty
	dec		ecx					; Modify the buffer to include the extra '1'
	inc		edx					; And increment by one the buffer size
	jmp		print
	
empty:
	cmp		esi,	ecx			 
	je		print				; We parsed the whole string, go and print the result
	dec		esi					; Reserved space that doesn't contain the number: just skip it
	
loop:		
	mov 	al,		[esi]		; Copy one character
	cmp 	al,		'9'
	je		carry				; If the c == '9' then we have a special case		
	jg		empty				; If c > '9' then c doesn't contain a number
	cmp 	al,		'0'			
	jl		empty				; If c < '0' then c doesn't contain a number
	
	inc		al					; Otherwise just increment the number
	mov		[esi],	al			; And copy it back in memory

print:
	mov		eax,	4			; UNIX sys_write call	
	mov		ebx,	1			; Write to stdout
	int 	80h					; Interrupt to the kernel
	
	mov		eax,	1			; Exit syscall number
	mov		ebx,	0			; Exit code 0
	int		80h					; System call
	
; Linux syscalls:
; http://docs.cs.up.ac.za/programming/asm/derick_tut/syscalls.html	
