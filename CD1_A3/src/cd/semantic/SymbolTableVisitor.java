package cd.semantic;

import cd.exceptions.SemanticFailure;
import cd.ir.Ast;
import cd.ir.Ast.MethodDecl;
import cd.ir.Ast.VarDecl;
import cd.ir.AstVisitor;
import cd.ir.ClassSymbolTable;
import cd.ir.Symbol;
import cd.ir.Ast.ClassDecl;
import cd.ir.Symbol.ClassSymbol;
import cd.ir.Symbol.MethodSymbol;
import cd.ir.Symbol.TypeSymbol;
import cd.ir.Symbol.VariableSymbol;
import cd.ir.Symbol.VariableSymbol.Kind;
import cd.ir.SymbolTable;

public class SymbolTableVisitor extends AstVisitor<Symbol, SymbolTable> {

	private final SymbolTable global;

	public SymbolTableVisitor(SymbolTable global) {
		this.global = global;
	}

	public Symbol visit(Ast ast, SymbolTable symbolTable) {
		return ast.accept(this, symbolTable);
	}

	public Symbol classDecl(ClassDecl ast, SymbolTable symbolTable) {

		ClassSymbol sym = (ClassSymbol) global.lookup(ast.name);
		ClassSymbolTable classSymbolTable = (ClassSymbolTable) global
				.getTable(sym);
		// visit fields and methods
		for (VarDecl varDecl : ast.fields()) {
			visit(varDecl, classSymbolTable);
		}

		for (MethodDecl methodDecl : ast.methods()) {
			visit(methodDecl, classSymbolTable);
		}

		return sym;
	}

	public Symbol varDecl(VarDecl ast, SymbolTable symbolTable) {
		if (symbolTable.lookup(ast.name) != null) {
			throw new SemanticFailure(SemanticFailure.Cause.DOUBLE_DECLARATION);
		}
		// Create symbol for varDecl
		TypeSymbol type = (TypeSymbol) global.lookup(ast.type);
		if (type == null) {
			throw new SemanticFailure(SemanticFailure.Cause.NO_SUCH_TYPE,
					"Not existing declaration type");
		}
		VariableSymbol sym = new VariableSymbol(ast.name, type, Kind.FIELD);

		ast.sym = sym;

		symbolTable.addEntry(ast.name, sym);
		return sym;
	}

	public Symbol methodDecl(MethodDecl ast, SymbolTable symbolTable) {

		if (((ClassSymbolTable) symbolTable).methodLookup(ast.name) != null) {
			throw new SemanticFailure(SemanticFailure.Cause.DOUBLE_DECLARATION,
					"Not unique method names in a class");
		}
		MethodSymbol sym = new MethodSymbol(ast);
		TypeSymbol retType = (TypeSymbol) global.lookup(ast.returnType);
		if (retType == null) {
			throw new SemanticFailure(SemanticFailure.Cause.NO_SUCH_TYPE,
					"Not existing return type");
		}
		sym.returnType = retType;
		SymbolTable methodSymbolTable = new SymbolTable();
		((ClassSymbolTable) symbolTable).addMethodEntry(ast.name, sym);

		symbolTable.linkTable(sym, methodSymbolTable);
		for (int i = 0; i < ast.argumentTypes.size(); i++) {
			if (methodSymbolTable.lookup(ast.argumentNames.get(i)) != null) {
				throw new SemanticFailure(
						SemanticFailure.Cause.DOUBLE_DECLARATION,
						"Not unique parameters names in a method");
			}
			TypeSymbol type = (TypeSymbol) global.lookup(ast.argumentTypes
					.get(i));
			VariableSymbol varSym = new VariableSymbol(
					ast.argumentNames.get(i), type, Kind.PARAM);

			methodSymbolTable.addEntry(ast.argumentNames.get(i), varSym);
		}
		ast.decls().accept(this, methodSymbolTable);
		ast.body().accept(this, methodSymbolTable);
		((ClassSymbolTable) symbolTable).addMethodEntry(ast.name, sym);
		ast.sym = sym;

		return sym;
	}

}
