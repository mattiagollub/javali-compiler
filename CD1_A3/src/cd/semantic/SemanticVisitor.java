package cd.semantic;

import cd.exceptions.SemanticFailure;
import cd.exceptions.SemanticFailure.Cause;
import cd.ir.Ast;
import cd.ir.Ast.BooleanConst;
import cd.ir.Ast.BuiltInReadFloat;
import cd.ir.Ast.BuiltInWriteln;
import cd.ir.Ast.Cast;
import cd.ir.Ast.ClassDecl;
import cd.ir.Ast.Field;
import cd.ir.Ast.FloatConst;
import cd.ir.Ast.IfElse;
import cd.ir.Ast.Index;
import cd.ir.Ast.IntConst;
import cd.ir.Ast.MethodCall;
import cd.ir.Ast.MethodCallExpr;
import cd.ir.Ast.MethodDecl;
import cd.ir.Ast.NewArray;
import cd.ir.Ast.NewObject;
import cd.ir.Ast.Nop;
import cd.ir.Ast.NullConst;
import cd.ir.Ast.ReturnStmt;
import cd.ir.Ast.Seq;
import cd.ir.Ast.ThisRef;
import cd.ir.Ast.UnaryOp;
import cd.ir.Ast.Var;
import cd.ir.Ast.VarDecl;
import cd.ir.Ast.BinaryOp.BOp;
import cd.ir.Ast.UnaryOp.UOp;
import cd.ir.ClassSymbolTable;
import cd.ir.Symbol;
import cd.ir.Symbol.ArrayTypeSymbol;
import cd.ir.Symbol.ClassSymbol;
import cd.ir.AstVisitor;
import cd.ir.Symbol.MethodSymbol;
import cd.ir.Symbol.TypeSymbol;
import cd.ir.Symbol.VariableSymbol;
import cd.ir.SymbolTable;

class SemanticArgs {
	public ClassSymbolTable classTable;
	public ClassSymbol classSymbol;
	public SymbolTable methodTable;
	public TypeSymbol returnType;
}

public class SemanticVisitor extends AstVisitor<TypeSymbol, SemanticArgs> {
	
	protected SymbolTable gst;
	
	public void check(ClassDecl cd, SymbolTable globalST) {
		gst = globalST;
		SemanticArgs args = new SemanticArgs();
		args.classTable = (ClassSymbolTable)gst.getTable(cd.sym);
		args.classSymbol = cd.sym;
		args.methodTable = null;
		args.returnType = null;
		
		checkCircularInheritance(cd);
		
		visit(cd, args);
		
	}
	
	@Override
	public TypeSymbol var(Var ast, SemanticArgs arg) {
		
		ClassSymbol superClass = arg.classSymbol;
		VariableSymbol varSym = null;
		
		// Check in method scope
		varSym = (VariableSymbol)arg.methodTable.lookup(ast.name);
		
		// Check in class/superclass scope
		while (superClass != null && varSym == null) {
			ClassSymbolTable superTable = (ClassSymbolTable)gst.getTable(superClass);
			varSym = (VariableSymbol)superTable.lookup(ast.name);
			
			superClass = superClass.superClass;
		}
		
		if (varSym == null)
			throw new SemanticFailure(Cause.NO_SUCH_VARIABLE, "Variable '%s' not defined in current scope.", ast.name);
		
		return varSym.type;
	}
	
	@Override
	public TypeSymbol varDecl(VarDecl ast, SemanticArgs arg) {
		return (TypeSymbol)gst.lookup("null");
	}
	
	@Override
	public TypeSymbol newObject(NewObject ast, SemanticArgs arg) {
		ClassSymbol createType = (ClassSymbol) gst.lookup(ast.typeName);
		
		if (createType == null)
			throw new SemanticFailure(Cause.NO_SUCH_TYPE, "Cannot instantiate undefined type '%s'.", ast.typeName);
		
		return createType;
	
	}
	
	@Override
	public TypeSymbol newArray(NewArray ast, SemanticArgs arg) {
		TypeSymbol sizeType = visit(ast.arg(), arg);
		TypeSymbol createType = (TypeSymbol)gst.lookup(ast.typeName);
		
		// Check that the element type exists
		if (createType == null)
			throw new SemanticFailure(Cause.NO_SUCH_TYPE, "Cannot instantiate undefined type '%s'.", createType.name);
		
		// Check size type
		if (!sizeType.equals("int"))
			throw new SemanticFailure(Cause.TYPE_ERROR, "Array size must be of type 'int' but found '%s'.", sizeType.name);
		
		return createType;
	}
	
	@Override
	public TypeSymbol index(Index ast, SemanticArgs arg) {
		TypeSymbol receiverType = visit(ast.left(), arg);
		TypeSymbol indexType = visit(ast.right(), arg);
		
		// Check that that we are working on an array
		if (!(receiverType instanceof ArrayTypeSymbol))
			throw new SemanticFailure(Cause.TYPE_ERROR, "Unable to dereference object of type '%s'.", receiverType.name);
	
		// Check index type
		if (!indexType.equals("int"))
			throw new SemanticFailure(Cause.TYPE_ERROR, "Array index must be of type 'int' but found '%s'.", indexType.name);
		
		return ((ArrayTypeSymbol)receiverType).elementType;
	}
	
	@Override
	public TypeSymbol methodDecl(Ast.MethodDecl ast, SemanticArgs arg) {
		
		arg.methodTable = arg.classTable.getTable(ast.sym);
	
		// Check for coherence with the superclass
		ClassSymbol superSymbol = arg.classSymbol.superClass;
		
		if (superSymbol != null) {
			ClassSymbolTable superTable = (ClassSymbolTable)gst.getTable(superSymbol);
			MethodSymbol superMethod = (MethodSymbol)superTable.methodLookup(ast.name);
			
			if (superMethod != null) {
				// Check that the methods have the same number of arguments.
				if (superMethod.ast.argumentNames.size() != ast.argumentNames.size())
					throw new SemanticFailure(Cause.INVALID_OVERRIDE, "Invalid override of method '%s' in class '%s'.", ast.name, arg.classSymbol.name);
				
				// Check that the arguments have the same type
				for (int i = 0; i < ast.argumentTypes.size(); i++) {
					if (superMethod.ast.argumentTypes.get(i).contentEquals(ast.argumentTypes.get(i)))
						throw new SemanticFailure(Cause.INVALID_OVERRIDE, "Incompatible argument types in override of method '%s'.", ast.name); 
				};
				
				// Check for equal return type
				if (!superMethod.ast.returnType.contentEquals(ast.returnType))
					throw new SemanticFailure(Cause.INVALID_OVERRIDE, "Incompatible return types in override of method '%s'.", ast.name);
			}
		}
		
		// Check for parameters with the same name
		for (String name : ast.argumentNames) {
			if (ast.argumentNames.indexOf(name) != ast.argumentNames.lastIndexOf(name)) {
				throw new SemanticFailure(Cause.DOUBLE_DECLARATION, "Argument %s declared twice in method %s.", name, ast.name);
			}
		}
		
		if (ast.sym.returnType.equals("void"))
			arg.returnType = null;
		else
			arg.returnType = ast.sym.returnType;
		
		visitChildren(ast, arg);
		
		// Verify that all paths return a value.
		if (arg.returnType != null) {
			if (!atLeastOneReturns(ast.body()))
				throw new SemanticFailure(Cause.MISSING_RETURN, "Not all paths return a value in method '%s'.", ast.name);
		}
		
		return (TypeSymbol)gst.lookup("void");
	}
	
	@Override
	public TypeSymbol returnStmt(ReturnStmt ast, SemanticArgs arg) {
		
		if (arg.returnType == null) {
			if (ast.arg() != null)
				throw new SemanticFailure(Cause.TYPE_ERROR, "Method of type 'void' cannot return an expression.");
		}
		else {
			if (ast.arg() == null)
				throw new SemanticFailure(Cause.TYPE_ERROR, "Method of type '%s' cannot return void.", arg.returnType.name);
			
			TypeSymbol returnType = visit(ast.arg(), arg);
			
			if ( !returnType.isSubtypeOf(arg.returnType))
				if (ast.arg() != null)
					throw new SemanticFailure(Cause.TYPE_ERROR, "Method of type '%s' cannot return an expression of type '%s'.", arg.returnType.name, returnType.name);
		}
		
		ast.setReturns(true);
		
		return (TypeSymbol)gst.lookup("void");
	}
	
	@Override
	public TypeSymbol thisRef(ThisRef ast, SemanticArgs arg) {
		return arg.classSymbol;
	}
	
	@Override
	public TypeSymbol methodCall(MethodCall ast, SemanticArgs arg) {
		TypeSymbol receiverType = visit(ast.receiver(), arg);
		
		//Check that the receiver is a reference type
		if (!(receiverType instanceof ClassSymbol))
			throw new SemanticFailure(Cause.TYPE_ERROR, "'%s' is not a method of '%s'.", ast.methodName, receiverType.name);
		
		// Check that the method exists
		MethodSymbol method = null;
		ClassSymbol receiverClass = (ClassSymbol)receiverType;
		
		while (receiverClass!= null && method == null) {
			ClassSymbolTable recvTable = (ClassSymbolTable)gst.getTable(receiverClass);
			method = (MethodSymbol)recvTable.methodLookup(ast.methodName);
			
			receiverClass = receiverClass.superClass;
		}
		
		// Check that the method exists
		if (method == null)
			throw new SemanticFailure(Cause.NO_SUCH_METHOD, "'%s' is not a method of '%s'.", ast.methodName, receiverType.name);
		
		int nDecl = method.ast.argumentNames.size();
		int nCall = ast.argumentsWithoutReceiver().size();
		
		// Check number of arguments
		if (nDecl != nCall)
			throw new SemanticFailure(Cause.WRONG_NUMBER_OF_ARGUMENTS, "Trying to call method '%s' with wrong arguments.", ast.methodName);
	
		String[] declTypes = method.ast.argumentTypes.toArray(new String[nDecl]);
		Ast.Expr[] callTypes = ast.argumentsWithoutReceiver().toArray(new Ast.Expr[nDecl]);
		
		// Check argument types
		for (int i = 0; i < method.ast.argumentNames.size(); i++) {
			TypeSymbol callType = visit(callTypes[i], arg);
			if (!callType.isSubtypeOf(declTypes[i]))
				throw new SemanticFailure(Cause.TYPE_ERROR, "Incompatible types ('%s', '%s') in call to method '%s'.", declTypes[i], callType.name, ast.methodName);
		}
		
		return (TypeSymbol)gst.lookup("void");
	}
	
	@Override
	public TypeSymbol methodCall(MethodCallExpr ast, SemanticArgs arg) {
		TypeSymbol receiverType = visit(ast.receiver(), arg);
		
		//Check that the receiver is a reference type
		if (!(receiverType instanceof ClassSymbol))
			throw new SemanticFailure(Cause.TYPE_ERROR, "'%s' is not a method of '%s'.", ast.methodName, receiverType.name);
		
		// Check that the method exists
		MethodSymbol method = null;
		ClassSymbol receiverClass = (ClassSymbol)receiverType;
		
		while (receiverClass!= null && method == null) {
			ClassSymbolTable recvTable = (ClassSymbolTable)gst.getTable(receiverClass);
			method = (MethodSymbol)recvTable.methodLookup(ast.methodName);
			receiverClass = receiverClass.superClass;
		}
		
		// Check that the method exists
		if (method == null)
			throw new SemanticFailure(Cause.NO_SUCH_METHOD, "'%s' is not a method of '%s'.", ast.methodName, receiverType.name);
		
		int nDecl = method.ast.argumentNames.size();
		int nCall = ast.argumentsWithoutReceiver().size();
		
		// Check number of arguments
		if (nDecl != nCall)
			throw new SemanticFailure(Cause.WRONG_NUMBER_OF_ARGUMENTS, "Trying to call method '%s' with wrong arguments.", ast.methodName);
	
		String[] declTypes = method.ast.argumentTypes.toArray(new String[nDecl]);
		Ast.Expr[] callTypes = ast.argumentsWithoutReceiver().toArray(new Ast.Expr[nDecl]);
		
		// Check argument types
		for (int i = 0; i < method.ast.argumentNames.size(); i++) {
			TypeSymbol callType = visit(callTypes[i], arg);
			if (!callType.isSubtypeOf(declTypes[i]))
				throw new SemanticFailure(Cause.TYPE_ERROR, "Incompatible types ('%s', '%s') in call to method '%s'.", declTypes[i], callType.name, ast.methodName);
		}
		
		return method.returnType;
	}
	
	@Override
	public TypeSymbol field(Field ast, SemanticArgs arg) {
		TypeSymbol receiverType = visit(ast.arg(), arg);
		
		// Check that the receiver is a reference type
		if (!(receiverType instanceof ClassSymbol))
			throw new SemanticFailure(Cause.TYPE_ERROR, "'%s' is not a field of '%s'.", ast.fieldName, receiverType.name);

		// Check that the field exists
		VariableSymbol varSym = null;
		ClassSymbol receiverClass = (ClassSymbol)receiverType;
		
		while (receiverClass!= null && varSym == null) {
			ClassSymbolTable recvTable = (ClassSymbolTable)gst.getTable(receiverClass);
			varSym = (VariableSymbol)recvTable.lookup(ast.fieldName);
			
			receiverClass = receiverClass.superClass;
		}
		
		if (varSym == null)
			throw new SemanticFailure(Cause.NO_SUCH_FIELD, "'%s' is not a field of '%s'.", ast.fieldName, receiverType.name);
		
		return varSym.type;
	}
	
	@Override
	public TypeSymbol builtInWrite(Ast.BuiltInWrite ast, SemanticArgs arg) {
		
		TypeSymbol exprType = visit(ast.arg(), arg);
		
		//Check for expression of type 'int'
		if (!exprType.name.contentEquals("int"))
			throw new SemanticFailure(Cause.TYPE_ERROR,	"Function 'write' expects argument of type 'int' but found '%s'.", exprType.name);
		
		return (TypeSymbol)gst.lookup("void");
	}
	
	@Override
	public TypeSymbol builtInWriteFloat(Ast.BuiltInWriteFloat ast, SemanticArgs arg) {
		
		TypeSymbol exprType = visit(ast.arg(), arg);
		
		// Check for expression of type 'float'
		if (!exprType.equals("float"))
			throw new SemanticFailure(Cause.TYPE_ERROR, "Function 'writef' expects argument of type 'float' but found '%s'.", exprType.name);
		
		return (TypeSymbol)gst.lookup("void");
	}
	
	@Override
	public TypeSymbol builtInWriteln(BuiltInWriteln ast, SemanticArgs arg) {
		return (TypeSymbol)gst.lookup("void");
	}
	
	@Override
	public TypeSymbol builtInRead(Ast.BuiltInRead ast, SemanticArgs arg) {
		return (TypeSymbol)gst.lookup("int");
	}
	
	@Override
	public TypeSymbol builtInReadFloat(BuiltInReadFloat ast, SemanticArgs arg) {
		return (TypeSymbol)gst.lookup("float");
	}
	
	@Override
	public TypeSymbol ifElse(Ast.IfElse ast, SemanticArgs arg) {
		
		TypeSymbol exprType = visit(ast.condition(), arg);
		visit(ast.then(), arg);
		visit(ast.otherwise(), arg);
		
		// Check for expression of type boolean
		if (!exprType.equals("boolean"))
			throw new SemanticFailure(Cause.TYPE_ERROR, "The condition of an 'if' block must be of type 'boolean' but found '%s'.", exprType.name);
		
		if (arg.returnType != null && atLeastOneReturns((Seq)ast.then()) && ast.otherwise() instanceof Seq && atLeastOneReturns((Seq)ast.otherwise()))
			ast.setReturns(true);
		
		return (TypeSymbol)gst.lookup("void");
	}

	@Override
	public TypeSymbol nullConst(NullConst ast, SemanticArgs arg) {
		return (TypeSymbol)gst.lookup("null");
	}
	
	@Override
	public TypeSymbol assign(Ast.Assign ast, SemanticArgs arg) {
		
		TypeSymbol leftType = visit(ast.left(), arg);
		
		// null can be assigned to any reference type
		if (ast.right() instanceof NullConst) {
			if (!leftType.isReferenceType())
				throw new SemanticFailure(Cause.TYPE_ERROR,	"'null' cannot be assigned to an expression of type '%s'", leftType.name);
			
			return (TypeSymbol)gst.lookup("void");
		}
		
		TypeSymbol rightType = visit(ast.right(), arg);		
		
		// Check for equal types
		if ((rightType.isReferenceType() && !rightType.isSubtypeOf(leftType)) || (!rightType.isReferenceType() && !rightType.equals(leftType)))
			throw new SemanticFailure(Cause.TYPE_ERROR,	"Cannot assign an expression of type '%s' to a variable of type '%s'.", rightType.name, leftType.name);
		
		// Check for assignable expression
		if (!(ast.left() instanceof Var || ast.left() instanceof Field || ast.left() instanceof Index))
			throw new SemanticFailure(Cause.NOT_ASSIGNABLE, "Expression cannot be assigned to a LHS of type '%s'.", ast.left().getClass().getName());
		
		return (TypeSymbol)gst.lookup("void");
	}
	
	@Override
	public TypeSymbol intConst(IntConst ast, SemanticArgs arg) {
		return (TypeSymbol)gst.lookup("int");
	}
	
	@Override
	public TypeSymbol floatConst(FloatConst ast, SemanticArgs arg) {
		return (TypeSymbol)gst.lookup("float");
	}
	
	@Override
	public TypeSymbol booleanConst(BooleanConst ast, SemanticArgs arg) {
		return (TypeSymbol)gst.lookup("boolean");
	}
	
	@Override
	public TypeSymbol cast(Cast ast, SemanticArgs arg) {
		TypeSymbol fromType = visit(ast.arg(), arg);
		TypeSymbol toType = (TypeSymbol)gst.lookup(ast.typeName);
		
		// Check that the new type exists
		if (toType == null)
			throw new SemanticFailure(Cause.NO_SUCH_TYPE, "Cannot instantiate undefined type '%s'.", ast.typeName);
		
		// Check if cast type is compatible
		if (!(fromType.isSubtypeOf(toType) || toType.isSubtypeOf(fromType)))
			throw new SemanticFailure(Cause.TYPE_ERROR, "Trying to cast expression of type '%s' to incompatible type '%s'.", fromType.name, toType.name);
		
		return toType;
	}
	
	@Override
	public TypeSymbol binaryOp(Ast.BinaryOp ast, SemanticArgs arg) {
		
		TypeSymbol type;
		BOp op = ast.operator;
		
		switch(op)
		{
			case B_TIMES:
			case B_DIV:
			case B_PLUS:
			case B_MINUS:
			case B_MOD:
				type = arithmeticOp(ast, arg);
				break;
			case B_AND:
			case B_OR:
				type = booleanOp(ast, arg);
				break;
			case B_EQUAL:
			case B_NOT_EQUAL:
				type = equalityOp(ast, arg);
				break;
			case B_GREATER_OR_EQUAL:
			case B_GREATER_THAN:
			case B_LESS_THAN:
			case B_LESS_OR_EQUAL:
				type = relationalOp(ast, arg);
				break;
			default:
				type = null; // We should never arrive here
				break;
		}
		
		return type;
	}
	
	@Override
	public TypeSymbol nop(Nop ast, SemanticArgs arg) {
		return (TypeSymbol)gst.lookup("void");
	}
	
	@Override
	public TypeSymbol unaryOp(UnaryOp ast, SemanticArgs arg) {
		TypeSymbol type;
		UOp op = ast.operator;
		
		switch(op)
		{
			case U_BOOL_NOT:
				type = booleanUnaryOp(ast, arg);
				break;
			case U_PLUS:
			case U_MINUS:
				type = arithmeticUnaryOp(ast, arg);
				break;
			default:
				type = null; // We should never arrive here
				break;
		}
		
		return type;
	}
	
	public TypeSymbol arithmeticOp(Ast.BinaryOp ast, SemanticArgs arg) {
		TypeSymbol leftType = visit(ast.left(), arg);
		TypeSymbol rightType = visit(ast.right(), arg);
		
		// Check for equal type
		if (!leftType.equals(rightType))
			throw new SemanticFailure(Cause.TYPE_ERROR,	"Cannot apply binary operator '%s' to expressions of different type ('%s', '%s').",	ast.operator.repr, leftType.name, rightType.name);
			
		// Check for correct type
		if (!(leftType.equals("float") || leftType.equals("int")))
			throw new SemanticFailure(Cause.TYPE_ERROR,	"Binary operator '%s' only takes expressions of type 'int' or 'float', but found '%s'.", ast.operator.repr, leftType.name);
			
		// Modulo doesn't work with floats
		if (ast.operator == BOp.B_MOD && (!leftType.equals("int")))
			throw new SemanticFailure(Cause.TYPE_ERROR, "Binary operator '%%' only takes expressions of type 'int', but found '%s'.", leftType.name);
				
		return leftType;
	}
	
	public TypeSymbol arithmeticUnaryOp(Ast.UnaryOp ast, SemanticArgs arg) {
		TypeSymbol type = visit(ast.arg(), arg);
		
		// Check for correct type
		if (!(type.equals("float") || type.equals("int")))
			throw new SemanticFailure(Cause.TYPE_ERROR, "Unary operator '%s' only takes expressions of type 'int' or 'float', but found '%s'.", ast.operator.repr, type.name);
			
		return type;
	}
	
	public TypeSymbol booleanOp(Ast.BinaryOp ast, SemanticArgs arg) {
		TypeSymbol leftType = visit(ast.left(), arg);
		TypeSymbol rightType = visit(ast.right(), arg);
		
		// Check for equal type
		if (!leftType.equals(rightType))
			throw new SemanticFailure(Cause.TYPE_ERROR,	"Cannot apply binary operator '%s' to expressions of different type ('%s', '%s').", ast.operator.repr, leftType.name, rightType.name);
			
		// Check for correct type
		if (!leftType.equals("boolean"))
			throw new SemanticFailure(Cause.TYPE_ERROR,	"Binary operator '%s' only takes expressions of type 'boolean', but found '%s'.", ast.operator.repr, leftType.name);
				
		return (TypeSymbol)gst.lookup("boolean");
	}
	
	public TypeSymbol booleanUnaryOp(Ast.UnaryOp ast, SemanticArgs arg) {
		TypeSymbol type = visit(ast.arg(), arg);
		
		// Check for correct type
		if (!type.equals("boolean"))
			throw new SemanticFailure(Cause.TYPE_ERROR,	"Unary operator '%s' only takes expressions of type 'boolean', but found '%s'.", ast.operator.repr, type.name);
				
		return type;
	}
	
	public TypeSymbol relationalOp(Ast.BinaryOp ast, SemanticArgs arg) {
		TypeSymbol leftType = visit(ast.left(), arg);
		TypeSymbol rightType = visit(ast.right(), arg);
		
		// Check for equal type
		if (!leftType.equals(rightType))
			throw new SemanticFailure(Cause.TYPE_ERROR,	"Cannot apply operator '%s' to expressions of different type ('%s', '%s').", ast.operator.repr, leftType.name, rightType.name);
			
		// Check for correct type
		if (!(leftType.equals("float") || leftType.equals("int")))
			throw new SemanticFailure(Cause.TYPE_ERROR, "Operator '%s' only takes expressions of type 'int' or 'float', but found '%s'.", ast.operator.repr, leftType.name);
				
		return (TypeSymbol)gst.lookup("boolean");
	}
	
	public TypeSymbol equalityOp(Ast.BinaryOp ast, SemanticArgs arg) {
		TypeSymbol leftType = visit(ast.left(), arg);
		TypeSymbol rightType = visit(ast.right(), arg);
				
		// Check for consistent types
		if (!(leftType.isSubtypeOf(rightType) || rightType.isSubtypeOf(leftType)))
			throw new SemanticFailure(Cause.TYPE_ERROR,	"Cannot apply binary operator '%s' to incompatible types ('%s', '%s').", ast.operator.repr, leftType.name, rightType.name);
			
				
		return (TypeSymbol)gst.lookup("boolean");
	}
	
	// Check that the class is not part of a circular inheritance
	private void checkCircularInheritance(ClassDecl cd)	{
		ClassSymbol cs = cd.sym;
		while (cs.superClass != null) {
			cs = cs.superClass;
			
			if (cs.name.contentEquals(cd.sym.name)) {
				throw new SemanticFailure(Cause.CIRCULAR_INHERITANCE, "Circular inheritance detected in class %s.", cd.sym.name);
			}
		}
	}
	
	private boolean atLeastOneReturns(Ast statements) {
		
		if (statements instanceof Seq) {
			for (Ast node : ((Seq)statements).children()) {
				if (node.getReturns()) {
					return true;
				}
			}
		}
		else {
			if (statements.getReturns()) {
				return true;
			}
		}
		
		return false;
	}
}
