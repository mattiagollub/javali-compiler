package cd.semantic;

import java.util.List;

import cd.Main;
import cd.exceptions.SemanticFailure;
import cd.exceptions.ToDoException;
import cd.exceptions.SemanticFailure.Cause;
import cd.ir.Ast.ClassDecl;
import cd.ir.ClassSymbolTable;
import cd.ir.Symbol;
import cd.ir.Symbol.ArrayTypeSymbol;
import cd.ir.Symbol.ClassSymbol;
import cd.ir.Symbol.MethodSymbol;
import cd.ir.Symbol.PrimitiveTypeSymbol;
import cd.ir.Symbol.TypeSymbol;
import cd.ir.SymbolTable;

public class SemanticAnalyzer {

	public final Main main;
	private final SemanticVisitor sv;
	private final SymbolTableVisitor stVisitor;
	private final SymbolTable global;

	public SemanticAnalyzer(Main main) {
		this.main = main;
		this.global = new SymbolTable();
		this.stVisitor = new SymbolTableVisitor(global);
		this.sv = new SemanticVisitor();
	}

	public void check(List<ClassDecl> classDecls) throws SemanticFailure {
		// Add primitives symbols to the global SymbolTable
		addPrimitives();
		// Add class symbols to the global SymbolTable
		for (ClassDecl classDecl : classDecls) {
			addClass(classDecl);
		}
		// Link classes and super classes in the global SymbolTable
		for (ClassDecl classDecl : classDecls) {
			addSuperClass(classDecl);
		}
		// Create nodes for each scope
		for (ClassDecl classDecl : classDecls) {
			stVisitor.visit(classDecl, global);
		}

		checkValidStartPoint();

		// Update the 'this' reference and check semantic constraints of a class
		for (ClassDecl classDecl : classDecls) {
			global.addEntry("this", global.lookup(classDecl.name));
			sv.check(classDecl, global);
		}

	}

	private void checkValidStartPoint() {

		ClassSymbol mainClassSymbol = (ClassSymbol) global.lookup("Main");
		if (mainClassSymbol == null)
			throw new SemanticFailure(Cause.INVALID_START_POINT,
					"Missing class 'Main'");

		ClassSymbolTable mainClassTable = (ClassSymbolTable) global
				.getTable(mainClassSymbol);
		MethodSymbol mainMethodSymbol = (MethodSymbol) mainClassTable
				.methodLookup("main");

		if (mainMethodSymbol == null)
			throw new SemanticFailure(Cause.INVALID_START_POINT,
					"Missing method 'main'");

		if (mainMethodSymbol.ast.argumentNames.size() != 0)
			throw new SemanticFailure(Cause.INVALID_START_POINT,
					"Method 'main' cannot take arguments.");

		if (!mainMethodSymbol.ast.returnType.contentEquals("void"))
			throw new SemanticFailure(Cause.INVALID_START_POINT,
					"Method 'main' must be of type 'void'.");

	}

	private void addSuperClass(ClassDecl classDecl) {
		ClassSymbol classSym = (ClassSymbol) global.lookup(classDecl.name);
		ClassSymbol superClass = (ClassSymbol) global
				.lookup(classDecl.superClass);
		if (superClass == null) {
			throw new SemanticFailure(SemanticFailure.Cause.NO_SUCH_TYPE,
					"Not existing superclass type");
		}
		classSym.superClass = superClass;

	}

	private void addClass(ClassDecl classDecl) {
		// check for OBJECT_CLASS_DEFINED
		if (classDecl.name.equals("Object")) {
			throw new SemanticFailure(
					SemanticFailure.Cause.OBJECT_CLASS_DEFINED,
					"Invalid classname Object");
		}

		// check for unique class names
		if (global.lookup(classDecl.name) != null) {
			throw new SemanticFailure(SemanticFailure.Cause.DOUBLE_DECLARATION,
					"Not unique class names");
		}

		// Create symbol for ClassDecl
		ClassSymbol sym = new ClassSymbol(classDecl);

		// create classSymbolTable
		ClassSymbolTable classSymbolTable = new ClassSymbolTable();

		// link symbolTable to classSymbolTable
		global.linkTable(sym, classSymbolTable);

		// link AST to the symbol
		classDecl.sym = sym;

		// add symbol to the symbol table
		global.addEntry(sym.name, sym);

		// Create corresponding array type
		global.addEntry(sym.name + "[]", new ArrayTypeSymbol(sym));

	}

	private void addPrimitives() {
		TypeSymbol intSym = new PrimitiveTypeSymbol("int");
		TypeSymbol floatSym = new PrimitiveTypeSymbol("float");
		TypeSymbol booleanSym = new PrimitiveTypeSymbol("boolean");
		TypeSymbol voidSym = new PrimitiveTypeSymbol("void");
		TypeSymbol nullSym = new ClassSymbol("null");
		ClassSymbol objectSym = new ClassSymbol("Object");

		global.linkTable(objectSym, new ClassSymbolTable());

		global.addEntry("int", intSym);
		global.addEntry("float", floatSym);
		global.addEntry("boolean", booleanSym);
		global.addEntry("void", voidSym);
		global.addEntry("null", nullSym);
		global.addEntry("Object", objectSym);

		global.addEntry("int[]", new ArrayTypeSymbol(intSym));
		global.addEntry("float[]", new ArrayTypeSymbol(floatSym));
		global.addEntry("boolean[]", new ArrayTypeSymbol(booleanSym));
		global.addEntry("Object[]", new ArrayTypeSymbol(objectSym));

	}

}
