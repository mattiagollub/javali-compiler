package cd.ir;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Symbol {

	public final String name;

	public static abstract class TypeSymbol extends Symbol {

		public TypeSymbol(String name) {
			super(name);
		}

		public abstract boolean isReferenceType();

		public String toString() {
			return name;
		}

		public abstract boolean isSubtypeOf(TypeSymbol type);

		public abstract boolean isSubtypeOf(String type);

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof TypeSymbol)
				return name.contentEquals(((TypeSymbol) obj).name);
			if (obj instanceof CharSequence)
				return name.contentEquals((CharSequence) obj);

			return super.equals(obj);
		}
	}

	public static class PrimitiveTypeSymbol extends TypeSymbol {
		public PrimitiveTypeSymbol(String name) {
			super(name);
		}

		public boolean isReferenceType() {
			return false;
		}

		public boolean isSubtypeOf(TypeSymbol type) {
			return (this.equals(type));
		}

		public boolean isSubtypeOf(String type) {
			return (this.equals(type));
		}
	}

	public static class ArrayTypeSymbol extends TypeSymbol {
		public final TypeSymbol elementType;

		public ArrayTypeSymbol(TypeSymbol elementType) {
			super(elementType.name + "[]");
			this.elementType = elementType;
		}

		public boolean isReferenceType() {
			return true;
		}

		public boolean isSubtypeOf(TypeSymbol type) {
			if (type.name.contentEquals("Object") || this.equals(type)
					|| type.name.contentEquals("null")
					|| name.contentEquals("null"))
				return true;
			return false;
		}

		public boolean isSubtypeOf(String type) {
			if (type.contentEquals("Object") || this.equals(type)
					|| type.contentEquals("null") || name.contentEquals("null"))
				return true;
			return false;
		}
	}

	public static class ClassSymbol extends TypeSymbol {
		public final Ast.ClassDecl ast;
		public ClassSymbol superClass;
		public final VariableSymbol thisSymbol = new VariableSymbol("this",
				this);
		public final Map<String, VariableSymbol> fields = new HashMap<String, VariableSymbol>();
		public final Map<String, MethodSymbol> methods = new HashMap<String, MethodSymbol>();

		public ClassSymbol(Ast.ClassDecl ast) {
			super(ast.name);
			this.ast = ast;
		}

		/**
		 * Used to create the default {@code Object} and {@code <null>} types
		 */
		public ClassSymbol(String name) {
			super(name);
			this.ast = null;
		}

		public boolean isReferenceType() {
			return true;
		}

		public VariableSymbol getField(String name) {
			VariableSymbol fsym = fields.get(name);
			if (fsym == null && superClass != null)
				return superClass.getField(name);
			return fsym;
		}

		public MethodSymbol getMethod(String name) {
			MethodSymbol msym = methods.get(name);
			if (msym == null && superClass != null)
				return superClass.getMethod(name);
			return msym;
		}

		public boolean isSubtypeOf(TypeSymbol type) {
			if (type.name.contentEquals("null") || name.contentEquals("null"))
				return true;
			if (this.equals(type))
				return true;
			if (superClass != null && superClass.name.contentEquals(type.name))
				return true;
			if (superClass != null)
				return superClass.isSubtypeOf(type);

			return false;
		}

		public boolean isSubtypeOf(String type) {
			if (type.contentEquals("null") || name.contentEquals("null"))
				return true;
			if (this.equals(type))
				return true;
			if (superClass != null && superClass.name.contentEquals(type))
				return true;
			if (superClass != null)
				return superClass.isSubtypeOf(type);

			return false;
		}
	}

	public static class MethodSymbol extends Symbol {

		public final Ast.MethodDecl ast;
		public final Map<String, VariableSymbol> locals = new HashMap<String, VariableSymbol>();
		public final List<VariableSymbol> parameters = new ArrayList<VariableSymbol>();

		public TypeSymbol returnType;

		public MethodSymbol(Ast.MethodDecl ast) {
			super(ast.name);
			this.ast = ast;
		}

		public String toString() {
			return name + "(...)";
		}
	}

	public static class VariableSymbol extends Symbol {

		public static enum Kind {
			PARAM, LOCAL, FIELD
		};

		public final TypeSymbol type;
		public final Kind kind;

		public VariableSymbol(String name, TypeSymbol type) {
			this(name, type, Kind.PARAM);
		}

		public VariableSymbol(String name, TypeSymbol type, Kind kind) {
			super(name);
			this.type = type;
			this.kind = kind;
		}

		public String toString() {
			return name;
		}
	}

	protected Symbol(String name) {
		this.name = name;
	}

}
