package cd.ir;

import java.util.HashMap;
import java.util.Map;

public class ClassSymbolTable extends SymbolTable {
	private final Map<String, Symbol> methodSymbolMap;

	public ClassSymbolTable() {
		this.methodSymbolMap = new HashMap<String, Symbol>();
	}

	public void addMethodEntry(String name, Symbol symbol) {
		this.methodSymbolMap.put(name, symbol);
	}

	public Symbol methodLookup(String name) {
		return methodSymbolMap.get(name);
	}

}
