package cd.ir;

import java.util.HashMap;
import java.util.Map;

public class SymbolTable {
	private final Map<String, Symbol> symbolMap;
	private final Map<Symbol, SymbolTable> tableMap;

	public SymbolTable() {
		this.symbolMap = new HashMap<String, Symbol>();
		this.tableMap = new HashMap<Symbol, SymbolTable>();
	}

	public void addEntry(String name, Symbol symbol) {
		this.symbolMap.put(name, symbol);
	}

	public Symbol lookup(String name) {
		return symbolMap.get(name);
	}

	public void linkTable(Symbol symbol, SymbolTable symbolTable) {
		tableMap.put(symbol, symbolTable);
	}

	public SymbolTable getTable(Symbol symbol) {
		return tableMap.get(symbol);
	}

}
