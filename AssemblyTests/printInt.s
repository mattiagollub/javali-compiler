.global main

.data 

main:
	mov $2343, %eax
	xor %edi, %edi
	call print
	jmp end
print:
l0: 
	mov $10, %ecx
	xor %edx, %edx
	div %ecx
	add $'0', %edx
	push %edx
	inc %edi
	test %eax, %eax
	jne l0
l1:
	mov %esp, %ecx
	call printdigit
	add $4, %esp
	dec %edi
	test %edi, %edi
	jne l1
	ret

printdigit:
	mov $4, %eax
      	mov $1, %ebx
      	mov $1, %edx
      	int $0x80
	ret

end:	mov $1, %eax
	mov $0, %ebx
	int $0x80





