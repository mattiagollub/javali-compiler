  # Emitting class Main {...}
    # Emitting void main(...) {...}
    .global main
main:
      # Emitting write(222)
      mov $222, %eax
      xor %edi, %edi
      call label0
      jmp end
label0:
label1:
      mov $10, %ecx
      xor %edx, %edx
      div %ecx
      add $'0', %edx
      pushal %edx
      inc %edi
      test %eax, %eax
      jne label1
label2:
      mov %esp, %ecx
      call label3
      add $4, %esp
      dec %edi
      test %edi, %edi
      jne label2
      ret
label3:
      mov $4, %eax
      mov $1, %ebx
      mov $1, %edx
      int $0x80
      ret
end:
      mov $1, %eax
      mov $0, %ebx
      int $0x80
      # Emitting writeln()

