/**************************************************************************************************
 *   JAVALI TREE GRAMMAR
 *   Compiler Construction I
 *
 *   ABOUT:
 *   -------
 *   
 *   A tree grammar takes as input the ANTLR AST generated by the rewriting rules of the parser grammar
 *   and generates a visitor that visits the ANTLR AST and issues the action code associated with the tree
 *   grammar rule. We use the tree grammar for generating the Javali AST (i.e., AST whose nodes are
 *   instances of cd.ir.Ast.java). Thus, we invoke the appropriate Javali AST constructors in the action
 *   code associated with the tree grammar rules.
 *
 *   The rewriting rules of the parser grammar are a good starting point for generating the tree grammar.
 *   The easiest way to generate a tree grammar is to start with the parser grammar (copy & paste) and to
 *   remove the grammar productions (left side of arrow), leaving only the rewriting rules (right side of arrow).
 *   Note, however, that the rewriting rules may have to be changed. For instance, imaginary tokens need no
 *   longer to be created, but can simply be referenced (i.e., brackets with token binding must be omitted).
 *   Also, variables must be replaced with the actual node that is visited. Most likely, the tree grammar
 *   will also become more condensed than the parser grammar since it can expect the parser grammar
 *   to pass on valid ANTLR ASTs only. For instance, the tree grammar does not need to encode
 *   operator precedences.
 *
**************************************************************************************************/

tree grammar JavaliWalker; // tree grammar, parses streams of nodes

// (1) OPTIONS
options {
	tokenVocab=Javali; // uses tokens defined in Javali.g
	ASTLabelType=CommonTree;
}

// (2) ACTIONS
@header {
package cd.parser; // include any import statements that are required by your action code

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

import cd.ir.Ast;
import cd.ir.Ast.UnaryOp.UOp;
import cd.ir.Ast.BinaryOp.BOp;
import cd.ir.Ast.*;
import cd.util.Pair;

import cd.exceptions.ParseFailure;
}

@members { // incude any members that are required by your action code

	public <T> List<T> emptyList() {
		return java.util.Collections.emptyList();
	}
    
	public Seq seq(List<? extends Ast>... members) {
		Seq res = new Seq(null);
		if (members != null) {
			for (List<? extends Ast> lst : members)
				res.rwChildren().addAll(lst);
		}
		return res;
	}

	public Integer intValue(String s, int radix, int line) {
		try {
			return Integer.valueOf(s, radix);
		} catch (NumberFormatException e) {
			throw new ParseFailure(line, "Illegal integer: " + s);
		}
	}
	
	public Float floatValue(String s, int line) {
		try {
			return Float.valueOf(s);
		} catch (NumberFormatException e) {
			throw new ParseFailure(line, "Illegal float: " + s);
		}
	}
	
}

// (3) TREE GRAMMAR RULES
// TODO: specify tree grammar rules and action code...
unit returns [List<ClassDecl> classDeclList]
   @init {
      classDeclList = new ArrayList<ClassDecl>();
   }
	: (c=classDecl { classDeclList.add($c.node); } )+
	;

classDecl returns [ClassDecl node]
	: ^(ClassDecl name=Identifier (child=declList)? )
	{
		List<? extends Ast> members = $child.members;
		if (members == null)
			members = new ArrayList<Decl>();
		node = new ClassDecl($name.text, "Object", members);
	}
	| ^(ClassDecl name=Identifier sup=Identifier (child=declList)? )
	{
		List<? extends Ast> members = $child.members;
		if (members == null)
			members = new ArrayList<Decl>();
		node = new ClassDecl($name.text, $sup.text, members);
	}
	;
	
declList returns [List<Decl> members]
	@init
	{
		members = new ArrayList<Decl>();
	}
	: ( v=varDecl { members.add($v.node); } | m=methodDecl { members.add($m.node); })+ 
	;
	

varDecl returns [VarDecl node]
	: ^(VarDecl t=type n=Identifier
	   {	
	   	node = new VarDecl($t.text, $n.text);
	   }
	   )
	;
	
methodDecl returns [MethodDecl node]
	@init
	{
		boolean paramsNotNull = false;
	}
	: ^(MethodDecl t=type n=Identifier (p=formalParamList { paramsNotNull = true; })? b=methodBody)
	{
		List<Pair<String>> params = $p.params;
		if (!paramsNotNull)
			params = new ArrayList<Pair<String>>();
		node = new MethodDecl($t.text, $n.getText(), params, $b.seqs.a, $b.seqs.b);
 	}
	| ^(MethodDecl 'void' n=Identifier (p=formalParamList { paramsNotNull = true; })? b=methodBody)
	{
		List<Pair<String>> params = $p.params;
		if (!paramsNotNull)
			params = new ArrayList<Pair<String>>();
		node = new MethodDecl("void", $n.getText(), params, $b.seqs.a, $b.seqs.b);
 	}
	;

formalParamList returns [List<Pair<String>> params]
	@init
	{
		params = new ArrayList<Pair<String>>();
	}
	: (t=type n=Identifier { params.add(new Pair<String>($t.text, $n.text)); })+
	;

methodBody  returns [Pair<Seq> seqs]
   	: mb=methodBodyWithDeclList (^(Seq st=stmtList))?
   	{
   		Seq s = $st.seq;
   		if (s == null)
   			s = new Seq(null);
		seqs = new Pair<Seq>($mb.seq, s);
   	}
   	| ^(Seq st=stmtList)
   	{
   		seqs = new Pair<Seq>(new Seq(null), $st.seq);
   	}
   	| (Seq)
   	{
   		seqs = new Pair<Seq>(new Seq(null), new Seq(null));
      	}
   	;

methodBodyWithDeclList  returns [Seq seq]
   	: ^(Seq d=declList)
   	{
   		List<Ast> nodes = new ArrayList<Ast>();
   		
   		for(Decl element:$d.members)
                	nodes.add(element);

   		
		seq = new Seq(nodes);
   	}
   	;
   	
stmtList returns [Seq seq]
	@init
	{
		seq = new Seq(null);
	}
	: (st=stmt { seq.rwChildren().add($st.node); })*
	;
	
stmt returns [Stmt node]
	: a=assignmentOrMethodCall { node = $a.node; }
	| io=ioStmt { node = $io.node; }
	| ifs=ifStmt { node = $ifs.node; }
	| ws=whileStmt { node = $ws.node; }
	| ret=returnStmt { node = $ret.node; }
	;

assignmentOrMethodCall returns [Stmt node]
  	: id=identAccess
  	{
		node = new MethodCall((MethodCallExpr)$id.node);
  	}
      	| ^(Assign id=identAccess ass=assignmentTail)
      	{
      		node = new Assign($id.node, $ass.node);
      	}
   	;
   
assignmentTail returns [Expr node]
	: a=assignmentRHS
	{
		node = $a.node;
	}
	;
	
assignmentRHS returns [Expr node]
	: e=expr { node = $e.node; }
	| e=newExpr { node = $e.node; }
	| e=readExpr { node = $e.node; }
	| e=readExprFloat { node = $e.node; }
 	;
 	
methodCallTail returns [List<Expr> exs]
 	: p=actualParamList
 	{
 		exs = $p.exs;
 	}
 	;
 	
actualParamList returns [List<Expr> exs]
 	@init
 	{
 		exs = new ArrayList<Expr>();
 	}
	: (e=expr
	{
		$exs.add($e.node);
	})+
	;
	
ioStmt returns [Stmt node]
	: ^(BuiltInWrite exp=expr)
	{
		node  = new BuiltInWrite($exp.node);
	}
	| ^(BuiltInWriteFloat exp=expr)
	{
		node  = new BuiltInWriteFloat($exp.node);
	}
	| (BuiltInWriteln)
	{
		node  = new BuiltInWriteln();
	}
	;
	
ifStmt returns [Stmt node]
	@init
	{
		boolean e = false;
	}
	: ^(IfElse exp=expr ^(StatementList st1 = stmtList) (^(StatementList st2 = stmtList) {e = true;})?)
	{
		Seq s1 = $st1.seq;
		Ast s2 = $st2.seq;
		if (s1 == null)
			s1 = new Seq(null);
		if (s2 == null)
		{
			if (e)
				s2 = new Seq(null);
			else
				s2 = new Nop();
		}
		
		node = new IfElse($exp.node, s1, s2);
	}	
	;
	
whileStmt returns [Stmt node]
	: ^(WhileLoop exp=expr ^(StatementList body=stmtList))
	{
		node = new WhileLoop($exp.node, $body.seq);
	}
	;
	
returnStmt returns [Stmt node]
	: ^(ReturnStmt (exp=expr)?)
	{
		// Can be null.
		node = new ReturnStmt($exp.node);
	}
	;

newExpr returns [Expr node]
	: ^(NewObject id=Identifier)
	{
		node = new NewObject($id.getText());
	}
	| ^(NewArray ida=Identifier exp=expr)
	{
		node = new NewArray($ida.text + "[]", $exp.node);
	}
	| ^(NewArray idp=primitiveType exp=expr)
	{
		node = new NewArray($idp.text + "[]", $exp.node);
	}
	;

readExpr returns [Expr node]
	: BuiltInRead
	{
		node = new BuiltInRead();
	}
	;

readExprFloat returns [Expr node]
   	: (BuiltInReadFloat)
   	{
		node = new BuiltInReadFloat();
	}
   	;
   
expr returns [Expr node]
	@init
	{
		// Will always be overwritten
		BOp o = BOp.B_TIMES;
	}
	: ^(BinaryOp  a=expr (co=compOp { o = $co.op; } | wo=weakOp { o = $wo.op; } | so=strongOp { o = $so.op; }) b=expr)
	{
		node = new BinaryOp($a.node, o, $b.node);
	}
	| ^(UnaryOp U_PLUS f=expr)
	{
		node = new UnaryOp(UOp.U_PLUS, $f.node);
	}
	| ^(UnaryOp U_MINUS f=expr)
	{
		node = new UnaryOp(UOp.U_MINUS, $f.node);
	}
	| ^(UnaryOp U_BOOL_NOT f=expr)
	{
		node = new UnaryOp(UOp.U_BOOL_NOT, $f.node);
	}
	| f=noSignFactor
	{
		node = $f.node;
	}
	; 	
	
compOp returns [BOp op]
	: B_EQUAL { op = BOp.B_EQUAL; }
	| B_NOT_EQUAL { op = BOp.B_NOT_EQUAL; }
	| B_LESS_THAN { op = BOp.B_LESS_THAN; }
	| B_LESS_OR_EQUAL { op = BOp.B_LESS_OR_EQUAL; }
	| B_GREATER_THAN { op = BOp.B_GREATER_THAN; } 
	| B_GREATER_OR_EQUAL { op = BOp.B_GREATER_OR_EQUAL; }
	;

weakOp returns [BOp op]
	: B_PLUS { op = BOp.B_PLUS; }
	| B_MINUS { op = BOp.B_MINUS; }
	| B_OR { op = BOp.B_OR; }
	;
	
strongOp returns [BinaryOp.BOp op]
	: B_TIMES { op = BOp.B_TIMES; }
	| B_DIV { op = BOp.B_DIV; }
	| B_MOD { op = BOp.B_MOD; }
	| B_AND { op = BOp.B_AND; }
	;
	
noSignFactor returns [Expr node]
	: ^(DecimalIntConst d=DecimalNumber) { node = new IntConst(Integer.parseInt($d.text)); }
	| ^(HexIntConst h=HexNumber)  { node = new IntConst(Integer.parseInt($h.text)); }
	| ^(FloatConst fl=FloatNumber)  { node = new FloatConst(Float.parseFloat($fl.text)); }
	| ^(BooleanConst b=BooleanLiteral) { node = new BooleanConst(Boolean.valueOf($b.text)); }
	| (NullConst) { node = new NullConst(); }
   	| i=identAccess { node = $i.node; }
	| ^(Cast t=type f=noSignFactor) { node = new Cast($f.node, $t.text); }
	;
	
identAccess2  returns [Expr node]
 	: ( 
      	^(Var id=Identifier)
      	{
      		node = new Var($id.text);
      	}
      	| ^(MethodCall id=Identifier ThisRef (tail=methodCallTail)? ) 
      	{
      		node = new MethodCallExpr(new ThisRef(), $id.text, $tail.exs);
      	}
      	| (ThisRef)
      	{
      		node = new ThisRef(); 
      	}
      	)
      	(s=identAccess { node =  $s.node; })?  
   ;
	
identAccess returns [Expr node]
  	: ^(MethodCall id=Identifier s=identAccess (tail=methodCallTail)?)
   	{
   		List<Expr> t = $tail.exs;
   		if (t == null)
   			t = new ArrayList<Expr>();
   		node = new MethodCallExpr($s.node, $id.text, t);
	}
   	| ^(Field id=Identifier s=identAccess)
   	{
   		node = new Field($s.node, $id.text);
   	}
   	| ^(Index idx=expr s=identAccess )
   	{
   		node = new Index($s.node, $idx.node);
	}
	| ^(Var id=Identifier)
      	{
      		node = new Var($id.text);
      	}
      	| (ThisRef)
      	{
      		node = new ThisRef(); 
      	}
   	;
  
type returns [String text]
	: p=primitiveType { text = $p.text; }
	| t=referenceType { text = $t.text; }
	;

referenceType returns [String text]
	: i=Identifier { text = $i.text; }
	| a=arrayType { text = $a.text; }
	;

primitiveType returns [String text]
	: 'int' { text = "int"; }
	| 'float' { text = "float"; }
	| 'boolean'{ text = "boolean"; }
	;

arrayType returns [String text]
	: ^(ArrayType id=Identifier)  { text = $id.text + "[]"; } 
	| ^(ArrayType t=primitiveType) { text = $t.text + "[]"; }
	;
