grammar Parser;

options { 
   output=AST; 
}

// IMAGINARY TOKENS
tokens {
   Operation;
   Constant;
   OpPlus;
   OpMinus;
   OpMultiply;
   OpDivide;
}

// PARSER RULES
program
	:	expression* EOF-> expression*
	;

expression
   : (left=term -> term)
     (op=weakOp right=term -> ^(Operation $expression $op $right))*
   ;

weakOp
   : '+' -> OpPlus
   | '-' -> OpMinus
   ;

term
   : (left=factor -> factor)
     (op=strongOp right=factor -> ^(Operation $term $op $right))*
   ;

strongOp
   : '*' -> OpMultiply 
   | '/' -> OpDivide 
   ;

factor
   : value=Number -> Constant[$value]
   | '(' expression ')' -> expression
   ;

// LEXER RULES
Number
   :	'0'
   |	'1'..'9' ('0'..'9')*
   ;

WS
	:	(' '|'\r'|'\t'|'\u000C'|'\n') {$channel=HIDDEN;}
	;

COMMENT
	:	'/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
	;

LINE_COMMENT
	:	'//' ~('\n'|'\r')*  ('\r\n' | '\r' | '\n')? {$channel=HIDDEN;}
	;