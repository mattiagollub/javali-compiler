import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import org.antlr.runtime.*;
import org.antlr.stringtemplate.*;
import java.io.*;

public class Main {
   
   public static void main(String[] args) throws Exception {
      
      ANTLRInputStream input = new ANTLRInputStream(System.in);
   
      ParserLexer lexer = new ParserLexer(input);
      CommonTokenStream tokens = new CommonTokenStream(lexer);
            
      ParserParser parser = new ParserParser(tokens);
      ParserParser.program_return r = parser.program();
      
      CommonTree t = (CommonTree) r.getTree();
      CommonTreeNodeStream nodes = new CommonTreeNodeStream(t);
            
      Walker walker = new Walker(nodes);
      
      DOTTreeGenerator gen = new DOTTreeGenerator();
      
      StringTemplate st = gen.toDOT(t); 
      
      System.out.println();
      System.out.println(st);
      System.out.println();
      
      System.out.println();
      System.out.println(t.toStringTree());
      System.out.println();
      
      walker.program();
   
   }
   
}