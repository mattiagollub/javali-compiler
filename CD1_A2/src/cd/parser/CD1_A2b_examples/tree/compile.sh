# /bin/bash

antlr_jar=./antlr-3.5.1-complete.jar
mkdir -p ./compiled

java -cp $antlr_jar org.antlr.Tool -o ./compiled Parser.g
java -cp $antlr_jar org.antlr.Tool -o ./compiled Walker.g
javac -cp $antlr_jar Main.java ./compiled/*.java
