tree grammar Walker;

options { 
   tokenVocab=Parser;
   ASTLabelType=CommonTree;
}

program
	:	(e=expression { System.out.println("expr=" + $e.value); } )* 
	;

expression returns [int value]
   : ^(Operation left=expression op=operator right=expression)
   {  
      if ($op.op == "+") {
         $value = $left.value + $right.value;
      } else if ($op.op == "-") {
         $value = $left.value - $right.value;
      } else if ($op.op == "*") {
         $value = $left.value * $right.value;
      } else if ($op.op == "/") {
         $value = $left.value / $right.value;
      }
      
   }
   | c=Constant
   {
      $value = Integer.valueOf($c.text); 
      System.out.println("v=" + $value);
   }
   ;

operator returns[String op]
   : OpPlus 
   { 
      $op= "+";
      System.out.println($op);
   }
   | OpMinus 
   { 
      $op= "-";
      System.out.println($op);
   }
   | OpMultiply 
   { 
      $op= "*"; 
      System.out.println($op);
   } 
   | OpDivide 
   { 
      $op= "/";
      System.out.println($op);
   }
   ;

