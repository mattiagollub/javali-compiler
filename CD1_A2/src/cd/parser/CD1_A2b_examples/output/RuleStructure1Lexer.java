// $ANTLR 3.4 /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g 2013-10-19 11:43:58

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class RuleStructure1Lexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int COMMENT=4;
    public static final int Class=5;
    public static final int Identifier=6;
    public static final int JavaIDDigit=7;
    public static final int LINE_COMMENT=8;
    public static final int Letter=9;
    public static final int Package=10;
    public static final int WS=11;

    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public RuleStructure1Lexer() {} 
    public RuleStructure1Lexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public RuleStructure1Lexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "/Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g"; }

    // $ANTLR start "T__12"
    public final void mT__12() throws RecognitionException {
        try {
            int _type = T__12;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:2:7: ( '.class' )
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:2:9: '.class'
            {
            match(".class"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:3:7: ( '/' )
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:3:9: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "Identifier"
    public final void mIdentifier() throws RecognitionException {
        try {
            int _type = Identifier;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:39:2: ( Letter ( Letter | JavaIDDigit )* )
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:39:4: Letter ( Letter | JavaIDDigit )*
            {
            mLetter(); 


            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:39:11: ( Letter | JavaIDDigit )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0=='$'||(LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')||(LA1_0 >= '\u00C0' && LA1_0 <= '\u00D6')||(LA1_0 >= '\u00D8' && LA1_0 <= '\u00F6')||(LA1_0 >= '\u00F8' && LA1_0 <= '\u1FFF')||(LA1_0 >= '\u3040' && LA1_0 <= '\u318F')||(LA1_0 >= '\u3300' && LA1_0 <= '\u337F')||(LA1_0 >= '\u3400' && LA1_0 <= '\u3D2D')||(LA1_0 >= '\u4E00' && LA1_0 <= '\u9FFF')||(LA1_0 >= '\uF900' && LA1_0 <= '\uFAFF')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:
            	    {
            	    if ( input.LA(1)=='$'||(input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z')||(input.LA(1) >= '\u00C0' && input.LA(1) <= '\u00D6')||(input.LA(1) >= '\u00D8' && input.LA(1) <= '\u00F6')||(input.LA(1) >= '\u00F8' && input.LA(1) <= '\u1FFF')||(input.LA(1) >= '\u3040' && input.LA(1) <= '\u318F')||(input.LA(1) >= '\u3300' && input.LA(1) <= '\u337F')||(input.LA(1) >= '\u3400' && input.LA(1) <= '\u3D2D')||(input.LA(1) >= '\u4E00' && input.LA(1) <= '\u9FFF')||(input.LA(1) >= '\uF900' && input.LA(1) <= '\uFAFF') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Identifier"

    // $ANTLR start "Letter"
    public final void mLetter() throws RecognitionException {
        try {
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:45:2: ( '\\u0024' | '\\u0041' .. '\\u005a' | '\\u005f' | '\\u0061' .. '\\u007a' | '\\u00c0' .. '\\u00d6' | '\\u00d8' .. '\\u00f6' | '\\u00f8' .. '\\u00ff' | '\\u0100' .. '\\u1fff' | '\\u3040' .. '\\u318f' | '\\u3300' .. '\\u337f' | '\\u3400' .. '\\u3d2d' | '\\u4e00' .. '\\u9fff' | '\\uf900' .. '\\ufaff' )
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:
            {
            if ( input.LA(1)=='$'||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z')||(input.LA(1) >= '\u00C0' && input.LA(1) <= '\u00D6')||(input.LA(1) >= '\u00D8' && input.LA(1) <= '\u00F6')||(input.LA(1) >= '\u00F8' && input.LA(1) <= '\u1FFF')||(input.LA(1) >= '\u3040' && input.LA(1) <= '\u318F')||(input.LA(1) >= '\u3300' && input.LA(1) <= '\u337F')||(input.LA(1) >= '\u3400' && input.LA(1) <= '\u3D2D')||(input.LA(1) >= '\u4E00' && input.LA(1) <= '\u9FFF')||(input.LA(1) >= '\uF900' && input.LA(1) <= '\uFAFF') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Letter"

    // $ANTLR start "JavaIDDigit"
    public final void mJavaIDDigit() throws RecognitionException {
        try {
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:62:2: ( '\\u0030' .. '\\u0039' | '\\u0660' .. '\\u0669' | '\\u06f0' .. '\\u06f9' | '\\u0966' .. '\\u096f' | '\\u09e6' .. '\\u09ef' | '\\u0a66' .. '\\u0a6f' | '\\u0ae6' .. '\\u0aef' | '\\u0b66' .. '\\u0b6f' | '\\u0be7' .. '\\u0bef' | '\\u0c66' .. '\\u0c6f' | '\\u0ce6' .. '\\u0cef' | '\\u0d66' .. '\\u0d6f' | '\\u0e50' .. '\\u0e59' | '\\u0ed0' .. '\\u0ed9' | '\\u1040' .. '\\u1049' )
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:
            {
            if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= '\u0660' && input.LA(1) <= '\u0669')||(input.LA(1) >= '\u06F0' && input.LA(1) <= '\u06F9')||(input.LA(1) >= '\u0966' && input.LA(1) <= '\u096F')||(input.LA(1) >= '\u09E6' && input.LA(1) <= '\u09EF')||(input.LA(1) >= '\u0A66' && input.LA(1) <= '\u0A6F')||(input.LA(1) >= '\u0AE6' && input.LA(1) <= '\u0AEF')||(input.LA(1) >= '\u0B66' && input.LA(1) <= '\u0B6F')||(input.LA(1) >= '\u0BE7' && input.LA(1) <= '\u0BEF')||(input.LA(1) >= '\u0C66' && input.LA(1) <= '\u0C6F')||(input.LA(1) >= '\u0CE6' && input.LA(1) <= '\u0CEF')||(input.LA(1) >= '\u0D66' && input.LA(1) <= '\u0D6F')||(input.LA(1) >= '\u0E50' && input.LA(1) <= '\u0E59')||(input.LA(1) >= '\u0ED0' && input.LA(1) <= '\u0ED9')||(input.LA(1) >= '\u1040' && input.LA(1) <= '\u1049') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "JavaIDDigit"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:79:2: ( ( ' ' | '\\r' | '\\t' | '\\u000C' | '\\n' ) )
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:79:4: ( ' ' | '\\r' | '\\t' | '\\u000C' | '\\n' )
            {
            if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||(input.LA(1) >= '\f' && input.LA(1) <= '\r')||input.LA(1)==' ' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WS"

    // $ANTLR start "COMMENT"
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:83:2: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:83:4: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 



            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:83:9: ( options {greedy=false; } : . )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0=='*') ) {
                    int LA2_1 = input.LA(2);

                    if ( (LA2_1=='/') ) {
                        alt2=2;
                    }
                    else if ( ((LA2_1 >= '\u0000' && LA2_1 <= '.')||(LA2_1 >= '0' && LA2_1 <= '\uFFFF')) ) {
                        alt2=1;
                    }


                }
                else if ( ((LA2_0 >= '\u0000' && LA2_0 <= ')')||(LA2_0 >= '+' && LA2_0 <= '\uFFFF')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:83:37: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            match("*/"); 



            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COMMENT"

    // $ANTLR start "LINE_COMMENT"
    public final void mLINE_COMMENT() throws RecognitionException {
        try {
            int _type = LINE_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:87:2: ( '//' (~ ( '\\n' | '\\r' ) )* ( '\\r\\n' | '\\r' | '\\n' )? )
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:87:4: '//' (~ ( '\\n' | '\\r' ) )* ( '\\r\\n' | '\\r' | '\\n' )?
            {
            match("//"); 



            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:87:9: (~ ( '\\n' | '\\r' ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0 >= '\u0000' && LA3_0 <= '\t')||(LA3_0 >= '\u000B' && LA3_0 <= '\f')||(LA3_0 >= '\u000E' && LA3_0 <= '\uFFFF')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:
            	    {
            	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:87:24: ( '\\r\\n' | '\\r' | '\\n' )?
            int alt4=4;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='\r') ) {
                int LA4_1 = input.LA(2);

                if ( (LA4_1=='\n') ) {
                    alt4=1;
                }
            }
            else if ( (LA4_0=='\n') ) {
                alt4=3;
            }
            switch (alt4) {
                case 1 :
                    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:87:25: '\\r\\n'
                    {
                    match("\r\n"); 



                    }
                    break;
                case 2 :
                    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:87:34: '\\r'
                    {
                    match('\r'); 

                    }
                    break;
                case 3 :
                    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:87:41: '\\n'
                    {
                    match('\n'); 

                    }
                    break;

            }


            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LINE_COMMENT"

    public void mTokens() throws RecognitionException {
        // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:1:8: ( T__12 | T__13 | Identifier | WS | COMMENT | LINE_COMMENT )
        int alt5=6;
        int LA5_0 = input.LA(1);

        if ( (LA5_0=='.') ) {
            alt5=1;
        }
        else if ( (LA5_0=='/') ) {
            switch ( input.LA(2) ) {
            case '*':
                {
                alt5=5;
                }
                break;
            case '/':
                {
                alt5=6;
                }
                break;
            default:
                alt5=2;
            }

        }
        else if ( (LA5_0=='$'||(LA5_0 >= 'A' && LA5_0 <= 'Z')||LA5_0=='_'||(LA5_0 >= 'a' && LA5_0 <= 'z')||(LA5_0 >= '\u00C0' && LA5_0 <= '\u00D6')||(LA5_0 >= '\u00D8' && LA5_0 <= '\u00F6')||(LA5_0 >= '\u00F8' && LA5_0 <= '\u1FFF')||(LA5_0 >= '\u3040' && LA5_0 <= '\u318F')||(LA5_0 >= '\u3300' && LA5_0 <= '\u337F')||(LA5_0 >= '\u3400' && LA5_0 <= '\u3D2D')||(LA5_0 >= '\u4E00' && LA5_0 <= '\u9FFF')||(LA5_0 >= '\uF900' && LA5_0 <= '\uFAFF')) ) {
            alt5=3;
        }
        else if ( ((LA5_0 >= '\t' && LA5_0 <= '\n')||(LA5_0 >= '\f' && LA5_0 <= '\r')||LA5_0==' ') ) {
            alt5=4;
        }
        else {
            NoViableAltException nvae =
                new NoViableAltException("", 5, 0, input);

            throw nvae;

        }
        switch (alt5) {
            case 1 :
                // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:1:10: T__12
                {
                mT__12(); 


                }
                break;
            case 2 :
                // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:1:16: T__13
                {
                mT__13(); 


                }
                break;
            case 3 :
                // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:1:22: Identifier
                {
                mIdentifier(); 


                }
                break;
            case 4 :
                // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:1:33: WS
                {
                mWS(); 


                }
                break;
            case 5 :
                // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:1:36: COMMENT
                {
                mCOMMENT(); 


                }
                break;
            case 6 :
                // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:1:44: LINE_COMMENT
                {
                mLINE_COMMENT(); 


                }
                break;

        }

    }


 

}