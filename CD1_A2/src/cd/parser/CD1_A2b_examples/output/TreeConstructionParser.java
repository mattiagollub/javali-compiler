// $ANTLR 3.4 /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g 2013-10-19 10:36:17

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.debug.*;
import java.io.IOException;
import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class TreeConstructionParser extends DebugParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "COMMENT", "Declaration", "Identifier", "JavaIDDigit", "LINE_COMMENT", "Letter", "List", "WS", "','", "';'", "'int'"
    };

    public static final int EOF=-1;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int COMMENT=4;
    public static final int Declaration=5;
    public static final int Identifier=6;
    public static final int JavaIDDigit=7;
    public static final int LINE_COMMENT=8;
    public static final int Letter=9;
    public static final int List=10;
    public static final int WS=11;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


public static final String[] ruleNames = new String[] {
    "invalidRule", "decl", "list", "statements", "program"
};

public static final boolean[] decisionCanBacktrack = new boolean[] {
    false, // invalid decision
    false, false, false, false
};

 
    public int ruleLevel = 0;
    public int getRuleLevel() { return ruleLevel; }
    public void incRuleLevel() { ruleLevel++; }
    public void decRuleLevel() { ruleLevel--; }
    public TreeConstructionParser(TokenStream input) {
        this(input, DebugEventSocketProxy.DEFAULT_DEBUGGER_PORT, new RecognizerSharedState());
    }
    public TreeConstructionParser(TokenStream input, int port, RecognizerSharedState state) {
        super(input, state);
        DebugEventSocketProxy proxy =
            new DebugEventSocketProxy(this,port,adaptor);
        setDebugListener(proxy);
        setTokenStream(new DebugTokenStream(input,proxy));
        try {
            proxy.handshake();
        }
        catch (IOException ioe) {
            reportError(ioe);
        }
        TreeAdaptor adap = new CommonTreeAdaptor();
        setTreeAdaptor(adap);
        proxy.setTreeAdaptor(adap);
    }

public TreeConstructionParser(TokenStream input, DebugEventListener dbg) {
    super(input, dbg);
     
    TreeAdaptor adap = new CommonTreeAdaptor();
    setTreeAdaptor(adap);


}

protected boolean evalPredicate(boolean result, String predicate) {
    dbg.semanticPredicate(result, predicate);
    return result;
}

protected DebugTreeAdaptor adaptor;
public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = new DebugTreeAdaptor(dbg,adaptor);


}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}

    public String[] getTokenNames() { return TreeConstructionParser.tokenNames; }
    public String getGrammarFileName() { return "/Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g"; }


    public static class program_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "program"
    // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:12:1: program : ( statements )* EOF -> ( statements )* ;
    public final TreeConstructionParser.program_return program() throws RecognitionException {
        TreeConstructionParser.program_return retval = new TreeConstructionParser.program_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token EOF2=null;
        TreeConstructionParser.statements_return statements1 =null;


        Object EOF2_tree=null;
        RewriteRuleTokenStream stream_EOF=new RewriteRuleTokenStream(adaptor,"token EOF");
        RewriteRuleSubtreeStream stream_statements=new RewriteRuleSubtreeStream(adaptor,"rule statements");
        try { dbg.enterRule(getGrammarFileName(), "program");
        if ( getRuleLevel()==0 ) {dbg.commence();}
        incRuleLevel();
        dbg.location(12, 0);

        try {
            // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:13:4: ( ( statements )* EOF -> ( statements )* )
            dbg.enterAlt(1);

            // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:13:6: ( statements )* EOF
            {
            dbg.location(13,6);
            // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:13:6: ( statements )*
            try { dbg.enterSubRule(1);

            loop1:
            do {
                int alt1=2;
                try { dbg.enterDecision(1, decisionCanBacktrack[1]);

                int LA1_0 = input.LA(1);

                if ( (LA1_0==Identifier||LA1_0==14) ) {
                    alt1=1;
                }


                } finally {dbg.exitDecision(1);}

                switch (alt1) {
            	case 1 :
            	    dbg.enterAlt(1);

            	    // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:13:6: statements
            	    {
            	    dbg.location(13,6);
            	    pushFollow(FOLLOW_statements_in_program44);
            	    statements1=statements();

            	    state._fsp--;

            	    stream_statements.add(statements1.getTree());

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);
            } finally {dbg.exitSubRule(1);}

            dbg.location(13,18);
            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_program47);  
            stream_EOF.add(EOF2);


            // AST REWRITE
            // elements: statements
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 13:22: -> ( statements )*
            {
                dbg.location(13,25);
                // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:13:25: ( statements )*
                while ( stream_statements.hasNext() ) {
                    dbg.location(13,25);
                    adaptor.addChild(root_0, stream_statements.nextTree());

                }
                stream_statements.reset();

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        dbg.location(14, 3);

        }
        finally {
            dbg.exitRule(getGrammarFileName(), "program");
            decRuleLevel();
            if ( getRuleLevel()==0 ) {dbg.terminate();}
        }

        return retval;
    }
    // $ANTLR end "program"


    public static class statements_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "statements"
    // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:16:1: statements : ( decl | list );
    public final TreeConstructionParser.statements_return statements() throws RecognitionException {
        TreeConstructionParser.statements_return retval = new TreeConstructionParser.statements_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        TreeConstructionParser.decl_return decl3 =null;

        TreeConstructionParser.list_return list4 =null;



        try { dbg.enterRule(getGrammarFileName(), "statements");
        if ( getRuleLevel()==0 ) {dbg.commence();}
        incRuleLevel();
        dbg.location(16, 0);

        try {
            // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:17:2: ( decl | list )
            int alt2=2;
            try { dbg.enterDecision(2, decisionCanBacktrack[2]);

            int LA2_0 = input.LA(1);

            if ( (LA2_0==14) ) {
                alt2=1;
            }
            else if ( (LA2_0==Identifier) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                dbg.recognitionException(nvae);
                throw nvae;

            }
            } finally {dbg.exitDecision(2);}

            switch (alt2) {
                case 1 :
                    dbg.enterAlt(1);

                    // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:17:4: decl
                    {
                    root_0 = (Object)adaptor.nil();


                    dbg.location(17,4);
                    pushFollow(FOLLOW_decl_in_statements65);
                    decl3=decl();

                    state._fsp--;

                    adaptor.addChild(root_0, decl3.getTree());

                    }
                    break;
                case 2 :
                    dbg.enterAlt(2);

                    // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:18:4: list
                    {
                    root_0 = (Object)adaptor.nil();


                    dbg.location(18,4);
                    pushFollow(FOLLOW_list_in_statements70);
                    list4=list();

                    state._fsp--;

                    adaptor.addChild(root_0, list4.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        dbg.location(19, 1);

        }
        finally {
            dbg.exitRule(getGrammarFileName(), "statements");
            decRuleLevel();
            if ( getRuleLevel()==0 ) {dbg.terminate();}
        }

        return retval;
    }
    // $ANTLR end "statements"


    public static class decl_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "decl"
    // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:21:1: decl : 'int' Identifier ( ',' Identifier )* ';' -> ( ^( Declaration Identifier ) )+ ;
    public final TreeConstructionParser.decl_return decl() throws RecognitionException {
        TreeConstructionParser.decl_return retval = new TreeConstructionParser.decl_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token string_literal5=null;
        Token Identifier6=null;
        Token char_literal7=null;
        Token Identifier8=null;
        Token char_literal9=null;

        Object string_literal5_tree=null;
        Object Identifier6_tree=null;
        Object char_literal7_tree=null;
        Object Identifier8_tree=null;
        Object char_literal9_tree=null;
        RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
        RewriteRuleTokenStream stream_13=new RewriteRuleTokenStream(adaptor,"token 13");
        RewriteRuleTokenStream stream_14=new RewriteRuleTokenStream(adaptor,"token 14");
        RewriteRuleTokenStream stream_12=new RewriteRuleTokenStream(adaptor,"token 12");

        try { dbg.enterRule(getGrammarFileName(), "decl");
        if ( getRuleLevel()==0 ) {dbg.commence();}
        incRuleLevel();
        dbg.location(21, 0);

        try {
            // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:22:4: ( 'int' Identifier ( ',' Identifier )* ';' -> ( ^( Declaration Identifier ) )+ )
            dbg.enterAlt(1);

            // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:22:6: 'int' Identifier ( ',' Identifier )* ';'
            {
            dbg.location(22,6);
            string_literal5=(Token)match(input,14,FOLLOW_14_in_decl84);  
            stream_14.add(string_literal5);

            dbg.location(22,12);
            Identifier6=(Token)match(input,Identifier,FOLLOW_Identifier_in_decl86);  
            stream_Identifier.add(Identifier6);

            dbg.location(22,23);
            // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:22:23: ( ',' Identifier )*
            try { dbg.enterSubRule(3);

            loop3:
            do {
                int alt3=2;
                try { dbg.enterDecision(3, decisionCanBacktrack[3]);

                int LA3_0 = input.LA(1);

                if ( (LA3_0==12) ) {
                    alt3=1;
                }


                } finally {dbg.exitDecision(3);}

                switch (alt3) {
            	case 1 :
            	    dbg.enterAlt(1);

            	    // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:22:24: ',' Identifier
            	    {
            	    dbg.location(22,24);
            	    char_literal7=(Token)match(input,12,FOLLOW_12_in_decl89);  
            	    stream_12.add(char_literal7);

            	    dbg.location(22,28);
            	    Identifier8=(Token)match(input,Identifier,FOLLOW_Identifier_in_decl91);  
            	    stream_Identifier.add(Identifier8);


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);
            } finally {dbg.exitSubRule(3);}

            dbg.location(22,41);
            char_literal9=(Token)match(input,13,FOLLOW_13_in_decl95);  
            stream_13.add(char_literal9);


            // AST REWRITE
            // elements: Identifier
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 22:45: -> ( ^( Declaration Identifier ) )+
            {
                dbg.location(22,48);
                if ( !(stream_Identifier.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_Identifier.hasNext() ) {
                    dbg.location(22,48);
                    // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:22:48: ^( Declaration Identifier )
                    {
                    Object root_1 = (Object)adaptor.nil();
                    dbg.location(22,50);
                    root_1 = (Object)adaptor.becomeRoot(
                    (Object)adaptor.create(Declaration, "Declaration")
                    , root_1);

                    dbg.location(22,62);
                    adaptor.addChild(root_1, 
                    stream_Identifier.nextNode()
                    );

                    adaptor.addChild(root_0, root_1);
                    }

                }
                stream_Identifier.reset();

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        dbg.location(23, 3);

        }
        finally {
            dbg.exitRule(getGrammarFileName(), "decl");
            decRuleLevel();
            if ( getRuleLevel()==0 ) {dbg.terminate();}
        }

        return retval;
    }
    // $ANTLR end "decl"


    public static class list_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "list"
    // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:25:1: list : Identifier ( ',' Identifier )* ';' -> ^( List ( Identifier )+ ) ;
    public final TreeConstructionParser.list_return list() throws RecognitionException {
        TreeConstructionParser.list_return retval = new TreeConstructionParser.list_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Identifier10=null;
        Token char_literal11=null;
        Token Identifier12=null;
        Token char_literal13=null;

        Object Identifier10_tree=null;
        Object char_literal11_tree=null;
        Object Identifier12_tree=null;
        Object char_literal13_tree=null;
        RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
        RewriteRuleTokenStream stream_13=new RewriteRuleTokenStream(adaptor,"token 13");
        RewriteRuleTokenStream stream_12=new RewriteRuleTokenStream(adaptor,"token 12");

        try { dbg.enterRule(getGrammarFileName(), "list");
        if ( getRuleLevel()==0 ) {dbg.commence();}
        incRuleLevel();
        dbg.location(25, 0);

        try {
            // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:26:4: ( Identifier ( ',' Identifier )* ';' -> ^( List ( Identifier )+ ) )
            dbg.enterAlt(1);

            // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:26:6: Identifier ( ',' Identifier )* ';'
            {
            dbg.location(26,6);
            Identifier10=(Token)match(input,Identifier,FOLLOW_Identifier_in_list122);  
            stream_Identifier.add(Identifier10);

            dbg.location(26,17);
            // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:26:17: ( ',' Identifier )*
            try { dbg.enterSubRule(4);

            loop4:
            do {
                int alt4=2;
                try { dbg.enterDecision(4, decisionCanBacktrack[4]);

                int LA4_0 = input.LA(1);

                if ( (LA4_0==12) ) {
                    alt4=1;
                }


                } finally {dbg.exitDecision(4);}

                switch (alt4) {
            	case 1 :
            	    dbg.enterAlt(1);

            	    // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:26:18: ',' Identifier
            	    {
            	    dbg.location(26,18);
            	    char_literal11=(Token)match(input,12,FOLLOW_12_in_list125);  
            	    stream_12.add(char_literal11);

            	    dbg.location(26,22);
            	    Identifier12=(Token)match(input,Identifier,FOLLOW_Identifier_in_list127);  
            	    stream_Identifier.add(Identifier12);


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);
            } finally {dbg.exitSubRule(4);}

            dbg.location(26,35);
            char_literal13=(Token)match(input,13,FOLLOW_13_in_list131);  
            stream_13.add(char_literal13);


            // AST REWRITE
            // elements: Identifier
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 26:39: -> ^( List ( Identifier )+ )
            {
                dbg.location(26,42);
                // /Users/margarita/Downloads/CD1_A2b_examples/TreeConstruction.g:26:42: ^( List ( Identifier )+ )
                {
                Object root_1 = (Object)adaptor.nil();
                dbg.location(26,44);
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(List, "List")
                , root_1);

                dbg.location(26,49);
                if ( !(stream_Identifier.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_Identifier.hasNext() ) {
                    dbg.location(26,49);
                    adaptor.addChild(root_1, 
                    stream_Identifier.nextNode()
                    );

                }
                stream_Identifier.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        dbg.location(27, 3);

        }
        finally {
            dbg.exitRule(getGrammarFileName(), "list");
            decRuleLevel();
            if ( getRuleLevel()==0 ) {dbg.terminate();}
        }

        return retval;
    }
    // $ANTLR end "list"

    // Delegated rules


 

    public static final BitSet FOLLOW_statements_in_program44 = new BitSet(new long[]{0x0000000000004040L});
    public static final BitSet FOLLOW_EOF_in_program47 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_decl_in_statements65 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_list_in_statements70 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_decl84 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_Identifier_in_decl86 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_12_in_decl89 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_Identifier_in_decl91 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_13_in_decl95 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Identifier_in_list122 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_12_in_list125 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_Identifier_in_list127 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_13_in_list131 = new BitSet(new long[]{0x0000000000000002L});

}