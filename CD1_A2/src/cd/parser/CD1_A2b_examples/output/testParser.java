// $ANTLR 3.4 /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/test.g 2013-10-19 11:47:39

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.debug.*;
import java.io.IOException;
import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class testParser extends DebugParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "COMMENT", "Class", "Identifier", "JavaIDDigit", "LINE_COMMENT", "Letter", "WS"
    };

    public static final int EOF=-1;
    public static final int COMMENT=4;
    public static final int Class=5;
    public static final int Identifier=6;
    public static final int JavaIDDigit=7;
    public static final int LINE_COMMENT=8;
    public static final int Letter=9;
    public static final int WS=10;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


public static final String[] ruleNames = new String[] {
    "invalidRule", "unit", "classDecl"
};

public static final boolean[] decisionCanBacktrack = new boolean[] {
    false, // invalid decision
    false
};

 
    public int ruleLevel = 0;
    public int getRuleLevel() { return ruleLevel; }
    public void incRuleLevel() { ruleLevel++; }
    public void decRuleLevel() { ruleLevel--; }
    public testParser(TokenStream input) {
        this(input, DebugEventSocketProxy.DEFAULT_DEBUGGER_PORT, new RecognizerSharedState());
    }
    public testParser(TokenStream input, int port, RecognizerSharedState state) {
        super(input, state);
        DebugEventSocketProxy proxy =
            new DebugEventSocketProxy(this,port,adaptor);
        setDebugListener(proxy);
        setTokenStream(new DebugTokenStream(input,proxy));
        try {
            proxy.handshake();
        }
        catch (IOException ioe) {
            reportError(ioe);
        }
        TreeAdaptor adap = new CommonTreeAdaptor();
        setTreeAdaptor(adap);
        proxy.setTreeAdaptor(adap);
    }

public testParser(TokenStream input, DebugEventListener dbg) {
    super(input, dbg);
     
    TreeAdaptor adap = new CommonTreeAdaptor();
    setTreeAdaptor(adap);


}

protected boolean evalPredicate(boolean result, String predicate) {
    dbg.semanticPredicate(result, predicate);
    return result;
}

protected DebugTreeAdaptor adaptor;
public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = new DebugTreeAdaptor(dbg,adaptor);


}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}

    public String[] getTokenNames() { return testParser.tokenNames; }
    public String getGrammarFileName() { return "/Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/test.g"; }


    public static class unit_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "unit"
    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/test.g:13:1: unit : ( classDecl[$unit.tree] )+ EOF -> ( classDecl )+ ;
    public final testParser.unit_return unit() throws RecognitionException {
        testParser.unit_return retval = new testParser.unit_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token EOF2=null;
        testParser.classDecl_return classDecl1 =null;


        Object EOF2_tree=null;
        RewriteRuleTokenStream stream_EOF=new RewriteRuleTokenStream(adaptor,"token EOF");
        RewriteRuleSubtreeStream stream_classDecl=new RewriteRuleSubtreeStream(adaptor,"rule classDecl");
        try { dbg.enterRule(getGrammarFileName(), "unit");
        if ( getRuleLevel()==0 ) {dbg.commence();}
        incRuleLevel();
        dbg.location(13, 0);

        try {
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/test.g:14:2: ( ( classDecl[$unit.tree] )+ EOF -> ( classDecl )+ )
            dbg.enterAlt(1);

            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/test.g:14:4: ( classDecl[$unit.tree] )+ EOF
            {
            dbg.location(14,4);
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/test.g:14:4: ( classDecl[$unit.tree] )+
            int cnt1=0;
            try { dbg.enterSubRule(1);

            loop1:
            do {
                int alt1=2;
                try { dbg.enterDecision(1, decisionCanBacktrack[1]);

                int LA1_0 = input.LA(1);

                if ( (LA1_0==Identifier) ) {
                    alt1=1;
                }


                } finally {dbg.exitDecision(1);}

                switch (alt1) {
            	case 1 :
            	    dbg.enterAlt(1);

            	    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/test.g:14:4: classDecl[$unit.tree]
            	    {
            	    dbg.location(14,4);
            	    pushFollow(FOLLOW_classDecl_in_unit39);
            	    classDecl1=classDecl(((Object)retval.tree));

            	    state._fsp--;

            	    stream_classDecl.add(classDecl1.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        dbg.recognitionException(eee);

                        throw eee;
                }
                cnt1++;
            } while (true);
            } finally {dbg.exitSubRule(1);}

            dbg.location(14,27);
            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_unit43);  
            stream_EOF.add(EOF2);


            // AST REWRITE
            // elements: classDecl
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 14:31: -> ( classDecl )+
            {
                dbg.location(14,34);
                if ( !(stream_classDecl.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_classDecl.hasNext() ) {
                    dbg.location(14,34);
                    adaptor.addChild(root_0, stream_classDecl.nextTree());

                }
                stream_classDecl.reset();

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        dbg.location(15, 1);

        }
        finally {
            dbg.exitRule(getGrammarFileName(), "unit");
            decRuleLevel();
            if ( getRuleLevel()==0 ) {dbg.terminate();}
        }

        return retval;
    }
    // $ANTLR end "unit"


    public static class classDecl_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "classDecl"
    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/test.g:17:1: classDecl[Object superClass] : name= Identifier -> ^( Class[$classDecl.start, \"ciaobelo\"] $name) ;
    public final testParser.classDecl_return classDecl(Object superClass) throws RecognitionException {
        testParser.classDecl_return retval = new testParser.classDecl_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token name=null;

        Object name_tree=null;
        RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");

        try { dbg.enterRule(getGrammarFileName(), "classDecl");
        if ( getRuleLevel()==0 ) {dbg.commence();}
        incRuleLevel();
        dbg.location(17, 0);

        try {
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/test.g:18:2: (name= Identifier -> ^( Class[$classDecl.start, \"ciaobelo\"] $name) )
            dbg.enterAlt(1);

            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/test.g:18:4: name= Identifier
            {
            dbg.location(18,9);
            name=(Token)match(input,Identifier,FOLLOW_Identifier_in_classDecl65);  
            stream_Identifier.add(name);


            // AST REWRITE
            // elements: name
            // token labels: name
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleTokenStream stream_name=new RewriteRuleTokenStream(adaptor,"token name",name);
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 18:21: -> ^( Class[$classDecl.start, \"ciaobelo\"] $name)
            {
                dbg.location(18,24);
                // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/test.g:18:24: ^( Class[$classDecl.start, \"ciaobelo\"] $name)
                {
                Object root_1 = (Object)adaptor.nil();
                dbg.location(18,26);
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(Class, ((Token)retval.start), "ciaobelo")
                , root_1);

                dbg.location(18,63);
                adaptor.addChild(root_1, superClass);
                dbg.location(18,78);
                adaptor.addChild(root_1, stream_name.nextNode());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        dbg.location(19, 1);

        }
        finally {
            dbg.exitRule(getGrammarFileName(), "classDecl");
            decRuleLevel();
            if ( getRuleLevel()==0 ) {dbg.terminate();}
        }

        return retval;
    }
    // $ANTLR end "classDecl"

    // Delegated rules


 

    public static final BitSet FOLLOW_classDecl_in_unit39 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_EOF_in_unit43 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Identifier_in_classDecl65 = new BitSet(new long[]{0x0000000000000002L});

}