// $ANTLR 3.4 /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g 2013-10-19 11:43:58

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.debug.*;
import java.io.IOException;
import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class RuleStructure1Parser extends DebugParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "COMMENT", "Class", "Identifier", "JavaIDDigit", "LINE_COMMENT", "Letter", "Package", "WS", "'.class'", "'/'"
    };

    public static final int EOF=-1;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int COMMENT=4;
    public static final int Class=5;
    public static final int Identifier=6;
    public static final int JavaIDDigit=7;
    public static final int LINE_COMMENT=8;
    public static final int Letter=9;
    public static final int Package=10;
    public static final int WS=11;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


public static final String[] ruleNames = new String[] {
    "invalidRule", "clazz", "packz", "classPath", "entity", "classes"
};

public static final boolean[] decisionCanBacktrack = new boolean[] {
    false, // invalid decision
    false, false, false
};

 
    public int ruleLevel = 0;
    public int getRuleLevel() { return ruleLevel; }
    public void incRuleLevel() { ruleLevel++; }
    public void decRuleLevel() { ruleLevel--; }
    public RuleStructure1Parser(TokenStream input) {
        this(input, DebugEventSocketProxy.DEFAULT_DEBUGGER_PORT, new RecognizerSharedState());
    }
    public RuleStructure1Parser(TokenStream input, int port, RecognizerSharedState state) {
        super(input, state);
        DebugEventSocketProxy proxy =
            new DebugEventSocketProxy(this,port,adaptor);
        setDebugListener(proxy);
        setTokenStream(new DebugTokenStream(input,proxy));
        try {
            proxy.handshake();
        }
        catch (IOException ioe) {
            reportError(ioe);
        }
        TreeAdaptor adap = new CommonTreeAdaptor();
        setTreeAdaptor(adap);
        proxy.setTreeAdaptor(adap);
    }

public RuleStructure1Parser(TokenStream input, DebugEventListener dbg) {
    super(input, dbg);
     
    TreeAdaptor adap = new CommonTreeAdaptor();
    setTreeAdaptor(adap);


}

protected boolean evalPredicate(boolean result, String predicate) {
    dbg.semanticPredicate(result, predicate);
    return result;
}

protected DebugTreeAdaptor adaptor;
public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = new DebugTreeAdaptor(dbg,adaptor);


}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}

    public String[] getTokenNames() { return RuleStructure1Parser.tokenNames; }
    public String getGrammarFileName() { return "/Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g"; }


    public static class classes_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "classes"
    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:14:1: classes : ( classPath )* EOF -> ( classPath )* ;
    public final RuleStructure1Parser.classes_return classes() throws RecognitionException {
        RuleStructure1Parser.classes_return retval = new RuleStructure1Parser.classes_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token EOF2=null;
        RuleStructure1Parser.classPath_return classPath1 =null;


        Object EOF2_tree=null;
        RewriteRuleTokenStream stream_EOF=new RewriteRuleTokenStream(adaptor,"token EOF");
        RewriteRuleSubtreeStream stream_classPath=new RewriteRuleSubtreeStream(adaptor,"rule classPath");
        try { dbg.enterRule(getGrammarFileName(), "classes");
        if ( getRuleLevel()==0 ) {dbg.commence();}
        incRuleLevel();
        dbg.location(14, 0);

        try {
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:15:4: ( ( classPath )* EOF -> ( classPath )* )
            dbg.enterAlt(1);

            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:15:6: ( classPath )* EOF
            {
            dbg.location(15,6);
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:15:6: ( classPath )*
            try { dbg.enterSubRule(1);

            loop1:
            do {
                int alt1=2;
                try { dbg.enterDecision(1, decisionCanBacktrack[1]);

                int LA1_0 = input.LA(1);

                if ( (LA1_0==Identifier) ) {
                    alt1=1;
                }


                } finally {dbg.exitDecision(1);}

                switch (alt1) {
            	case 1 :
            	    dbg.enterAlt(1);

            	    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:15:6: classPath
            	    {
            	    dbg.location(15,6);
            	    pushFollow(FOLLOW_classPath_in_classes47);
            	    classPath1=classPath();

            	    state._fsp--;

            	    stream_classPath.add(classPath1.getTree());

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);
            } finally {dbg.exitSubRule(1);}

            dbg.location(15,17);
            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_classes50);  
            stream_EOF.add(EOF2);


            // AST REWRITE
            // elements: classPath
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 15:21: -> ( classPath )*
            {
                dbg.location(15,24);
                // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:15:24: ( classPath )*
                while ( stream_classPath.hasNext() ) {
                    dbg.location(15,24);
                    adaptor.addChild(root_0, stream_classPath.nextTree());

                }
                stream_classPath.reset();

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        dbg.location(17, 3);

        }
        finally {
            dbg.exitRule(getGrammarFileName(), "classes");
            decRuleLevel();
            if ( getRuleLevel()==0 ) {dbg.terminate();}
        }

        return retval;
    }
    // $ANTLR end "classes"


    public static class classPath_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "classPath"
    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:19:1: classPath : ( entity[$classPath.tree] -> entity ) ( '/' entity[$classPath.tree] -> entity )* ;
    public final RuleStructure1Parser.classPath_return classPath() throws RecognitionException {
        RuleStructure1Parser.classPath_return retval = new RuleStructure1Parser.classPath_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal4=null;
        RuleStructure1Parser.entity_return entity3 =null;

        RuleStructure1Parser.entity_return entity5 =null;


        Object char_literal4_tree=null;
        RewriteRuleTokenStream stream_13=new RewriteRuleTokenStream(adaptor,"token 13");
        RewriteRuleSubtreeStream stream_entity=new RewriteRuleSubtreeStream(adaptor,"rule entity");
        try { dbg.enterRule(getGrammarFileName(), "classPath");
        if ( getRuleLevel()==0 ) {dbg.commence();}
        incRuleLevel();
        dbg.location(19, 0);

        try {
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:20:2: ( ( entity[$classPath.tree] -> entity ) ( '/' entity[$classPath.tree] -> entity )* )
            dbg.enterAlt(1);

            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:20:4: ( entity[$classPath.tree] -> entity ) ( '/' entity[$classPath.tree] -> entity )*
            {
            dbg.location(20,4);
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:20:4: ( entity[$classPath.tree] -> entity )
            dbg.enterAlt(1);

            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:20:5: entity[$classPath.tree]
            {
            dbg.location(20,5);
            pushFollow(FOLLOW_entity_in_classPath73);
            entity3=entity(((Object)retval.tree));

            state._fsp--;

            stream_entity.add(entity3.getTree());

            // AST REWRITE
            // elements: entity
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 20:29: -> entity
            {
                dbg.location(20,32);
                adaptor.addChild(root_0, stream_entity.nextTree());

            }


            retval.tree = root_0;

            }

            dbg.location(20,40);
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:20:40: ( '/' entity[$classPath.tree] -> entity )*
            try { dbg.enterSubRule(2);

            loop2:
            do {
                int alt2=2;
                try { dbg.enterDecision(2, decisionCanBacktrack[2]);

                int LA2_0 = input.LA(1);

                if ( (LA2_0==13) ) {
                    alt2=1;
                }


                } finally {dbg.exitDecision(2);}

                switch (alt2) {
            	case 1 :
            	    dbg.enterAlt(1);

            	    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:20:41: '/' entity[$classPath.tree]
            	    {
            	    dbg.location(20,41);
            	    char_literal4=(Token)match(input,13,FOLLOW_13_in_classPath82);  
            	    stream_13.add(char_literal4);

            	    dbg.location(20,45);
            	    pushFollow(FOLLOW_entity_in_classPath84);
            	    entity5=entity(((Object)retval.tree));

            	    state._fsp--;

            	    stream_entity.add(entity5.getTree());

            	    // AST REWRITE
            	    // elements: entity
            	    // token labels: 
            	    // rule labels: retval
            	    // token list labels: 
            	    // rule list labels: 
            	    // wildcard labels: 
            	    retval.tree = root_0;
            	    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            	    root_0 = (Object)adaptor.nil();
            	    // 20:69: -> entity
            	    {
            	        dbg.location(20,72);
            	        adaptor.addChild(root_0, stream_entity.nextTree());

            	    }


            	    retval.tree = root_0;

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);
            } finally {dbg.exitSubRule(2);}


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        dbg.location(21, 1);

        }
        finally {
            dbg.exitRule(getGrammarFileName(), "classPath");
            decRuleLevel();
            if ( getRuleLevel()==0 ) {dbg.terminate();}
        }

        return retval;
    }
    // $ANTLR end "classPath"


    public static class entity_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "entity"
    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:23:1: entity[Object superEntity] : ( packz[superEntity] -> packz | clazz[superEntity] -> clazz );
    public final RuleStructure1Parser.entity_return entity(Object superEntity) throws RecognitionException {
        RuleStructure1Parser.entity_return retval = new RuleStructure1Parser.entity_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        RuleStructure1Parser.packz_return packz6 =null;

        RuleStructure1Parser.clazz_return clazz7 =null;


        RewriteRuleSubtreeStream stream_packz=new RewriteRuleSubtreeStream(adaptor,"rule packz");
        RewriteRuleSubtreeStream stream_clazz=new RewriteRuleSubtreeStream(adaptor,"rule clazz");
        try { dbg.enterRule(getGrammarFileName(), "entity");
        if ( getRuleLevel()==0 ) {dbg.commence();}
        incRuleLevel();
        dbg.location(23, 0);

        try {
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:24:4: ( packz[superEntity] -> packz | clazz[superEntity] -> clazz )
            int alt3=2;
            try { dbg.enterDecision(3, decisionCanBacktrack[3]);

            int LA3_0 = input.LA(1);

            if ( (LA3_0==Identifier) ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1==12) ) {
                    alt3=2;
                }
                else if ( (LA3_1==EOF||LA3_1==Identifier||LA3_1==13) ) {
                    alt3=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 1, input);

                    dbg.recognitionException(nvae);
                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                dbg.recognitionException(nvae);
                throw nvae;

            }
            } finally {dbg.exitDecision(3);}

            switch (alt3) {
                case 1 :
                    dbg.enterAlt(1);

                    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:24:6: packz[superEntity]
                    {
                    dbg.location(24,6);
                    pushFollow(FOLLOW_packz_in_entity105);
                    packz6=packz(superEntity);

                    state._fsp--;

                    stream_packz.add(packz6.getTree());

                    // AST REWRITE
                    // elements: packz
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 24:25: -> packz
                    {
                        dbg.location(24,28);
                        adaptor.addChild(root_0, stream_packz.nextTree());

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    dbg.enterAlt(2);

                    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:25:7: clazz[superEntity]
                    {
                    dbg.location(25,7);
                    pushFollow(FOLLOW_clazz_in_entity118);
                    clazz7=clazz(superEntity);

                    state._fsp--;

                    stream_clazz.add(clazz7.getTree());

                    // AST REWRITE
                    // elements: clazz
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 25:26: -> clazz
                    {
                        dbg.location(25,29);
                        adaptor.addChild(root_0, stream_clazz.nextTree());

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        dbg.location(27, 3);

        }
        finally {
            dbg.exitRule(getGrammarFileName(), "entity");
            decRuleLevel();
            if ( getRuleLevel()==0 ) {dbg.terminate();}
        }

        return retval;
    }
    // $ANTLR end "entity"


    public static class packz_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "packz"
    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:29:1: packz[Object superPackage] : name= Identifier -> ^( Package[$packz.start,\"Pakkage\"+$superPackage] $name) ;
    public final RuleStructure1Parser.packz_return packz(Object superPackage) throws RecognitionException {
        RuleStructure1Parser.packz_return retval = new RuleStructure1Parser.packz_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token name=null;

        Object name_tree=null;
        RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");

        try { dbg.enterRule(getGrammarFileName(), "packz");
        if ( getRuleLevel()==0 ) {dbg.commence();}
        incRuleLevel();
        dbg.location(29, 0);

        try {
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:30:4: (name= Identifier -> ^( Package[$packz.start,\"Pakkage\"+$superPackage] $name) )
            dbg.enterAlt(1);

            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:30:6: name= Identifier
            {
            dbg.location(30,10);
            name=(Token)match(input,Identifier,FOLLOW_Identifier_in_packz144);  
            stream_Identifier.add(name);


            // AST REWRITE
            // elements: name
            // token labels: name
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleTokenStream stream_name=new RewriteRuleTokenStream(adaptor,"token name",name);
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 30:22: -> ^( Package[$packz.start,\"Pakkage\"+$superPackage] $name)
            {
                dbg.location(30,25);
                // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:30:25: ^( Package[$packz.start,\"Pakkage\"+$superPackage] $name)
                {
                Object root_1 = (Object)adaptor.nil();
                dbg.location(30,27);
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(Package, ((Token)retval.start), "Pakkage"+superPackage)
                , root_1);

                dbg.location(30,73);
                adaptor.addChild(root_1,  superPackage );
                dbg.location(30,92);
                adaptor.addChild(root_1, stream_name.nextNode());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        dbg.location(31, 3);

        }
        finally {
            dbg.exitRule(getGrammarFileName(), "packz");
            decRuleLevel();
            if ( getRuleLevel()==0 ) {dbg.terminate();}
        }

        return retval;
    }
    // $ANTLR end "packz"


    public static class clazz_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "clazz"
    // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:33:1: clazz[Object superPackage] : name= Identifier '.class' -> ^( Class $name) ;
    public final RuleStructure1Parser.clazz_return clazz(Object superPackage) throws RecognitionException {
        RuleStructure1Parser.clazz_return retval = new RuleStructure1Parser.clazz_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token name=null;
        Token string_literal8=null;

        Object name_tree=null;
        Object string_literal8_tree=null;
        RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
        RewriteRuleTokenStream stream_12=new RewriteRuleTokenStream(adaptor,"token 12");

        try { dbg.enterRule(getGrammarFileName(), "clazz");
        if ( getRuleLevel()==0 ) {dbg.commence();}
        incRuleLevel();
        dbg.location(33, 0);

        try {
            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:34:4: (name= Identifier '.class' -> ^( Class $name) )
            dbg.enterAlt(1);

            // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:34:6: name= Identifier '.class'
            {
            dbg.location(34,10);
            name=(Token)match(input,Identifier,FOLLOW_Identifier_in_clazz174);  
            stream_Identifier.add(name);

            dbg.location(34,22);
            string_literal8=(Token)match(input,12,FOLLOW_12_in_clazz176);  
            stream_12.add(string_literal8);


            // AST REWRITE
            // elements: name
            // token labels: name
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleTokenStream stream_name=new RewriteRuleTokenStream(adaptor,"token name",name);
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 34:31: -> ^( Class $name)
            {
                dbg.location(34,34);
                // /Users/margarita/Documents/uni/Compiler Design/delorean/CD1_A2/src/cd/parser/CD1_A2b_examples/RuleStructure1.g:34:34: ^( Class $name)
                {
                Object root_1 = (Object)adaptor.nil();
                dbg.location(34,36);
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(Class, "Class")
                , root_1);

                dbg.location(34,42);
                adaptor.addChild(root_1,  superPackage );
                dbg.location(34,61);
                adaptor.addChild(root_1, stream_name.nextNode());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        dbg.location(35, 3);

        }
        finally {
            dbg.exitRule(getGrammarFileName(), "clazz");
            decRuleLevel();
            if ( getRuleLevel()==0 ) {dbg.terminate();}
        }

        return retval;
    }
    // $ANTLR end "clazz"

    // Delegated rules


 

    public static final BitSet FOLLOW_classPath_in_classes47 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_EOF_in_classes50 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_entity_in_classPath73 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_13_in_classPath82 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_entity_in_classPath84 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_packz_in_entity105 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_clazz_in_entity118 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Identifier_in_packz144 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Identifier_in_clazz174 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_clazz176 = new BitSet(new long[]{0x0000000000000002L});

}