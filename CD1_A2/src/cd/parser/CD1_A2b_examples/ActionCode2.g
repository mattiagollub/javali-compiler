grammar ActionCode2;

operation 
   : left=operand right=operand op=operator[$left.value, $right.value] { System.out.println($op.result); }
   ;
   
operator[int left, int right] returns [int result]
   : '+' { $result = $left + $right; } 
   | '-' { $result = $left - $right; } 
   ;
   
operand returns [int value]
   : num=Number {$value = Integer.valueOf($num.text); }
   ;

Number
   :	'0'
   |	'1'..'9' ('0'..'9')*
   ;

WS
	:	(' '|'\r'|'\t'|'\u000C'|'\n') {$channel=HIDDEN;}
	;

COMMENT
	:	'/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
	;

LINE_COMMENT
	:	'//' ~('\n'|'\r')*  ('\r\n' | '\r' | '\n')? {$channel=HIDDEN;}
	;