# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class A {...}
  # Emitting class B {...}
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $8, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var a
    mov %ebp, %edi
    add $-4, %edi
    push $8
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.A, %edx
    movl %edx, 0(%eax)
    mov %esi, (%edi)
    
    
    # Emitting assign
    
    # Emitting value of var a
    mov %ebp, %edi
    add $-4, %edi
    movl 0(%edi), %esi
    cmp $0, %esi
    jne label0
    push $4
    call _exit
label0:
    add $4, %esi
    
    movl $1, %edi
    mov %edi, (%esi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting a.foo
      
      # Emitting value of var a
      mov %ebp, %esi
      add $-4, %esi
      movl 0(%esi), %edi
      cmp $0, %edi
      jne label1
      push $4
      call _exit
label1:
      add $4, %edi
      movl 0(%edi), %edi
      
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    
    # Emitting assign
    # Emitting address of var a
    mov %ebp, %edi
    add $-4, %edi
    push $12
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.B, %edx
    movl %edx, 0(%eax)
    mov %esi, (%edi)
    
    
    # Emitting assign
    
    # Emitting value of var a
    mov %ebp, %edi
    add $-4, %edi
    movl 0(%edi), %esi
    cmp $0, %esi
    jne label2
    push $4
    call _exit
label2:
    add $4, %esi
    
    movl $2, %edi
    mov %edi, (%esi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting a.foo
      
      # Emitting value of var a
      mov %ebp, %esi
      add $-4, %esi
      movl 0(%esi), %edi
      cmp $0, %edi
      jne label3
      push $4
      call _exit
label3:
      add $4, %edi
      movl 0(%edi), %edi
      
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    
    # Emitting assign
    # Emitting address of var b
    mov %ebp, %edi
    add $-8, %edi
    push $12
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.B, %edx
    movl %edx, 0(%eax)
    mov %esi, (%edi)
    
    
    # Emitting assign
    
    # Emitting value of var b
    mov %ebp, %edi
    add $-8, %edi
    movl 0(%edi), %esi
    cmp $0, %esi
    jne label4
    push $4
    call _exit
label4:
    add $4, %esi
    
    movl $3, %edi
    mov %edi, (%esi)
    
    
    # Emitting assign
    
    # Emitting value of var b
    mov %ebp, %esi
    add $-8, %esi
    movl 0(%esi), %edi
    cmp $0, %edi
    jne label5
    push $4
    call _exit
label5:
    add $8, %edi
    
    movl $4, %esi
    mov %esi, (%edi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting b.foo
      
      # Emitting value of var b
      mov %ebp, %edi
      add $-8, %edi
      movl 0(%edi), %esi
      cmp $0, %esi
      jne label6
      push $4
      call _exit
label6:
      add $4, %esi
      movl 0(%esi), %esi
      
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting b.bar
      
      # Emitting value of var b
      mov %ebp, %edi
      add $-8, %edi
      movl 0(%edi), %esi
      cmp $0, %esi
      jne label7
      push $4
      call _exit
label7:
      add $8, %esi
      movl 0(%esi), %esi
      
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $8, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.Main, %edi
    movl %edi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %edi
  push %esi
  call *%edi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.A.arr:
  .long vTable.Object.arr
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.A:
  .long vTable.Object
vTable.int:
  .long -1
vTable.B:
  .long vTable.A
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.B.arr:
  .long vTable.A.arr
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
