# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $12, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var i0
    mov %ebp, %edi
    add $-8, %edi
    movl $5, %esi
    mov %esi, (%edi)
    
    
    # Emitting assign
    # Emitting address of var i1
    mov %ebp, %esi
    add $-12, %esi
    movl $2, %edi
    mov %edi, (%esi)
    
    
    # Emitting assign
    # Emitting address of var r1
    mov %ebp, %edi
    add $-4, %edi
    movl $6, %esi
    mov %esi, (%edi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting r1
      # Emitting value of var r1
      mov %ebp, %edi
      add $-4, %edi
      movl 0(%edi), %esi
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Emitting assign
    # Emitting address of var r1
    mov %ebp, %esi
    add $-4, %esi
    movl $10, %edi
    mov %edi, (%esi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting r1
      # Emitting value of var r1
      mov %ebp, %esi
      add $-4, %esi
      movl 0(%esi), %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Emitting assign
    # Emitting address of var r1
    mov %ebp, %edi
    add $-4, %edi
    movl $300, %esi
    mov %esi, (%edi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting r1
      # Emitting value of var r1
      mov %ebp, %edi
      add $-4, %edi
      movl 0(%edi), %esi
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $12, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.Main, %edi
    movl %edi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %edi
  push %esi
  call *%edi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.int:
  .long -1
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
