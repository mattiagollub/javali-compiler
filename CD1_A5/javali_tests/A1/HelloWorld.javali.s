# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting 222
      movl $222, %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %edi
    mov $vTable.Main, %esi
    movl %esi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %esi
  push %edi
  call *%esi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.int:
  .long -1
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
