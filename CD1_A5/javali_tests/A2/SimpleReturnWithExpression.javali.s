# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class Main {...}
    # Emitting int m(...) {...}
    # Declaration of method 'm'.
decl.Main.m:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
      # Emitting (a + b)
        # Emitting b
        # Emitting value of var b
        mov %ebp, %esi
        add $20, %esi
        movl 0(%esi), %edi
      push %edi
        # Emitting a
        # Emitting value of var a
        mov %ebp, %esi
        add $16, %esi
        movl 0(%esi), %edi
      pop %esi
      add %esi, %edi
      push %edi
label2:
      pop %edi
    push %edi
      # Emitting 1
      movl $1, %edi
    pop %esi
    add %edi, %esi
    push %esi
label0:
    pop %esi
    mov %ebp, %edi
    add $24, %edi
    mov %esi, (%edi)
    movl 0(%ebp), %esp
    add $-16, %esp
    jmp ret.Main.m
ret.Main.m:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $4, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var res
    mov %ebp, %edi
    add $-4, %edi
    movl $-1, %esi
    mov %esi, (%edi)
    
    
    # Emitting assign
    # Emitting address of var res
    mov %ebp, %esi
    add $-4, %esi
    # Emitting call to method m
    sub $4, %esp
      # Emitting 2
      movl $2, %edi
    push %edi
      # Emitting 1
      movl $1, %edi
    push %edi
      # Emitting this
      mov 12(%ebp), %edi
    cmp $0, %edi
    jne label4
    push $4
    call _exit
label4:
    push %edi
    movl 0(%edi), %edi
    movl 4(%edi), %edi
    call *%edi
    add $12, %esp
    pop %edi
    mov %edi, (%esi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting res
      # Emitting value of var res
      mov %ebp, %esi
      add $-4, %esi
      movl 0(%esi), %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Emitting assign
    # Emitting address of var res
    mov %ebp, %edi
    add $-4, %edi
    # Emitting call to method m
    sub $4, %esp
      # Emitting 2
      movl $2, %esi
    push %esi
      # Emitting 1
      movl $1, %esi
    push %esi
      # Emitting this
      mov 12(%ebp), %esi
    cmp $0, %esi
    jne label5
    push $4
    call _exit
label5:
    push %esi
    movl 0(%esi), %esi
    movl 4(%esi), %esi
    call *%esi
    add $12, %esp
    pop %esi
    mov %esi, (%edi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting res
      # Emitting value of var res
      mov %ebp, %edi
      add $-4, %edi
      movl 0(%edi), %esi
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting this.m(...)
      # Emitting call to method m
      sub $4, %esp
        # Emitting 2
        movl $2, %esi
      push %esi
        # Emitting 1
        movl $1, %esi
      push %esi
        # Emitting this
        mov 12(%ebp), %esi
      cmp $0, %esi
      jne label6
      push $4
      call _exit
label6:
      push %esi
      movl 0(%esi), %esi
      movl 4(%esi), %esi
      call *%esi
      add $12, %esp
      pop %esi
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting this.m(...)
      # Emitting call to method m
      sub $4, %esp
        # Emitting 2
        movl $2, %esi
      push %esi
        # Emitting 1
        movl $1, %esi
      push %esi
        # Emitting this
        mov 12(%ebp), %esi
      cmp $0, %esi
      jne label7
      push $4
      call _exit
label7:
      push %esi
      movl 0(%esi), %esi
      movl 4(%esi), %esi
      call *%esi
      add $12, %esp
      pop %esi
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $4, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.Main, %edi
    movl %edi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %edi
  push %esi
  call *%edi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.m
  .long decl.Main.main
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.int:
  .long -1
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
