# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class A {...}
    # Emitting void foo(...) {...}
    # Declaration of method 'foo'.
decl.A.foo:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting p
      # Emitting value of var p
      mov %ebp, %esi
      add $16, %esi
      movl 0(%esi), %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting i
      # Emitting value of var i
      mov 12(%ebp), %esi
      add $4, %esi
      movl 0(%esi), %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
ret.A.foo:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $4, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var a
    mov %ebp, %edi
    add $-4, %edi
    push $8
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.A, %edx
    movl %edx, 0(%eax)
    mov %esi, (%edi)
    
    
    # Emitting assign
    
    # Emitting value of var a
    mov %ebp, %edi
    add $-4, %edi
    movl 0(%edi), %esi
    cmp $0, %esi
    jne label0
    push $4
    call _exit
label0:
    add $4, %esi
    
    movl $10, %edi
    mov %edi, (%esi)
    
    # Emitting call to method foo
    sub $4, %esp
      # Emitting 1
      movl $1, %edi
    push %edi
      # Emitting a
      # Emitting value of var a
      mov %ebp, %esi
      add $-4, %esi
      movl 0(%esi), %edi
    cmp $0, %edi
    jne label1
    push $4
    call _exit
label1:
    push %edi
    movl 0(%edi), %edi
    movl 4(%edi), %edi
    call *%edi
    add $12, %esp
    # Emitting call to method foo
    sub $4, %esp
      # Emitting 2
      movl $2, %edi
    push %edi
      # Emitting a
      # Emitting value of var a
      mov %ebp, %esi
      add $-4, %esi
      movl 0(%esi), %edi
    cmp $0, %edi
    jne label2
    push $4
    call _exit
label2:
    push %edi
    movl 0(%edi), %edi
    movl 4(%edi), %edi
    call *%edi
    add $12, %esp
    # Emitting call to method foo
    sub $4, %esp
      # Emitting 3
      movl $3, %edi
    push %edi
      # Emitting a
      # Emitting value of var a
      mov %ebp, %esi
      add $-4, %esi
      movl 0(%esi), %edi
    cmp $0, %edi
    jne label3
    push $4
    call _exit
label3:
    push %edi
    movl 0(%edi), %edi
    movl 4(%edi), %edi
    call *%edi
    add $12, %esp
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $4, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %edi
    mov $vTable.Main, %esi
    movl %esi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %esi
  push %edi
  call *%esi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.A.arr:
  .long vTable.Object.arr
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.A:
  .long vTable.Object
  .long decl.A.foo
vTable.int:
  .long -1
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
