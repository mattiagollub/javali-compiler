# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $8, %esp
    push %ebx
    push %edi
    push %esi
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting 1
      movl $1, %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Emitting assign
    # Emitting address of var y
    mov %ebp, %edi
    add $-4, %edi
    push %eax
    push %ebx
    push %edx
    push $scanfbuf
    push $int_format
    call _scanf
    add $8, %esp
    pop %edx
    pop %ebx
    pop %eax
    mov scanfbuf, %esi
    mov %esi, (%edi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting (y + 1)
        # Emitting 1
        movl $1, %esi
      push %esi
        # Emitting y
        # Emitting value of var y
        mov %ebp, %edi
        add $-4, %edi
        movl 0(%edi), %esi
      pop %edi
      add %edi, %esi
      push %esi
label0:
      pop %esi
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Emitting assign
    # Emitting address of var x
    mov 12(%ebp), %esi
    add $4, %esi
    push %eax
    push %ebx
    push %edx
    push $scanfbuf
    push $int_format
    call _scanf
    add $8, %esp
    pop %edx
    pop %ebx
    pop %eax
    mov scanfbuf, %edi
    mov %edi, (%esi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting (x + 1)
        # Emitting 1
        movl $1, %edi
      push %edi
        # Emitting x
        # Emitting value of var x
        mov 12(%ebp), %esi
        add $4, %esi
        movl 0(%esi), %edi
      pop %esi
      add %esi, %edi
      push %edi
label2:
      pop %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Emitting assign
    # Emitting address of var arr
    mov %ebp, %edi
    add $-8, %edi
      # Emitting 55
      movl $55, %esi
    cmp $0, %esi
    jge label4
    push $5
    call _exit
label4:
    push %esi
    add $2, %esi
    imul $4, %esi
    push %esi
    call _malloc
    add $4, %esp
    mov %eax, %edx
    pop %esi
    push %edx
    mov $vTable.int.arr, %ecx
    movl %ecx, 0(%edx)
    add $4, %edx
    mov %esi, (%edx)
    pop %edx
    mov %edx, (%edi)
    
    
    # Emitting assign
    # Emitting value of var arr
    mov %ebp, %edi
    add $-8, %edi
    movl 0(%edi), %edx
    # Emitting value of var x
    mov 12(%ebp), %esi
    add $4, %esi
    movl 0(%esi), %edi
    push %edx
    add $4, %edx
    cmp %edi, (%edx)
    jle label6
    cmp $0, %edi
    jge label5
label6:
    push $3
    call _exit
label5:
    pop %edx
    add $2, %edi
    imul $4, %edi
    add %edi, %edx
    push %eax
    push %ebx
    push %edx
    push $scanfbuf
    push $int_format
    call _scanf
    add $8, %esp
    pop %edx
    pop %ebx
    pop %eax
    mov scanfbuf, %edi
    mov %edi, (%edx)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting (arr[x] + 1)
        # Emitting arr[x]
        # Emitting value of var arr
        mov %ebp, %edx
        add $-8, %edx
        movl 0(%edx), %edi
        # Emitting value of var x
        mov 12(%ebp), %esi
        add $4, %esi
        movl 0(%esi), %edx
        push %edi
        add $4, %edi
        cmp %edx, (%edi)
        jle label10
        cmp $0, %edx
        jge label9
label10:
        push $3
        call _exit
label9:
        pop %edi
        add $2, %edx
        imul $4, %edx
        add %edx, %edi
        movl 0(%edi), %edi
      push %edi
        # Emitting 1
        movl $1, %edi
      pop %edx
      add %edi, %edx
      push %edx
label7:
      pop %edx
    push %edx
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $8, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $8
    call _malloc
    add $4, %esp
    mov %eax, %edx
    mov $vTable.Main, %edi
    movl %edi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %edi
  push %edx
  call *%edi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.int:
  .long -1
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
