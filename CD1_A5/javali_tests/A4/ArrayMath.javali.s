# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $4, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var x
    mov 12(%ebp), %edi
    add $4, %edi
      # Emitting 3
      movl $3, %esi
    cmp $0, %esi
    jge label0
    push $5
    call _exit
label0:
    push %esi
    add $2, %esi
    imul $4, %esi
    push %esi
    call _malloc
    add $4, %esp
    mov %eax, %edx
    pop %esi
    push %edx
    mov $vTable.int.arr, %ecx
    movl %ecx, 0(%edx)
    add $4, %edx
    mov %esi, (%edx)
    pop %edx
    mov %edx, (%edi)
    
    
    # Emitting assign
    # Emitting value of var x
    mov 12(%ebp), %edi
    add $4, %edi
    movl 0(%edi), %edx
    movl $0, %edi
    push %edx
    add $4, %edx
    cmp %edi, (%edx)
    jle label2
    cmp $0, %edi
    jge label1
label2:
    push $3
    call _exit
label1:
    pop %edx
    add $2, %edi
    imul $4, %edi
    add %edi, %edx
    movl $3, %edi
    mov %edi, (%edx)
    
    
    # Emitting assign
    # Emitting value of var x
    mov 12(%ebp), %edx
    add $4, %edx
    movl 0(%edx), %edi
    movl $1, %edx
    push %edi
    add $4, %edi
    cmp %edx, (%edi)
    jle label4
    cmp $0, %edx
    jge label3
label4:
    push $3
    call _exit
label3:
    pop %edi
    add $2, %edx
    imul $4, %edx
    add %edx, %edi
    movl $4, %edx
    mov %edx, (%edi)
    
    
    # Emitting assign
    # Emitting value of var x
    mov 12(%ebp), %edi
    add $4, %edi
    movl 0(%edi), %edx
    movl $2, %edi
    push %edx
    add $4, %edx
    cmp %edi, (%edx)
    jle label6
    cmp $0, %edi
    jge label5
label6:
    push $3
    call _exit
label5:
    pop %edx
    add $2, %edi
    imul $4, %edi
    add %edi, %edx
    movl $5, %edi
    mov %edi, (%edx)
    
    
    # Emitting assign
    # Emitting address of var i
    mov %ebp, %edi
    add $-4, %edi
      # Emitting (x[0] + x[1])
        # Emitting x[1]
        # Emitting value of var x
        mov 12(%ebp), %esi
        add $4, %esi
        movl 0(%esi), %edx
        movl $1, %esi
        push %edx
        add $4, %edx
        cmp %esi, (%edx)
        jle label12
        cmp $0, %esi
        jge label11
label12:
        push $3
        call _exit
label11:
        pop %edx
        add $2, %esi
        imul $4, %esi
        add %esi, %edx
        movl 0(%edx), %edx
      push %edx
        # Emitting x[0]
        # Emitting value of var x
        mov 12(%ebp), %esi
        add $4, %esi
        movl 0(%esi), %edx
        movl $0, %esi
        push %edx
        add $4, %edx
        cmp %esi, (%edx)
        jle label14
        cmp $0, %esi
        jge label13
label14:
        push $3
        call _exit
label13:
        pop %edx
        add $2, %esi
        imul $4, %esi
        add %esi, %edx
        movl 0(%edx), %edx
      pop %esi
      add %esi, %edx
      push %edx
label9:
      pop %edx
    push %edx
      # Emitting x[2]
      # Emitting value of var x
      mov 12(%ebp), %esi
      add $4, %esi
      movl 0(%esi), %edx
      movl $2, %esi
      push %edx
      add $4, %edx
      cmp %esi, (%edx)
      jle label16
      cmp $0, %esi
      jge label15
label16:
      push $3
      call _exit
label15:
      pop %edx
      add $2, %esi
      imul $4, %esi
      add %esi, %edx
      movl 0(%edx), %edx
    pop %esi
    add %edx, %esi
    push %esi
label7:
    pop %esi
    mov %esi, (%edi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting i
      # Emitting value of var i
      mov %ebp, %edi
      add $-4, %edi
      movl 0(%edi), %esi
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $4, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $8
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.Main, %edi
    movl %edi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %edi
  push %esi
  call *%edi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.int:
  .long -1
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
