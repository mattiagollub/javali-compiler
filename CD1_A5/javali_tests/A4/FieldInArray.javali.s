# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class A {...}
    # Emitting void foo(...) {...}
    # Declaration of method 'foo'.
decl.A.foo:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting 1
      movl $1, %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting field
      # Emitting value of var field
      mov 12(%ebp), %esi
      add $4, %esi
      movl 0(%esi), %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
ret.A.foo:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $4, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var x
    mov 12(%ebp), %edi
    add $4, %edi
      # Emitting 2
      movl $2, %esi
    cmp $0, %esi
    jge label0
    push $5
    call _exit
label0:
    push %esi
    add $2, %esi
    imul $4, %esi
    push %esi
    call _malloc
    add $4, %esp
    mov %eax, %edx
    pop %esi
    push %edx
    mov $vTable.A.arr, %ecx
    movl %ecx, 0(%edx)
    add $4, %edx
    mov %esi, (%edx)
    pop %edx
    mov %edx, (%edi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting 1
      movl $1, %edx
    push %edx
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Emitting assign
    # Emitting value of var x
    mov 12(%ebp), %edi
    add $4, %edi
    movl 0(%edi), %edx
    movl $1, %edi
    push %edx
    add $4, %edx
    cmp %edi, (%edx)
    jle label2
    cmp $0, %edi
    jge label1
label2:
    push $3
    call _exit
label1:
    pop %edx
    add $2, %edi
    imul $4, %edi
    add %edi, %edx
    push %edx
    push $8
    call _malloc
    add $4, %esp
    mov %eax, %edi
    mov $vTable.A, %esi
    movl %esi, 0(%eax)
    pop %edx
    mov %edi, (%edx)
    
    
    # Emitting assign
    
    # Emitting value of var x
    mov 12(%ebp), %edx
    add $4, %edx
    movl 0(%edx), %edi
    movl $1, %edx
    push %edi
    add $4, %edi
    cmp %edx, (%edi)
    jle label4
    cmp $0, %edx
    jge label3
label4:
    push $3
    call _exit
label3:
    pop %edi
    add $2, %edx
    imul $4, %edx
    add %edx, %edi
    movl 0(%edi), %edi
    cmp $0, %edi
    jne label5
    push $4
    call _exit
label5:
    add $4, %edi
    
    movl $2, %edx
    mov %edx, (%edi)
    
    
    # Emitting assign
    # Emitting address of var i
    mov %ebp, %edx
    add $-4, %edx
    
    # Emitting value of var x
    mov 12(%ebp), %esi
    add $4, %esi
    movl 0(%esi), %edi
    movl $1, %esi
    push %edi
    add $4, %edi
    cmp %esi, (%edi)
    jle label7
    cmp $0, %esi
    jge label6
label7:
    push $3
    call _exit
label6:
    pop %edi
    add $2, %esi
    imul $4, %esi
    add %esi, %edi
    movl 0(%edi), %edi
    cmp $0, %edi
    jne label8
    push $4
    call _exit
label8:
    add $4, %edi
    movl 0(%edi), %edi
    
    mov %edi, (%edx)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting i
      # Emitting value of var i
      mov %ebp, %edx
      add $-4, %edx
      movl 0(%edx), %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $4, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $8
    call _malloc
    add $4, %esp
    mov %eax, %edi
    mov $vTable.Main, %edx
    movl %edx, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %edx
  push %edi
  call *%edx
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.A.arr:
  .long vTable.Object.arr
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.A:
  .long vTable.Object
  .long decl.A.foo
vTable.int:
  .long -1
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
