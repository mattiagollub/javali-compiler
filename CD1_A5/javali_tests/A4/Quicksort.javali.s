# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class Record {...}
  # Emitting class Main {...}
    # Emitting void swap(...) {...}
    # Declaration of method 'swap'.
decl.Main.swap:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $4, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var temp
    mov %ebp, %edi
    add $-4, %edi
    
    # Emitting value of var r1
    mov %ebp, %edx
    add $16, %edx
    movl 0(%edx), %esi
    cmp $0, %esi
    jne label0
    push $4
    call _exit
label0:
    add $4, %esi
    movl 0(%esi), %esi
    
    mov %esi, (%edi)
    
    
    # Emitting assign
    
    # Emitting value of var r1
    mov %ebp, %edi
    add $16, %edi
    movl 0(%edi), %esi
    cmp $0, %esi
    jne label1
    push $4
    call _exit
label1:
    add $4, %esi
    
    
    # Emitting value of var r2
    mov %ebp, %edx
    add $20, %edx
    movl 0(%edx), %edi
    cmp $0, %edi
    jne label2
    push $4
    call _exit
label2:
    add $4, %edi
    movl 0(%edi), %edi
    
    mov %edi, (%esi)
    
    
    # Emitting assign
    
    # Emitting value of var r2
    mov %ebp, %esi
    add $20, %esi
    movl 0(%esi), %edi
    cmp $0, %edi
    jne label3
    push $4
    call _exit
label3:
    add $4, %edi
    
    # Emitting value of var temp
    mov %ebp, %edx
    add $-4, %edx
    movl 0(%edx), %esi
    mov %esi, (%edi)
    
ret.Main.swap:
    pop %esi
    pop %edi
    pop %ebx
    add $4, %esp
    pop %esp
    pop %ebp
    ret
    
    # Emitting void sort(...) {...}
    # Declaration of method 'sort'.
decl.Main.sort:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $12, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var m
    mov %ebp, %edi
    add $-12, %edi
      # Emitting (a[left].a + a[right].a)
        # Emitting a[right].a
        
        # Emitting value of var a
        mov 12(%ebp), %edx
        add $4, %edx
        movl 0(%edx), %esi
        # Emitting value of var right
        mov %ebp, %ecx
        add $20, %ecx
        movl 0(%ecx), %edx
        push %esi
        add $4, %esi
        cmp %edx, (%esi)
        jle label9
        cmp $0, %edx
        jge label8
label9:
        push $3
        call _exit
label8:
        pop %esi
        add $2, %edx
        imul $4, %edx
        add %edx, %esi
        movl 0(%esi), %esi
        cmp $0, %esi
        jne label10
        push $4
        call _exit
label10:
        add $4, %esi
        movl 0(%esi), %esi
        
      push %esi
        # Emitting a[left].a
        
        # Emitting value of var a
        mov 12(%ebp), %edx
        add $4, %edx
        movl 0(%edx), %esi
        # Emitting value of var left
        mov %ebp, %ecx
        add $16, %ecx
        movl 0(%ecx), %edx
        push %esi
        add $4, %esi
        cmp %edx, (%esi)
        jle label12
        cmp $0, %edx
        jge label11
label12:
        push $3
        call _exit
label11:
        pop %esi
        add $2, %edx
        imul $4, %edx
        add %edx, %esi
        movl 0(%esi), %esi
        cmp $0, %esi
        jne label13
        push $4
        call _exit
label13:
        add $4, %esi
        movl 0(%esi), %esi
        
      pop %edx
      add %edx, %esi
      push %esi
label6:
      pop %esi
    push %esi
      # Emitting 2
      movl $2, %esi
    pop %edx
    push %eax
    push %ebx
    push %edx
    push %esi
    push %edx
    pop %eax
    pop %ebx
    cmp $0, %ebx
    jne label14
    push $8
    call _exit
label14:
    xor %edx, %edx
    idiv %ebx
    pop %edx
    pop %ebx
    mov %eax, %edx
    pop %eax
    push %edx
label4:
    pop %edx
    mov %edx, (%edi)
    
    
    # Emitting assign
    # Emitting address of var i
    mov %ebp, %edx
    add $-4, %edx
    # Emitting value of var left
    mov %ebp, %esi
    add $16, %esi
    movl 0(%esi), %edi
    mov %edi, (%edx)
    
    
    # Emitting assign
    # Emitting address of var j
    mov %ebp, %edi
    add $-8, %edi
    # Emitting value of var right
    mov %ebp, %esi
    add $20, %esi
    movl 0(%esi), %edx
    mov %edx, (%edi)
    
label15:
      # Emitting (i <= j)
        # Emitting j
        # Emitting value of var j
        mov %ebp, %edi
        add $-8, %edi
        movl 0(%edi), %edx
      push %edx
        # Emitting i
        # Emitting value of var i
        mov %ebp, %edi
        add $-4, %edi
        movl 0(%edi), %edx
      pop %edi
      cmp %edi, %edx
      jle label20
      mov $0, %edx
      jmp label19
label20:
      mov $1, %edx
label19:
      push %edx
label17:
      pop %edx
    cmp $1, %edx
    jne label16
      # Emitting (...)
        # Emitting while ((a[i].a < m)) {...}
label21:
          # Emitting (a[i].a < m)
            # Emitting a[i].a
            
            # Emitting value of var a
            mov 12(%ebp), %esi
            add $4, %esi
            movl 0(%esi), %edi
            # Emitting value of var i
            mov %ebp, %ecx
            add $-4, %ecx
            movl 0(%ecx), %esi
            push %edi
            add $4, %edi
            cmp %esi, (%edi)
            jle label26
            cmp $0, %esi
            jge label25
label26:
            push $3
            call _exit
label25:
            pop %edi
            add $2, %esi
            imul $4, %esi
            add %esi, %edi
            movl 0(%edi), %edi
            cmp $0, %edi
            jne label27
            push $4
            call _exit
label27:
            add $4, %edi
            movl 0(%edi), %edi
            
          push %edi
            # Emitting m
            # Emitting value of var m
            mov %ebp, %esi
            add $-12, %esi
            movl 0(%esi), %edi
          pop %esi
          cmp %edi, %esi
          jl label29
          mov $0, %esi
          jmp label28
label29:
          mov $1, %esi
label28:
          push %esi
label23:
          pop %esi
        cmp $1, %esi
        jne label22
          # Emitting (...)
            # Emitting i = (i + 1)
            
            # Emitting assign
            # Emitting address of var i
            mov %ebp, %edi
            add $-4, %edi
              # Emitting 1
              movl $1, %ecx
            push %ecx
              # Emitting i
              # Emitting value of var i
              mov %ebp, %ebx
              add $-4, %ebx
              movl 0(%ebx), %ecx
            pop %ebx
            add %ebx, %ecx
            push %ecx
label30:
            pop %ecx
            mov %ecx, (%edi)
            
        jmp label21
label22:
        # Emitting while ((a[j].a > m)) {...}
label32:
          # Emitting (a[j].a > m)
            # Emitting a[j].a
            
            # Emitting value of var a
            mov 12(%ebp), %ecx
            add $4, %ecx
            movl 0(%ecx), %esi
            # Emitting value of var j
            mov %ebp, %edi
            add $-8, %edi
            movl 0(%edi), %ecx
            push %esi
            add $4, %esi
            cmp %ecx, (%esi)
            jle label37
            cmp $0, %ecx
            jge label36
label37:
            push $3
            call _exit
label36:
            pop %esi
            add $2, %ecx
            imul $4, %ecx
            add %ecx, %esi
            movl 0(%esi), %esi
            cmp $0, %esi
            jne label38
            push $4
            call _exit
label38:
            add $4, %esi
            movl 0(%esi), %esi
            
          push %esi
            # Emitting m
            # Emitting value of var m
            mov %ebp, %ecx
            add $-12, %ecx
            movl 0(%ecx), %esi
          pop %ecx
          cmp %esi, %ecx
          jg label40
          mov $0, %ecx
          jmp label39
label40:
          mov $1, %ecx
label39:
          push %ecx
label34:
          pop %ecx
        cmp $1, %ecx
        jne label33
          # Emitting (...)
            # Emitting j = (j - 1)
            
            # Emitting assign
            # Emitting address of var j
            mov %ebp, %esi
            add $-8, %esi
              # Emitting 1
              movl $1, %edi
            push %edi
              # Emitting j
              # Emitting value of var j
              mov %ebp, %ebx
              add $-8, %ebx
              movl 0(%ebx), %edi
            pop %ebx
            sub %ebx, %edi
            push %edi
label41:
            pop %edi
            mov %edi, (%esi)
            
        jmp label32
label33:
        # Emitting if ((i <= j)) {...} else {...}
          # Emitting (i <= j)
            # Emitting j
            # Emitting value of var j
            mov %ebp, %edi
            add $-8, %edi
            movl 0(%edi), %ecx
          push %ecx
            # Emitting i
            # Emitting value of var i
            mov %ebp, %edi
            add $-4, %edi
            movl 0(%edi), %ecx
          pop %edi
          cmp %edi, %ecx
          jle label46
          mov $0, %ecx
          jmp label45
label46:
          mov $1, %ecx
label45:
          push %ecx
label43:
          pop %ecx
        cmp $1, %ecx
        jne label47
          # Emitting (...)
            # Emitting this.swap(...)
            # Emitting call to method swap
            push %edx
            sub $4, %esp
              # Emitting a[j]
              # Emitting value of var a
              mov 12(%ebp), %edi
              add $4, %edi
              movl 0(%edi), %ecx
              # Emitting value of var j
              mov %ebp, %esi
              add $-8, %esi
              movl 0(%esi), %edi
              push %ecx
              add $4, %ecx
              cmp %edi, (%ecx)
              jle label50
              cmp $0, %edi
              jge label49
label50:
              push $3
              call _exit
label49:
              pop %ecx
              add $2, %edi
              imul $4, %edi
              add %edi, %ecx
              movl 0(%ecx), %ecx
            push %ecx
              # Emitting a[i]
              # Emitting value of var a
              mov 12(%ebp), %edi
              add $4, %edi
              movl 0(%edi), %ecx
              # Emitting value of var i
              mov %ebp, %esi
              add $-4, %esi
              movl 0(%esi), %edi
              push %ecx
              add $4, %ecx
              cmp %edi, (%ecx)
              jle label52
              cmp $0, %edi
              jge label51
label52:
              push $3
              call _exit
label51:
              pop %ecx
              add $2, %edi
              imul $4, %edi
              add %edi, %ecx
              movl 0(%ecx), %ecx
            push %ecx
              # Emitting this
              mov 12(%ebp), %ecx
            cmp $0, %ecx
            jne label53
            push $4
            call _exit
label53:
            push %ecx
            movl 0(%ecx), %ecx
            movl 8(%ecx), %ecx
            call *%ecx
            add $16, %esp
            pop %edx
            # Emitting i = (i + 1)
            
            # Emitting assign
            # Emitting address of var i
            mov %ebp, %ecx
            add $-4, %ecx
              # Emitting 1
              movl $1, %edi
            push %edi
              # Emitting i
              # Emitting value of var i
              mov %ebp, %esi
              add $-4, %esi
              movl 0(%esi), %edi
            pop %esi
            add %esi, %edi
            push %edi
label54:
            pop %edi
            mov %edi, (%ecx)
            
            # Emitting j = (j - 1)
            
            # Emitting assign
            # Emitting address of var j
            mov %ebp, %edi
            add $-8, %edi
              # Emitting 1
              movl $1, %ecx
            push %ecx
              # Emitting j
              # Emitting value of var j
              mov %ebp, %esi
              add $-8, %esi
              movl 0(%esi), %ecx
            pop %esi
            sub %esi, %ecx
            push %ecx
label56:
            pop %ecx
            mov %ecx, (%edi)
            
        jmp label48
label47:
          # Emitting nop
label48:
    jmp label15
label16:
      # Emitting (left < j)
        # Emitting j
        # Emitting value of var j
        mov %ebp, %ecx
        add $-8, %ecx
        movl 0(%ecx), %edx
      push %edx
        # Emitting left
        # Emitting value of var left
        mov %ebp, %ecx
        add $16, %ecx
        movl 0(%ecx), %edx
      pop %ecx
      cmp %ecx, %edx
      jl label61
      mov $0, %edx
      jmp label60
label61:
      mov $1, %edx
label60:
      push %edx
label58:
      pop %edx
    cmp $1, %edx
    jne label62
      # Emitting (...)
        # Emitting this.sort(...)
        # Emitting call to method sort
        sub $4, %esp
          # Emitting j
          # Emitting value of var j
          mov %ebp, %ecx
          add $-8, %ecx
          movl 0(%ecx), %edx
        push %edx
          # Emitting left
          # Emitting value of var left
          mov %ebp, %ecx
          add $16, %ecx
          movl 0(%ecx), %edx
        push %edx
          # Emitting this
          mov 12(%ebp), %edx
        cmp $0, %edx
        jne label64
        push $4
        call _exit
label64:
        push %edx
        movl 0(%edx), %edx
        movl 4(%edx), %edx
        call *%edx
        add $16, %esp
    jmp label63
label62:
      # Emitting nop
label63:
      # Emitting (i < right)
        # Emitting right
        # Emitting value of var right
        mov %ebp, %ecx
        add $20, %ecx
        movl 0(%ecx), %edx
      push %edx
        # Emitting i
        # Emitting value of var i
        mov %ebp, %ecx
        add $-4, %ecx
        movl 0(%ecx), %edx
      pop %ecx
      cmp %ecx, %edx
      jl label68
      mov $0, %edx
      jmp label67
label68:
      mov $1, %edx
label67:
      push %edx
label65:
      pop %edx
    cmp $1, %edx
    jne label69
      # Emitting (...)
        # Emitting this.sort(...)
        # Emitting call to method sort
        sub $4, %esp
          # Emitting right
          # Emitting value of var right
          mov %ebp, %ecx
          add $20, %ecx
          movl 0(%ecx), %edx
        push %edx
          # Emitting i
          # Emitting value of var i
          mov %ebp, %ecx
          add $-4, %ecx
          movl 0(%ecx), %edx
        push %edx
          # Emitting this
          mov 12(%ebp), %edx
        cmp $0, %edx
        jne label71
        push $4
        call _exit
label71:
        push %edx
        movl 0(%edx), %edx
        movl 4(%edx), %edx
        call *%edx
        add $16, %esp
    jmp label70
label69:
      # Emitting nop
label70:
ret.Main.sort:
    pop %esi
    pop %edi
    pop %ebx
    add $12, %esp
    pop %esp
    pop %ebp
    ret
    
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $8, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var SIZE
    mov %ebp, %edi
    add $-4, %edi
    movl $5, %esi
    mov %esi, (%edi)
    
    
    # Emitting assign
    # Emitting address of var a
    mov 12(%ebp), %esi
    add $4, %esi
      # Emitting SIZE
      # Emitting value of var SIZE
      mov %ebp, %edx
      add $-4, %edx
      movl 0(%edx), %edi
    cmp $0, %edi
    jge label72
    push $5
    call _exit
label72:
    push %edi
    add $2, %edi
    imul $4, %edi
    push %edi
    call _malloc
    add $4, %esp
    mov %eax, %edx
    pop %edi
    push %edx
    mov $vTable.Record.arr, %ecx
    movl %ecx, 0(%edx)
    add $4, %edx
    mov %edi, (%edx)
    pop %edx
    mov %edx, (%esi)
    
    
    # Emitting assign
    # Emitting address of var j
    mov %ebp, %edx
    add $-8, %edx
    movl $0, %esi
    mov %esi, (%edx)
    
label73:
      # Emitting (j < SIZE)
        # Emitting SIZE
        # Emitting value of var SIZE
        mov %ebp, %edx
        add $-4, %edx
        movl 0(%edx), %esi
      push %esi
        # Emitting j
        # Emitting value of var j
        mov %ebp, %edx
        add $-8, %edx
        movl 0(%edx), %esi
      pop %edx
      cmp %edx, %esi
      jl label78
      mov $0, %esi
      jmp label77
label78:
      mov $1, %esi
label77:
      push %esi
label75:
      pop %esi
    cmp $1, %esi
    jne label74
      # Emitting (...)
        # Emitting a[j] = new Record()
        
        # Emitting assign
        # Emitting value of var a
        mov 12(%ebp), %edi
        add $4, %edi
        movl 0(%edi), %edx
        # Emitting value of var j
        mov %ebp, %ecx
        add $-8, %ecx
        movl 0(%ecx), %edi
        push %edx
        add $4, %edx
        cmp %edi, (%edx)
        jle label80
        cmp $0, %edi
        jge label79
label80:
        push $3
        call _exit
label79:
        pop %edx
        add $2, %edi
        imul $4, %edi
        add %edi, %edx
        push %edx
        push $8
        call _malloc
        add $4, %esp
        mov %eax, %edi
        mov $vTable.Record, %ecx
        movl %ecx, 0(%eax)
        pop %edx
        mov %edi, (%edx)
        
        # Emitting j = (j + 1)
        
        # Emitting assign
        # Emitting address of var j
        mov %ebp, %edi
        add $-8, %edi
          # Emitting 1
          movl $1, %edx
        push %edx
          # Emitting j
          # Emitting value of var j
          mov %ebp, %ecx
          add $-8, %ecx
          movl 0(%ecx), %edx
        pop %ecx
        add %ecx, %edx
        push %edx
label81:
        pop %edx
        mov %edx, (%edi)
        
    jmp label73
label74:
    
    # Emitting assign
    
    # Emitting value of var a
    mov 12(%ebp), %edx
    add $4, %edx
    movl 0(%edx), %esi
    movl $0, %edx
    push %esi
    add $4, %esi
    cmp %edx, (%esi)
    jle label84
    cmp $0, %edx
    jge label83
label84:
    push $3
    call _exit
label83:
    pop %esi
    add $2, %edx
    imul $4, %edx
    add %edx, %esi
    movl 0(%esi), %esi
    cmp $0, %esi
    jne label85
    push $4
    call _exit
label85:
    add $4, %esi
    
    movl $5, %edx
    mov %edx, (%esi)
    
    
    # Emitting assign
    
    # Emitting value of var a
    mov 12(%ebp), %esi
    add $4, %esi
    movl 0(%esi), %edx
    movl $1, %esi
    push %edx
    add $4, %edx
    cmp %esi, (%edx)
    jle label87
    cmp $0, %esi
    jge label86
label87:
    push $3
    call _exit
label86:
    pop %edx
    add $2, %esi
    imul $4, %esi
    add %esi, %edx
    movl 0(%edx), %edx
    cmp $0, %edx
    jne label88
    push $4
    call _exit
label88:
    add $4, %edx
    
    movl $3, %esi
    mov %esi, (%edx)
    
    
    # Emitting assign
    
    # Emitting value of var a
    mov 12(%ebp), %edx
    add $4, %edx
    movl 0(%edx), %esi
    movl $2, %edx
    push %esi
    add $4, %esi
    cmp %edx, (%esi)
    jle label90
    cmp $0, %edx
    jge label89
label90:
    push $3
    call _exit
label89:
    pop %esi
    add $2, %edx
    imul $4, %edx
    add %edx, %esi
    movl 0(%esi), %esi
    cmp $0, %esi
    jne label91
    push $4
    call _exit
label91:
    add $4, %esi
    
    movl $1, %edx
    mov %edx, (%esi)
    
    
    # Emitting assign
    
    # Emitting value of var a
    mov 12(%ebp), %esi
    add $4, %esi
    movl 0(%esi), %edx
    movl $3, %esi
    push %edx
    add $4, %edx
    cmp %esi, (%edx)
    jle label93
    cmp $0, %esi
    jge label92
label93:
    push $3
    call _exit
label92:
    pop %edx
    add $2, %esi
    imul $4, %esi
    add %esi, %edx
    movl 0(%edx), %edx
    cmp $0, %edx
    jne label94
    push $4
    call _exit
label94:
    add $4, %edx
    
    movl $4, %esi
    mov %esi, (%edx)
    
    
    # Emitting assign
    
    # Emitting value of var a
    mov 12(%ebp), %edx
    add $4, %edx
    movl 0(%edx), %esi
    movl $4, %edx
    push %esi
    add $4, %esi
    cmp %edx, (%esi)
    jle label96
    cmp $0, %edx
    jge label95
label96:
    push $3
    call _exit
label95:
    pop %esi
    add $2, %edx
    imul $4, %edx
    add %edx, %esi
    movl 0(%esi), %esi
    cmp $0, %esi
    jne label97
    push $4
    call _exit
label97:
    add $4, %esi
    
    movl $2, %edx
    mov %edx, (%esi)
    
    # Emitting call to method sort
    sub $4, %esp
      # Emitting 4
      movl $4, %edx
    push %edx
      # Emitting 0
      movl $0, %edx
    push %edx
      # Emitting this
      mov 12(%ebp), %edx
    cmp $0, %edx
    jne label98
    push $4
    call _exit
label98:
    push %edx
    movl 0(%edx), %edx
    movl 4(%edx), %edx
    call *%edx
    add $16, %esp
    
    # Emitting assign
    # Emitting address of var i
    mov 12(%ebp), %edx
    add $8, %edx
    
    # Emitting value of var a
    mov 12(%ebp), %edi
    add $4, %edi
    movl 0(%edi), %esi
    movl $3, %edi
    push %esi
    add $4, %esi
    cmp %edi, (%esi)
    jle label100
    cmp $0, %edi
    jge label99
label100:
    push $3
    call _exit
label99:
    pop %esi
    add $2, %edi
    imul $4, %edi
    add %edi, %esi
    movl 0(%esi), %esi
    cmp $0, %esi
    jne label101
    push $4
    call _exit
label101:
    add $4, %esi
    movl 0(%esi), %esi
    
    mov %esi, (%edx)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting i
      # Emitting value of var i
      mov 12(%ebp), %edx
      add $8, %edx
      movl 0(%edx), %esi
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $8, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $12
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.Main, %edx
    movl %edx, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %edx
  push %esi
  call *%edx
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.sort
  .long decl.Main.swap
  .long decl.Main.main
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.Record.arr:
  .long vTable.Object.arr
vTable.Record:
  .long vTable.Object
vTable.int:
  .long -1
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
