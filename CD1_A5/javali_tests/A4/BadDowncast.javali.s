# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class A {...}
  # Emitting class B {...}
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $8, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var a
    mov %ebp, %edi
    add $-4, %edi
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.A, %edx
    movl %edx, 0(%eax)
    mov %esi, (%edi)
    
    
    # Emitting assign
    # Emitting address of var b
    mov %ebp, %esi
    add $-8, %esi
    # Emitting value of var a
    mov %ebp, %edx
    add $-4, %edx
    movl 0(%edx), %edi
    push %edi
    cmp $0, %edi
    je label1
    cmp $vTable.B, %edi
    je label1
    cmp $-1, %edi
    je exit
label0:
    mov (%edi), %edi
    cmp $vTable.B, %edi
    je label1
    cmp $-1, %edi
    jne label0
label2:
    push $1
    call _exit
label1:
    pop %edi
    mov %edi, (%esi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting 0
      movl $0, %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $8, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %edi
    mov $vTable.Main, %esi
    movl %esi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %esi
  push %edi
  call *%esi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.A.arr:
  .long vTable.Object.arr
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.A:
  .long vTable.Object
vTable.int:
  .long -1
vTable.B:
  .long vTable.A
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.B.arr:
  .long vTable.A.arr
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
