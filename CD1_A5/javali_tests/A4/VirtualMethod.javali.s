# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class A {...}
    # Emitting void override(...) {...}
    # Declaration of method 'override'.
decl.A.override:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting 0
      movl $0, %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
ret.A.override:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
    # Emitting void base(...) {...}
    # Declaration of method 'base'.
decl.A.base:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting 1
      movl $1, %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
ret.A.base:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
  # Emitting class B {...}
    # Emitting void override(...) {...}
    # Declaration of method 'override'.
decl.B.override:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting 2
      movl $2, %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
ret.B.override:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
    # Emitting void sub(...) {...}
    # Declaration of method 'sub'.
decl.B.sub:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting 3
      movl $3, %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
ret.B.sub:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $8, %esp
    push %ebx
    push %edi
    push %esi
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Emitting assign
    # Emitting address of var a
    mov %ebp, %edi
    add $-4, %edi
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.A, %edx
    movl %edx, 0(%eax)
    mov %esi, (%edi)
    
    # Emitting call to method base
    sub $4, %esp
      # Emitting a
      # Emitting value of var a
      mov %ebp, %edi
      add $-4, %edi
      movl 0(%edi), %esi
    cmp $0, %esi
    jne label0
    push $4
    call _exit
label0:
    push %esi
    movl 0(%esi), %esi
    movl 8(%esi), %esi
    call *%esi
    add $8, %esp
    # Emitting call to method override
    sub $4, %esp
      # Emitting a
      # Emitting value of var a
      mov %ebp, %edi
      add $-4, %edi
      movl 0(%edi), %esi
    cmp $0, %esi
    jne label1
    push $4
    call _exit
label1:
    push %esi
    movl 0(%esi), %esi
    movl 4(%esi), %esi
    call *%esi
    add $8, %esp
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Emitting assign
    # Emitting address of var b
    mov %ebp, %esi
    add $-8, %esi
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %edi
    mov $vTable.B, %edx
    movl %edx, 0(%eax)
    mov %edi, (%esi)
    
    # Emitting call to method base
    sub $4, %esp
      # Emitting b
      # Emitting value of var b
      mov %ebp, %esi
      add $-8, %esi
      movl 0(%esi), %edi
    cmp $0, %edi
    jne label2
    push $4
    call _exit
label2:
    push %edi
    movl 0(%edi), %edi
    movl 8(%edi), %edi
    call *%edi
    add $8, %esp
    # Emitting call to method override
    sub $4, %esp
      # Emitting b
      # Emitting value of var b
      mov %ebp, %esi
      add $-8, %esi
      movl 0(%esi), %edi
    cmp $0, %edi
    jne label3
    push $4
    call _exit
label3:
    push %edi
    movl 0(%edi), %edi
    movl 4(%edi), %edi
    call *%edi
    add $8, %esp
    # Emitting call to method sub
    sub $4, %esp
      # Emitting b
      # Emitting value of var b
      mov %ebp, %esi
      add $-8, %esi
      movl 0(%esi), %edi
    cmp $0, %edi
    jne label4
    push $4
    call _exit
label4:
    push %edi
    movl 0(%edi), %edi
    movl 12(%edi), %edi
    call *%edi
    add $8, %esp
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Emitting assign
    # Emitting address of var a
    mov %ebp, %edi
    add $-4, %edi
    # Emitting value of var b
    mov %ebp, %edx
    add $-8, %edx
    movl 0(%edx), %esi
    mov %esi, (%edi)
    
    # Emitting call to method base
    sub $4, %esp
      # Emitting a
      # Emitting value of var a
      mov %ebp, %edi
      add $-4, %edi
      movl 0(%edi), %esi
    cmp $0, %esi
    jne label5
    push $4
    call _exit
label5:
    push %esi
    movl 0(%esi), %esi
    movl 8(%esi), %esi
    call *%esi
    add $8, %esp
    # Emitting call to method override
    sub $4, %esp
      # Emitting a
      # Emitting value of var a
      mov %ebp, %edi
      add $-4, %edi
      movl 0(%edi), %esi
    cmp $0, %esi
    jne label6
    push $4
    call _exit
label6:
    push %esi
    movl 0(%esi), %esi
    movl 4(%esi), %esi
    call *%esi
    add $8, %esp
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $8, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.Main, %edi
    movl %edi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %edi
  push %esi
  call *%edi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.A.arr:
  .long vTable.Object.arr
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.A:
  .long vTable.Object
  .long decl.A.override
  .long decl.A.base
vTable.int:
  .long -1
vTable.B:
  .long vTable.A
  .long decl.B.override
  .long decl.A.base
  .long decl.B.sub
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.B.arr:
  .long vTable.A.arr
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
