# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class A {...}
    # Emitting void m(...) {...}
    # Declaration of method 'm'.
decl.A.m:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting b
      # Emitting value of var b
      mov 12(%ebp), %esi
      add $4, %esi
      movl 0(%esi), %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
ret.A.m:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $20, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var size
    mov %ebp, %edi
    add $-16, %edi
    movl $3, %esi
    mov %esi, (%edi)
    
    
    # Emitting assign
    # Emitting address of var i
    mov %ebp, %esi
    add $-20, %esi
    movl $0, %edi
    mov %edi, (%esi)
    
    
    # Emitting assign
    # Emitting address of var a
    mov %ebp, %edi
    add $-4, %edi
      # Emitting (size * 2)
        # Emitting 2
        movl $2, %esi
      push %esi
        # Emitting size
        # Emitting value of var size
        mov %ebp, %edx
        add $-16, %edx
        movl 0(%edx), %esi
      pop %edx
      imul %edx, %esi
      push %esi
label0:
      pop %esi
    cmp $0, %esi
    jge label2
    push $5
    call _exit
label2:
    push %esi
    add $2, %esi
    imul $4, %esi
    push %esi
    call _malloc
    add $4, %esp
    mov %eax, %edx
    pop %esi
    push %edx
    mov $vTable.A.arr, %ecx
    movl %ecx, 0(%edx)
    add $4, %edx
    mov %esi, (%edx)
    pop %edx
    mov %edx, (%edi)
    
label3:
      # Emitting (i < (size * 2))
        # Emitting (size * 2)
          # Emitting 2
          movl $2, %edx
        push %edx
          # Emitting size
          # Emitting value of var size
          mov %ebp, %edi
          add $-16, %edi
          movl 0(%edi), %edx
        pop %edi
        imul %edi, %edx
        push %edx
label7:
        pop %edx
      push %edx
        # Emitting i
        # Emitting value of var i
        mov %ebp, %edi
        add $-20, %edi
        movl 0(%edi), %edx
      pop %edi
      cmp %edi, %edx
      jl label10
      mov $0, %edx
      jmp label9
label10:
      mov $1, %edx
label9:
      push %edx
label5:
      pop %edx
    cmp $1, %edx
    jne label4
      # Emitting (...)
        # Emitting a[i] = new A()
        
        # Emitting assign
        # Emitting value of var a
        mov %ebp, %esi
        add $-4, %esi
        movl 0(%esi), %edi
        # Emitting value of var i
        mov %ebp, %ecx
        add $-20, %ecx
        movl 0(%ecx), %esi
        push %edi
        add $4, %edi
        cmp %esi, (%edi)
        jle label12
        cmp $0, %esi
        jge label11
label12:
        push $3
        call _exit
label11:
        pop %edi
        add $2, %esi
        imul $4, %esi
        add %esi, %edi
        push %edx
        push $8
        call _malloc
        add $4, %esp
        mov %eax, %esi
        mov $vTable.A, %ecx
        movl %ecx, 0(%eax)
        pop %edx
        mov %esi, (%edi)
        
        # Emitting a[i].b = i
        
        # Emitting assign
        
        # Emitting value of var a
        mov %ebp, %edi
        add $-4, %edi
        movl 0(%edi), %esi
        # Emitting value of var i
        mov %ebp, %ecx
        add $-20, %ecx
        movl 0(%ecx), %edi
        push %esi
        add $4, %esi
        cmp %edi, (%esi)
        jle label14
        cmp $0, %edi
        jge label13
label14:
        push $3
        call _exit
label13:
        pop %esi
        add $2, %edi
        imul $4, %edi
        add %edi, %esi
        movl 0(%esi), %esi
        cmp $0, %esi
        jne label15
        push $4
        call _exit
label15:
        add $4, %esi
        
        # Emitting value of var i
        mov %ebp, %ecx
        add $-20, %ecx
        movl 0(%ecx), %edi
        mov %edi, (%esi)
        
        # Emitting i = (i + 1)
        
        # Emitting assign
        # Emitting address of var i
        mov %ebp, %edi
        add $-20, %edi
          # Emitting 1
          movl $1, %esi
        push %esi
          # Emitting i
          # Emitting value of var i
          mov %ebp, %ecx
          add $-20, %ecx
          movl 0(%ecx), %esi
        pop %ecx
        add %ecx, %esi
        push %esi
label16:
        pop %esi
        mov %esi, (%edi)
        
    jmp label3
label4:
    
    # Emitting assign
    # Emitting address of var oa
    mov %ebp, %edx
    add $-12, %edx
    # Emitting value of var a
    mov %ebp, %edi
    add $-4, %edi
    movl 0(%edi), %esi
    mov %esi, (%edx)
    
    
    # Emitting assign
    # Emitting address of var b
    mov %ebp, %esi
    add $-8, %esi
    # Emitting value of var oa
    mov %ebp, %edi
    add $-12, %edi
    movl 0(%edi), %edx
    push %edx
    cmp $0, %edx
    je label19
    cmp $vTable.A.arr, %edx
    je label19
    cmp $-1, %edx
    je exit
label18:
    mov (%edx), %edx
    cmp $vTable.A.arr, %edx
    je label19
    cmp $-1, %edx
    jne label18
label20:
    push $1
    call _exit
label19:
    pop %edx
    mov %edx, (%esi)
    
    
    # Emitting assign
    # Emitting address of var i
    mov %ebp, %edx
    add $-20, %edx
    movl $0, %esi
    mov %esi, (%edx)
    
label21:
      # Emitting (i < (size * 2))
        # Emitting (size * 2)
          # Emitting 2
          movl $2, %esi
        push %esi
          # Emitting size
          # Emitting value of var size
          mov %ebp, %edx
          add $-16, %edx
          movl 0(%edx), %esi
        pop %edx
        imul %edx, %esi
        push %esi
label25:
        pop %esi
      push %esi
        # Emitting i
        # Emitting value of var i
        mov %ebp, %edx
        add $-20, %edx
        movl 0(%edx), %esi
      pop %edx
      cmp %edx, %esi
      jl label28
      mov $0, %esi
      jmp label27
label28:
      mov $1, %esi
label27:
      push %esi
label23:
      pop %esi
    cmp $1, %esi
    jne label22
      # Emitting (...)
        # Emitting b[i].m(...)
        # Emitting call to method m
        sub $4, %esp
          # Emitting b[i]
          # Emitting value of var b
          mov %ebp, %edi
          add $-8, %edi
          movl 0(%edi), %edx
          # Emitting value of var i
          mov %ebp, %ecx
          add $-20, %ecx
          movl 0(%ecx), %edi
          push %edx
          add $4, %edx
          cmp %edi, (%edx)
          jle label30
          cmp $0, %edi
          jge label29
label30:
          push $3
          call _exit
label29:
          pop %edx
          add $2, %edi
          imul $4, %edi
          add %edi, %edx
          movl 0(%edx), %edx
        cmp $0, %edx
        jne label31
        push $4
        call _exit
label31:
        push %edx
        movl 0(%edx), %edx
        movl 4(%edx), %edx
        call *%edx
        add $8, %esp
        # Emitting i = (i + 1)
        
        # Emitting assign
        # Emitting address of var i
        mov %ebp, %edx
        add $-20, %edx
          # Emitting 1
          movl $1, %edi
        push %edi
          # Emitting i
          # Emitting value of var i
          mov %ebp, %ecx
          add $-20, %ecx
          movl 0(%ecx), %edi
        pop %ecx
        add %ecx, %edi
        push %edi
label32:
        pop %edi
        mov %edi, (%edx)
        
    jmp label21
label22:
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $20, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.Main, %edi
    movl %edi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %edi
  push %esi
  call *%edi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.A.arr:
  .long vTable.Object.arr
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.A:
  .long vTable.Object
  .long decl.A.m
vTable.int:
  .long -1
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
