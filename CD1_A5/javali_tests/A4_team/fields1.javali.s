# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class A {...}
    # Emitting void run(...) {...}
    # Declaration of method 'run'.
decl.A.run:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    
    # Emitting value of var c
    mov 12(%ebp), %esi
    add $4, %esi
    movl 0(%esi), %edi
    cmp $0, %edi
    jne label0
    push $4
    call _exit
label0:
    add $4, %edi
    
      # Emitting 10
      movl $10, %esi
    push %esi
      # Emitting c.m
      
      # Emitting value of var c
      mov 12(%ebp), %edx
      add $4, %edx
      movl 0(%edx), %esi
      cmp $0, %esi
      jne label3
      push $4
      call _exit
label3:
      add $4, %esi
      movl 0(%esi), %esi
      
    pop %edx
    add %edx, %esi
    push %esi
label1:
    pop %esi
    mov %esi, (%edi)
    
ret.A.run:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
  # Emitting class B {...}
    # Emitting void run(...) {...}
    # Declaration of method 'run'.
decl.B.run:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    
    # Emitting value of var c
    mov 12(%ebp), %esi
    add $8, %esi
    movl 0(%esi), %edi
    cmp $0, %edi
    jne label4
    push $4
    call _exit
label4:
    add $4, %edi
    
      # Emitting 10
      movl $10, %esi
    push %esi
      # Emitting c.m
      
      # Emitting value of var c
      mov 12(%ebp), %edx
      add $8, %edx
      movl 0(%edx), %esi
      cmp $0, %esi
      jne label7
      push $4
      call _exit
label7:
      add $4, %esi
      movl 0(%esi), %esi
      
    pop %edx
    add %edx, %esi
    push %esi
label5:
    pop %esi
    mov %esi, (%edi)
    
ret.B.run:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $12, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var a
    mov %ebp, %edi
    add $-4, %edi
    push $8
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.A, %edx
    movl %edx, 0(%eax)
    mov %esi, (%edi)
    
    
    # Emitting assign
    # Emitting address of var b
    mov %ebp, %esi
    add $-8, %esi
    push $16
    call _malloc
    add $4, %esp
    mov %eax, %edi
    mov $vTable.B, %edx
    movl %edx, 0(%eax)
    mov %edi, (%esi)
    
    
    # Emitting assign
    
    # Emitting value of var b
    mov %ebp, %esi
    add $-8, %esi
    movl 0(%esi), %edi
    cmp $0, %edi
    jne label8
    push $4
    call _exit
label8:
    add $8, %edi
    
    push $8
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.D, %edx
    movl %edx, 0(%eax)
    mov %esi, (%edi)
    
    # Emitting call to method subRun
    sub $4, %esp
      # Emitting b.c
      
      # Emitting value of var b
      mov %ebp, %edi
      add $-8, %edi
      movl 0(%edi), %esi
      cmp $0, %esi
      jne label9
      push $4
      call _exit
label9:
      add $8, %esi
      movl 0(%esi), %esi
      
    cmp $0, %esi
    jne label10
    push $4
    call _exit
label10:
    push %esi
    movl 0(%esi), %esi
    movl 4(%esi), %esi
    call *%esi
    add $8, %esp
    # Emitting call to method run
    sub $4, %esp
      # Emitting b
      # Emitting value of var b
      mov %ebp, %edi
      add $-8, %edi
      movl 0(%edi), %esi
    cmp $0, %esi
    jne label11
    push $4
    call _exit
label11:
    push %esi
    movl 0(%esi), %esi
    movl 4(%esi), %esi
    call *%esi
    add $8, %esp
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting b.c.m
      
      
      # Emitting value of var b
      mov %ebp, %edi
      add $-8, %edi
      movl 0(%edi), %esi
      cmp $0, %esi
      jne label12
      push $4
      call _exit
label12:
      add $8, %esi
      movl 0(%esi), %esi
      
      cmp $0, %esi
      jne label13
      push $4
      call _exit
label13:
      add $4, %esi
      movl 0(%esi), %esi
      
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    
    # Emitting assign
    # Emitting address of var a
    mov %ebp, %esi
    add $-4, %esi
    # Emitting value of var b
    mov %ebp, %edx
    add $-8, %edx
    movl 0(%edx), %edi
    push %edi
    cmp $0, %edi
    je label15
    cmp $vTable.A, %edi
    je label15
    cmp $-1, %edi
    je exit
label14:
    mov (%edi), %edi
    cmp $vTable.A, %edi
    je label15
    cmp $-1, %edi
    jne label14
label16:
    push $1
    call _exit
label15:
    pop %edi
    mov %edi, (%esi)
    
    
    # Emitting assign
    
    # Emitting value of var a
    mov %ebp, %esi
    add $-4, %esi
    movl 0(%esi), %edi
    cmp $0, %edi
    jne label17
    push $4
    call _exit
label17:
    add $4, %edi
    
    
    # Emitting value of var b
    mov %ebp, %edx
    add $-8, %edx
    movl 0(%edx), %esi
    cmp $0, %esi
    jne label18
    push $4
    call _exit
label18:
    add $8, %esi
    movl 0(%esi), %esi
    
    push %esi
    cmp $0, %esi
    je label20
    cmp $vTable.C, %esi
    je label20
    cmp $-1, %esi
    je exit
label19:
    mov (%esi), %esi
    cmp $vTable.C, %esi
    je label20
    cmp $-1, %esi
    jne label19
label21:
    push $1
    call _exit
label20:
    pop %esi
    mov %esi, (%edi)
    
    # Emitting call to method subRun
    sub $4, %esp
      # Emitting a.c
      
      # Emitting value of var a
      mov %ebp, %edi
      add $-4, %edi
      movl 0(%edi), %esi
      cmp $0, %esi
      jne label22
      push $4
      call _exit
label22:
      add $4, %esi
      movl 0(%esi), %esi
      
    cmp $0, %esi
    jne label23
    push $4
    call _exit
label23:
    push %esi
    movl 0(%esi), %esi
    movl 4(%esi), %esi
    call *%esi
    add $8, %esp
    # Emitting call to method run
    sub $4, %esp
      # Emitting a
      # Emitting value of var a
      mov %ebp, %edi
      add $-4, %edi
      movl 0(%edi), %esi
    cmp $0, %esi
    jne label24
    push $4
    call _exit
label24:
    push %esi
    movl 0(%esi), %esi
    movl 4(%esi), %esi
    call *%esi
    add $8, %esp
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting a.c.m
      
      
      # Emitting value of var a
      mov %ebp, %edi
      add $-4, %edi
      movl 0(%edi), %esi
      cmp $0, %esi
      jne label25
      push $4
      call _exit
label25:
      add $4, %esi
      movl 0(%esi), %esi
      
      cmp $0, %esi
      jne label26
      push $4
      call _exit
label26:
      add $4, %esi
      movl 0(%esi), %esi
      
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $12, %esp
    pop %esp
    pop %ebp
    ret
    
  # Emitting class C {...}
    # Emitting void subRun(...) {...}
    # Declaration of method 'subRun'.
decl.C.subRun:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var m
    mov 12(%ebp), %edi
    add $4, %edi
    movl $0, %esi
    mov %esi, (%edi)
    
ret.C.subRun:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
  # Emitting class D {...}
    # Emitting void subRun(...) {...}
    # Declaration of method 'subRun'.
decl.D.subRun:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var m
    mov 12(%ebp), %edi
    add $4, %edi
    movl $10, %esi
    mov %esi, (%edi)
    
ret.D.subRun:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.Main, %edi
    movl %edi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %edi
  push %esi
  call *%edi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.D:
  .long vTable.C
  .long decl.D.subRun
vTable.Main.arr:
  .long vTable.Object.arr
vTable.A:
  .long vTable.Object
  .long decl.A.run
vTable.int:
  .long -1
vTable.B:
  .long vTable.A
  .long decl.B.run
vTable.float.arr:
  .long vTable.Object
vTable.C:
  .long vTable.Object
  .long decl.C.subRun
vTable.C.arr:
  .long vTable.Object.arr
vTable.B.arr:
  .long vTable.A.arr
vTable.float:
  .long -1
vTable.int.arr:
  .long vTable.Object
vTable.A.arr:
  .long vTable.Object.arr
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.boolean.arr:
  .long vTable.Object
vTable.D.arr:
  .long vTable.C.arr
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
