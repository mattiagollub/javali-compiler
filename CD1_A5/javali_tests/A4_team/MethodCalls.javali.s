# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class A {...}
    # Emitting int am(...) {...}
    # Declaration of method 'am'.
decl.A.am:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
    movl $1, %edi
    mov %ebp, %esi
    add $16, %esi
    mov %edi, (%esi)
    movl 0(%ebp), %esp
    add $-16, %esp
    jmp ret.A.am
ret.A.am:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
  # Emitting class B {...}
    # Emitting int[] bm(...) {...}
    # Declaration of method 'bm'.
decl.B.bm:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $8, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var i
    mov %ebp, %edi
    add $-8, %edi
    movl $0, %esi
    mov %esi, (%edi)
    
    
    # Emitting assign
    # Emitting address of var a
    mov %ebp, %esi
    add $-4, %esi
      # Emitting 5
      movl $5, %edi
    cmp $0, %edi
    jge label0
    push $5
    call _exit
label0:
    push %edi
    add $2, %edi
    imul $4, %edi
    push %edi
    call _malloc
    add $4, %esp
    mov %eax, %edx
    pop %edi
    push %edx
    mov $vTable.int.arr, %ecx
    movl %ecx, 0(%edx)
    add $4, %edx
    mov %edi, (%edx)
    pop %edx
    mov %edx, (%esi)
    
label1:
      # Emitting (i < 5)
        # Emitting 5
        movl $5, %edx
      push %edx
        # Emitting i
        # Emitting value of var i
        mov %ebp, %esi
        add $-8, %esi
        movl 0(%esi), %edx
      pop %esi
      cmp %esi, %edx
      jl label6
      mov $0, %edx
      jmp label5
label6:
      mov $1, %edx
label5:
      push %edx
label3:
      pop %edx
    cmp $1, %edx
    jne label2
      # Emitting (...)
        # Emitting a[i] = i
        
        # Emitting assign
        # Emitting value of var a
        mov %ebp, %edi
        add $-4, %edi
        movl 0(%edi), %esi
        # Emitting value of var i
        mov %ebp, %ecx
        add $-8, %ecx
        movl 0(%ecx), %edi
        push %esi
        add $4, %esi
        cmp %edi, (%esi)
        jle label8
        cmp $0, %edi
        jge label7
label8:
        push $3
        call _exit
label7:
        pop %esi
        add $2, %edi
        imul $4, %edi
        add %edi, %esi
        # Emitting value of var i
        mov %ebp, %ecx
        add $-8, %ecx
        movl 0(%ecx), %edi
        mov %edi, (%esi)
        
        # Emitting i = (i + 1)
        
        # Emitting assign
        # Emitting address of var i
        mov %ebp, %edi
        add $-8, %edi
          # Emitting 1
          movl $1, %esi
        push %esi
          # Emitting i
          # Emitting value of var i
          mov %ebp, %ecx
          add $-8, %ecx
          movl 0(%ecx), %esi
        pop %ecx
        add %ecx, %esi
        push %esi
label9:
        pop %esi
        mov %esi, (%edi)
        
    jmp label1
label2:
    # Emitting value of var a
    mov %ebp, %esi
    add $-4, %esi
    movl 0(%esi), %edx
    mov %ebp, %esi
    add $16, %esi
    mov %edx, (%esi)
    movl 0(%ebp), %esp
    add $-24, %esp
    jmp ret.B.bm
ret.B.bm:
    pop %esi
    pop %edi
    pop %ebx
    add $8, %esp
    pop %esp
    pop %ebp
    ret
    
  # Emitting class C {...}
    # Emitting B cm(...) {...}
    # Declaration of method 'cm'.
decl.C.cm:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $4, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var b
    mov %ebp, %edi
    add $-4, %edi
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.B, %edx
    movl %edx, 0(%eax)
    mov %esi, (%edi)
    
    # Emitting value of var b
    mov %ebp, %edi
    add $-4, %edi
    movl 0(%edi), %esi
    mov %ebp, %edi
    add $16, %edi
    mov %esi, (%edi)
    movl 0(%ebp), %esp
    add $-20, %esp
    jmp ret.C.cm
ret.C.cm:
    pop %esi
    pop %edi
    pop %ebx
    add $4, %esp
    pop %esp
    pop %ebp
    ret
    
    # Emitting C copyme(...) {...}
    # Declaration of method 'copyme'.
decl.C.copyme:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $4, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var c
    mov %ebp, %edi
    add $-4, %edi
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.C, %edx
    movl %edx, 0(%eax)
    mov %esi, (%edi)
    
    # Emitting value of var c
    mov %ebp, %edi
    add $-4, %edi
    movl 0(%edi), %esi
    mov %ebp, %edi
    add $16, %edi
    mov %esi, (%edi)
    movl 0(%ebp), %esp
    add $-20, %esp
    jmp ret.C.copyme
ret.C.copyme:
    pop %esi
    pop %edi
    pop %ebx
    add $4, %esp
    pop %esp
    pop %ebp
    ret
    
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $8, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var a
    mov %ebp, %edi
    add $-4, %edi
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.A, %edx
    movl %edx, 0(%eax)
    mov %esi, (%edi)
    
    
    # Emitting assign
    # Emitting address of var c
    mov %ebp, %esi
    add $-8, %esi
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %edi
    mov $vTable.C, %edx
    movl %edx, 0(%eax)
    mov %edi, (%esi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting c.cm(...).bm(...)[4]
      # Emitting call to method bm
      sub $4, %esp
        # Emitting c.cm(...)
        # Emitting call to method cm
        sub $4, %esp
          # Emitting c
          # Emitting value of var c
          mov %ebp, %esi
          add $-8, %esi
          movl 0(%esi), %edi
        cmp $0, %edi
        jne label13
        push $4
        call _exit
label13:
        push %edi
        movl 0(%edi), %edi
        movl 12(%edi), %edi
        call *%edi
        add $4, %esp
        pop %edi
      cmp $0, %edi
      jne label14
      push $4
      call _exit
label14:
      push %edi
      movl 0(%edi), %edi
      movl 8(%edi), %edi
      call *%edi
      add $4, %esp
      pop %edi
      movl $4, %esi
      push %edi
      add $4, %edi
      cmp %esi, (%edi)
      jle label12
      cmp $0, %esi
      jge label11
label12:
      push $3
      call _exit
label11:
      pop %edi
      add $2, %esi
      imul $4, %esi
      add %esi, %edi
      movl 0(%edi), %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting c.copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).am(...)
      # Emitting call to method am
      sub $4, %esp
        # Emitting c.copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...)
        # Emitting call to method copyme
        sub $4, %esp
          # Emitting c.copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...)
          # Emitting call to method copyme
          sub $4, %esp
            # Emitting c.copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...)
            # Emitting call to method copyme
            sub $4, %esp
              # Emitting c.copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...)
              # Emitting call to method copyme
              sub $4, %esp
                # Emitting c.copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...)
                # Emitting call to method copyme
                sub $4, %esp
                  # Emitting c.copyme(...).copyme(...).copyme(...).copyme(...).copyme(...).copyme(...)
                  # Emitting call to method copyme
                  sub $4, %esp
                    # Emitting c.copyme(...).copyme(...).copyme(...).copyme(...).copyme(...)
                    # Emitting call to method copyme
                    sub $4, %esp
                      # Emitting c.copyme(...).copyme(...).copyme(...).copyme(...)
                      # Emitting call to method copyme
                      sub $4, %esp
                        # Emitting c.copyme(...).copyme(...).copyme(...)
                        # Emitting call to method copyme
                        sub $4, %esp
                          # Emitting c.copyme(...).copyme(...)
                          # Emitting call to method copyme
                          sub $4, %esp
                            # Emitting c.copyme(...)
                            # Emitting call to method copyme
                            sub $4, %esp
                              # Emitting c
                              # Emitting value of var c
                              mov %ebp, %esi
                              add $-8, %esi
                              movl 0(%esi), %edi
                            cmp $0, %edi
                            jne label15
                            push $4
                            call _exit
label15:
                            push %edi
                            movl 0(%edi), %edi
                            movl 16(%edi), %edi
                            call *%edi
                            add $4, %esp
                            pop %edi
                          cmp $0, %edi
                          jne label16
                          push $4
                          call _exit
label16:
                          push %edi
                          movl 0(%edi), %edi
                          movl 16(%edi), %edi
                          call *%edi
                          add $4, %esp
                          pop %edi
                        cmp $0, %edi
                        jne label17
                        push $4
                        call _exit
label17:
                        push %edi
                        movl 0(%edi), %edi
                        movl 16(%edi), %edi
                        call *%edi
                        add $4, %esp
                        pop %edi
                      cmp $0, %edi
                      jne label18
                      push $4
                      call _exit
label18:
                      push %edi
                      movl 0(%edi), %edi
                      movl 16(%edi), %edi
                      call *%edi
                      add $4, %esp
                      pop %edi
                    cmp $0, %edi
                    jne label19
                    push $4
                    call _exit
label19:
                    push %edi
                    movl 0(%edi), %edi
                    movl 16(%edi), %edi
                    call *%edi
                    add $4, %esp
                    pop %edi
                  cmp $0, %edi
                  jne label20
                  push $4
                  call _exit
label20:
                  push %edi
                  movl 0(%edi), %edi
                  movl 16(%edi), %edi
                  call *%edi
                  add $4, %esp
                  pop %edi
                cmp $0, %edi
                jne label21
                push $4
                call _exit
label21:
                push %edi
                movl 0(%edi), %edi
                movl 16(%edi), %edi
                call *%edi
                add $4, %esp
                pop %edi
              cmp $0, %edi
              jne label22
              push $4
              call _exit
label22:
              push %edi
              movl 0(%edi), %edi
              movl 16(%edi), %edi
              call *%edi
              add $4, %esp
              pop %edi
            cmp $0, %edi
            jne label23
            push $4
            call _exit
label23:
            push %edi
            movl 0(%edi), %edi
            movl 16(%edi), %edi
            call *%edi
            add $4, %esp
            pop %edi
          cmp $0, %edi
          jne label24
          push $4
          call _exit
label24:
          push %edi
          movl 0(%edi), %edi
          movl 16(%edi), %edi
          call *%edi
          add $4, %esp
          pop %edi
        cmp $0, %edi
        jne label25
        push $4
        call _exit
label25:
        push %edi
        movl 0(%edi), %edi
        movl 16(%edi), %edi
        call *%edi
        add $4, %esp
        pop %edi
      cmp $0, %edi
      jne label26
      push $4
      call _exit
label26:
      push %edi
      movl 0(%edi), %edi
      movl 4(%edi), %edi
      call *%edi
      add $4, %esp
      pop %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $8, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %edi
    mov $vTable.Main, %esi
    movl %esi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %esi
  push %edi
  call *%esi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.Main.arr:
  .long vTable.Object.arr
vTable.A:
  .long vTable.Object
  .long decl.A.am
vTable.B:
  .long vTable.A
  .long decl.A.am
  .long decl.B.bm
vTable.int:
  .long -1
vTable.float.arr:
  .long vTable.Object
vTable.C:
  .long vTable.B
  .long decl.A.am
  .long decl.B.bm
  .long decl.C.cm
  .long decl.C.copyme
vTable.C.arr:
  .long vTable.B.arr
vTable.B.arr:
  .long vTable.A.arr
vTable.float:
  .long -1
vTable.int.arr:
  .long vTable.Object
vTable.A.arr:
  .long vTable.Object.arr
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.boolean.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
