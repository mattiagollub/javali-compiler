# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $16, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var f
    mov %ebp, %edi
    add $-12, %edi
    movl $1, %esi
    mov %esi, (%edi)
    
    
    # Emitting assign
    # Emitting address of var g
    mov %ebp, %esi
    add $-16, %esi
    movl $2, %edi
    mov %edi, (%esi)
    
    
    # Emitting assign
    # Emitting address of var a
    mov %ebp, %edi
    add $-4, %edi
    mov $1, %esi
    mov %esi, (%edi)
    
    
    # Emitting assign
    # Emitting address of var b
    mov %ebp, %esi
    add $-8, %esi
    mov $0, %edi
    mov %edi, (%esi)
    
      # Emitting true
      mov $1, %edi
    cmp $1, %edi
    jne label0
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
    jmp label1
label0:
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
label1:
      # Emitting (true <= true)
        # Emitting true
        mov $1, %edi
      push %edi
        # Emitting true
        mov $1, %edi
      pop %esi
      cmp %esi, %edi
      jle label5
      mov $0, %edi
      jmp label4
label5:
      mov $1, %edi
label4:
      push %edi
label2:
      pop %edi
    cmp $1, %edi
    jne label6
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
    jmp label7
label6:
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
label7:
      # Emitting (true > true)
        # Emitting true
        mov $1, %edi
      push %edi
        # Emitting true
        mov $1, %edi
      pop %esi
      cmp %esi, %edi
      jg label11
      mov $0, %edi
      jmp label10
label11:
      mov $1, %edi
label10:
      push %edi
label8:
      pop %edi
    cmp $1, %edi
    jne label12
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
    jmp label13
label12:
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
label13:
      # Emitting (true >= true)
        # Emitting true
        mov $1, %edi
      push %edi
        # Emitting true
        mov $1, %edi
      pop %esi
      cmp %esi, %edi
      jge label17
      mov $0, %edi
      jmp label16
label17:
      mov $1, %edi
label16:
      push %edi
label14:
      pop %edi
    cmp $1, %edi
    jne label18
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
    jmp label19
label18:
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
label19:
      # Emitting true
      mov $1, %edi
    cmp $1, %edi
    jne label20
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
    jmp label21
label20:
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
label21:
      # Emitting false
      mov $0, %edi
    cmp $1, %edi
    jne label22
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
    jmp label23
label22:
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
label23:
      # Emitting true
      mov $1, %edi
    cmp $1, %edi
    jne label24
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
    jmp label25
label24:
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
label25:
      # Emitting true
      mov $1, %edi
    cmp $1, %edi
    jne label26
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
    jmp label27
label26:
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
label27:
      # Emitting false
      mov $0, %edi
    cmp $1, %edi
    jne label28
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
    jmp label29
label28:
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
label29:
label30:
      # Emitting b
      # Emitting value of var b
      mov %ebp, %esi
      add $-8, %esi
      movl 0(%esi), %edi
    cmp $1, %edi
    jne label31
      # Emitting (...)
        # Emitting write(5)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 5
          movl $5, %esi
        push %esi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
        # Emitting b = !(b)
        
        # Emitting assign
        # Emitting address of var b
        mov %ebp, %esi
        add $-8, %esi
          # Emitting b
          # Emitting value of var b
          mov %ebp, %ecx
          add $-8, %ecx
          movl 0(%ecx), %edx
        negl %edx
        incl %edx
        mov %edx, (%esi)
        
    jmp label30
label31:
label32:
      # Emitting a
      # Emitting value of var a
      mov %ebp, %edx
      add $-4, %edx
      movl 0(%edx), %edi
    cmp $1, %edi
    jne label33
      # Emitting (...)
        # Emitting write(6)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 6
          movl $6, %edx
        push %edx
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
        # Emitting a = !(a)
        
        # Emitting assign
        # Emitting address of var a
        mov %ebp, %edx
        add $-4, %edx
          # Emitting a
          # Emitting value of var a
          mov %ebp, %ecx
          add $-4, %ecx
          movl 0(%ecx), %esi
        negl %esi
        incl %esi
        mov %esi, (%edx)
        
    jmp label32
label33:
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $16, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %edi
    mov $vTable.Main, %esi
    movl %esi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %esi
  push %edi
  call *%esi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.int:
  .long -1
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
