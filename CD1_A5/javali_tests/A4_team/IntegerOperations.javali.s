# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $12, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var a
    mov %ebp, %edi
    add $-4, %edi
    movl $23457, %esi
    mov %esi, (%edi)
    
    
    # Emitting assign
    # Emitting address of var b
    mov %ebp, %esi
    add $-8, %esi
    push %eax
    push %ebx
    push %edx
    push $scanfbuf
    push $int_format
    call _scanf
    add $8, %esp
    pop %edx
    pop %ebx
    pop %eax
    mov scanfbuf, %edi
    mov %edi, (%esi)
    
    
    # Emitting assign
    # Emitting address of var c
    mov %ebp, %edi
    add $-12, %edi
    push %eax
    push %ebx
    push %edx
    push $scanfbuf
    push $int_format
    call _scanf
    add $8, %esp
    pop %edx
    pop %ebx
    pop %eax
    mov scanfbuf, %esi
    mov %esi, (%edi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting (a + b)
        # Emitting b
        # Emitting value of var b
        mov %ebp, %edi
        add $-8, %edi
        movl 0(%edi), %esi
      push %esi
        # Emitting a
        # Emitting value of var a
        mov %ebp, %edi
        add $-4, %edi
        movl 0(%edi), %esi
      pop %edi
      add %edi, %esi
      push %esi
label0:
      pop %esi
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting (a - b)
        # Emitting b
        # Emitting value of var b
        mov %ebp, %edi
        add $-8, %edi
        movl 0(%edi), %esi
      push %esi
        # Emitting a
        # Emitting value of var a
        mov %ebp, %edi
        add $-4, %edi
        movl 0(%edi), %esi
      pop %edi
      sub %edi, %esi
      push %esi
label2:
      pop %esi
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting ((a * b) + c)
        # Emitting (a * b)
          # Emitting b
          # Emitting value of var b
          mov %ebp, %edi
          add $-8, %edi
          movl 0(%edi), %esi
        push %esi
          # Emitting a
          # Emitting value of var a
          mov %ebp, %edi
          add $-4, %edi
          movl 0(%edi), %esi
        pop %edi
        imul %edi, %esi
        push %esi
label6:
        pop %esi
      push %esi
        # Emitting c
        # Emitting value of var c
        mov %ebp, %edi
        add $-12, %edi
        movl 0(%edi), %esi
      pop %edi
      add %esi, %edi
      push %edi
label4:
      pop %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting ((a / b) - c)
        # Emitting (a / b)
          # Emitting b
          # Emitting value of var b
          mov %ebp, %esi
          add $-8, %esi
          movl 0(%esi), %edi
        push %edi
          # Emitting a
          # Emitting value of var a
          mov %ebp, %esi
          add $-4, %esi
          movl 0(%esi), %edi
        pop %esi
        push %eax
        push %ebx
        push %edx
        push %esi
        push %edi
        pop %eax
        pop %ebx
        cmp $0, %ebx
        jne label12
        push $8
        call _exit
label12:
        xor %edx, %edx
        idiv %ebx
        pop %edx
        pop %ebx
        mov %eax, %edi
        pop %eax
        push %edi
label10:
        pop %edi
      push %edi
        # Emitting c
        # Emitting value of var c
        mov %ebp, %esi
        add $-12, %esi
        movl 0(%esi), %edi
      pop %esi
      sub %edi, %esi
      push %esi
label8:
      pop %esi
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    push %eax
    push %ecx
    push %edx
    push $newline
    call _printf
    add $4, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting (a % b)
        # Emitting b
        # Emitting value of var b
        mov %ebp, %edi
        add $-8, %edi
        movl 0(%edi), %esi
      push %esi
        # Emitting a
        # Emitting value of var a
        mov %ebp, %edi
        add $-4, %edi
        movl 0(%edi), %esi
      pop %edi
      push %eax
      push %ebx
      push %edx
      push %edi
      push %esi
      pop %eax
      pop %ebx
      xor %edx, %edx
      idiv %ebx
      mov %edx, %esi
      pop %edx
      pop %ebx
      pop %eax
      push %esi
label13:
      pop %esi
    push %esi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $12, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %esi
    mov $vTable.Main, %edi
    movl %edi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %edi
  push %esi
  call *%edi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.int:
  .long -1
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
