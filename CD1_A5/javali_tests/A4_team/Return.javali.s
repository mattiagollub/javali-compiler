# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting this.test(...)
      # Emitting call to method test
      sub $4, %esp
        # Emitting 0
        movl $0, %edi
      push %edi
        # Emitting this
        mov 12(%ebp), %edi
      cmp $0, %edi
      jne label0
      push $4
      call _exit
label0:
      push %edi
      movl 0(%edi), %edi
      movl 4(%edi), %edi
      call *%edi
      add $8, %esp
      pop %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting this.test(...)
      # Emitting call to method test
      sub $4, %esp
        # Emitting 1
        movl $1, %edi
      push %edi
        # Emitting this
        mov 12(%ebp), %edi
      cmp $0, %edi
      jne label1
      push $4
      call _exit
label1:
      push %edi
      movl 0(%edi), %edi
      movl 4(%edi), %edi
      call *%edi
      add $8, %esp
      pop %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting this.test(...)
      # Emitting call to method test
      sub $4, %esp
        # Emitting 2
        movl $2, %edi
      push %edi
        # Emitting this
        mov 12(%ebp), %edi
      cmp $0, %edi
      jne label2
      push $4
      call _exit
label2:
      push %edi
      movl 0(%edi), %edi
      movl 4(%edi), %edi
      call *%edi
      add $8, %esp
      pop %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting this.test(...)
      # Emitting call to method test
      sub $4, %esp
        # Emitting 3
        movl $3, %edi
      push %edi
        # Emitting this
        mov 12(%ebp), %edi
      cmp $0, %edi
      jne label3
      push $4
      call _exit
label3:
      push %edi
      movl 0(%edi), %edi
      movl 4(%edi), %edi
      call *%edi
      add $8, %esp
      pop %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    # Emitting call to method testVoidRet
    sub $4, %esp
      # Emitting this
      mov 12(%ebp), %edi
    cmp $0, %edi
    jne label4
    push $4
    call _exit
label4:
    push %edi
    movl 0(%edi), %edi
    movl 8(%edi), %edi
    call *%edi
    add $8, %esp
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
    # Emitting int test(...) {...}
    # Declaration of method 'test'.
decl.Main.test:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
      # Emitting (n == 0)
        # Emitting 0
        movl $0, %edi
      push %edi
        # Emitting n
        # Emitting value of var n
        mov %ebp, %esi
        add $16, %esi
        movl 0(%esi), %edi
      pop %esi
      cmp %esi, %edi
      je label8
      mov $0, %edi
      jmp label7
label8:
      mov $1, %edi
label7:
      push %edi
label5:
      pop %edi
    cmp $1, %edi
    jne label9
      # Emitting (...)
        # Emitting return 9
        movl $9, %edi
        mov %ebp, %esi
        add $20, %esi
        mov %edi, (%esi)
        movl 0(%ebp), %esp
        add $-16, %esp
        jmp ret.Main.test
    jmp label10
label9:
      # Emitting (...)
        # Emitting if ((n == 1)) {...} else {...}
          # Emitting (n == 1)
            # Emitting 1
            movl $1, %esi
          push %esi
            # Emitting n
            # Emitting value of var n
            mov %ebp, %edi
            add $16, %edi
            movl 0(%edi), %esi
          pop %edi
          cmp %edi, %esi
          je label14
          mov $0, %esi
          jmp label13
label14:
          mov $1, %esi
label13:
          push %esi
label11:
          pop %esi
        cmp $1, %esi
        jne label15
          # Emitting (...)
            # Emitting return 8
            movl $8, %esi
            mov %ebp, %edi
            add $20, %edi
            mov %esi, (%edi)
            movl 0(%ebp), %esp
            add $-16, %esp
            jmp ret.Main.test
        jmp label16
label15:
          # Emitting nop
label16:
        # Emitting if ((n == 2)) {...} else {...}
          # Emitting (n == 2)
            # Emitting 2
            movl $2, %edi
          push %edi
            # Emitting n
            # Emitting value of var n
            mov %ebp, %esi
            add $16, %esi
            movl 0(%esi), %edi
          pop %esi
          cmp %esi, %edi
          je label20
          mov $0, %edi
          jmp label19
label20:
          mov $1, %edi
label19:
          push %edi
label17:
          pop %edi
        cmp $1, %edi
        jne label21
          # Emitting (...)
            # Emitting return 7
            movl $7, %edi
            mov %ebp, %esi
            add $20, %esi
            mov %edi, (%esi)
            movl 0(%ebp), %esp
            add $-16, %esp
            jmp ret.Main.test
        jmp label22
label21:
          # Emitting (...)
            # Emitting return 6
            movl $6, %esi
            mov %ebp, %edi
            add $20, %edi
            mov %esi, (%edi)
            movl 0(%ebp), %esp
            add $-16, %esp
            jmp ret.Main.test
label22:
label10:
ret.Main.test:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
    # Emitting void testVoidRet(...) {...}
    # Declaration of method 'testVoidRet'.
decl.Main.testVoidRet:
    push %ebp
    push %esp
    mov %esp, %ebp
    push %ebx
    push %edi
    push %esi
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting 5
      movl $5, %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
      # Emitting true
      mov $1, %edi
    cmp $1, %edi
    jne label23
      # Emitting (...)
        # Emitting return
        movl 0(%ebp), %esp
        add $-16, %esp
        jmp ret.Main.testVoidRet
    jmp label24
label23:
      # Emitting nop
label24:
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting 4
      movl $4, %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
ret.Main.testVoidRet:
    pop %esi
    pop %edi
    pop %ebx
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %edi
    mov $vTable.Main, %esi
    movl %esi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %esi
  push %edi
  call *%esi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.test
  .long decl.Main.testVoidRet
  .long decl.Main.main
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.int:
  .long -1
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
