# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $32, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var a
    mov %ebp, %edi
    add $-4, %edi
    push %eax
    push %ebx
    push %edx
    push $scanfbuf
    push $int_format
    call _scanf
    add $8, %esp
    pop %edx
    pop %ebx
    pop %eax
    mov scanfbuf, %esi
    mov %esi, (%edi)
    
    
    # Emitting assign
    # Emitting address of var b
    mov %ebp, %esi
    add $-8, %esi
    push %eax
    push %ebx
    push %edx
    push $scanfbuf
    push $int_format
    call _scanf
    add $8, %esp
    pop %edx
    pop %ebx
    pop %eax
    mov scanfbuf, %edi
    mov %edi, (%esi)
    
    
    # Emitting assign
    # Emitting address of var c
    mov %ebp, %edi
    add $-12, %edi
    push %eax
    push %ebx
    push %edx
    push $scanfbuf
    push $int_format
    call _scanf
    add $8, %esp
    pop %edx
    pop %ebx
    pop %eax
    mov scanfbuf, %esi
    mov %esi, (%edi)
    
    
    # Emitting assign
    # Emitting address of var g
    mov %ebp, %esi
    add $-28, %esi
    movl $0, %edi
    mov %edi, (%esi)
    
    
    # Emitting assign
    # Emitting address of var h
    mov %ebp, %edi
    add $-32, %edi
    movl $0, %esi
    mov %esi, (%edi)
    
      # Emitting (a == b)
        # Emitting b
        # Emitting value of var b
        mov %ebp, %edi
        add $-8, %edi
        movl 0(%edi), %esi
      push %esi
        # Emitting a
        # Emitting value of var a
        mov %ebp, %edi
        add $-4, %edi
        movl 0(%edi), %esi
      pop %edi
      cmp %edi, %esi
      je label3
      mov $0, %esi
      jmp label2
label3:
      mov $1, %esi
label2:
      push %esi
label0:
      pop %esi
    cmp $1, %esi
    jne label4
      # Emitting (...)
        # Emitting c = (c + 1)
        
        # Emitting assign
        # Emitting address of var c
        mov %ebp, %esi
        add $-12, %esi
          # Emitting 1
          movl $1, %edi
        push %edi
          # Emitting c
          # Emitting value of var c
          mov %ebp, %edx
          add $-12, %edx
          movl 0(%edx), %edi
        pop %edx
        add %edx, %edi
        push %edi
label6:
        pop %edi
        mov %edi, (%esi)
        
    jmp label5
label4:
      # Emitting (...)
        # Emitting write(1)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting 1
          movl $1, %edi
        push %edi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
label5:
label8:
      # Emitting (g < 5)
        # Emitting 5
        movl $5, %edi
      push %edi
        # Emitting g
        # Emitting value of var g
        mov %ebp, %esi
        add $-28, %esi
        movl 0(%esi), %edi
      pop %esi
      cmp %esi, %edi
      jl label13
      mov $0, %edi
      jmp label12
label13:
      mov $1, %edi
label12:
      push %edi
label10:
      pop %edi
    cmp $1, %edi
    jne label9
      # Emitting (...)
        # Emitting write(h)
        
        # Builtin Write
        push %eax
        push %ecx
        push %edx
          # Emitting h
          # Emitting value of var h
          mov %ebp, %edx
          add $-32, %edx
          movl 0(%edx), %esi
        push %esi
        push $int_format
        call _printf
        add $8, %esp
        pop %edx
        pop %ecx
        pop %eax
        
        # Emitting h = (h + 1)
        
        # Emitting assign
        # Emitting address of var h
        mov %ebp, %esi
        add $-32, %esi
          # Emitting 1
          movl $1, %edx
        push %edx
          # Emitting h
          # Emitting value of var h
          mov %ebp, %ecx
          add $-32, %ecx
          movl 0(%ecx), %edx
        pop %ecx
        add %ecx, %edx
        push %edx
label14:
        pop %edx
        mov %edx, (%esi)
        
        # Emitting g = (g + 1)
        
        # Emitting assign
        # Emitting address of var g
        mov %ebp, %edx
        add $-28, %edx
          # Emitting 1
          movl $1, %esi
        push %esi
          # Emitting g
          # Emitting value of var g
          mov %ebp, %ecx
          add $-28, %ecx
          movl 0(%ecx), %esi
        pop %ecx
        add %ecx, %esi
        push %esi
label16:
        pop %esi
        mov %esi, (%edx)
        
    jmp label8
label9:
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting c
      # Emitting value of var c
      mov %ebp, %esi
      add $-12, %esi
      movl 0(%esi), %edi
    push %edi
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $32, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %edi
    mov $vTable.Main, %esi
    movl %esi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %esi
  push %edi
  call *%esi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.int:
  .long -1
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
