# Emitting prologue
.extern _printf
.extern _scanf
.extern _malloc
.extern _exit
.global _main

.text
  # Emitting class InsertSort {...}
    # Emitting int[] sort(...) {...}
    # Declaration of method 'sort'.
decl.InsertSort.sort:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $12, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var ii
    mov %ebp, %edi
    add $-4, %edi
    movl $1, %esi
    mov %esi, (%edi)
    
label0:
      # Emitting (ii < length)
        # Emitting length
        # Emitting value of var length
        mov 12(%ebp), %edi
        add $4, %edi
        movl 0(%edi), %esi
      push %esi
        # Emitting ii
        # Emitting value of var ii
        mov %ebp, %edi
        add $-4, %edi
        movl 0(%edi), %esi
      pop %edi
      cmp %edi, %esi
      jl label5
      mov $0, %esi
      jmp label4
label5:
      mov $1, %esi
label4:
      push %esi
label2:
      pop %esi
    cmp $1, %esi
    jne label1
      # Emitting (...)
        # Emitting kk = ii
        
        # Emitting assign
        # Emitting address of var kk
        mov %ebp, %edi
        add $-8, %edi
        # Emitting value of var ii
        mov %ebp, %ecx
        add $-4, %ecx
        movl 0(%ecx), %edx
        mov %edx, (%edi)
        
        # Emitting while ((kk > 0)) {...}
label6:
          # Emitting (kk > 0)
            # Emitting 0
            movl $0, %edx
          push %edx
            # Emitting kk
            # Emitting value of var kk
            mov %ebp, %edi
            add $-8, %edi
            movl 0(%edi), %edx
          pop %edi
          cmp %edi, %edx
          jg label11
          mov $0, %edx
          jmp label10
label11:
          mov $1, %edx
label10:
          push %edx
label8:
          pop %edx
        cmp $1, %edx
        jne label7
          # Emitting (...)
            # Emitting if ((data[kk] < data[(kk - 1)])) {...} else {...}
              # Emitting (data[kk] < data[(kk - 1)])
                # Emitting data[(kk - 1)]
                # Emitting value of var data
                mov 12(%ebp), %ecx
                add $8, %ecx
                movl 0(%ecx), %edi
                  # Emitting 1
                  movl $1, %ecx
                push %ecx
                  # Emitting kk
                  # Emitting value of var kk
                  mov %ebp, %ebx
                  add $-8, %ebx
                  movl 0(%ebx), %ecx
                pop %ebx
                sub %ebx, %ecx
                push %ecx
label16:
                pop %ecx
                push %edi
                add $4, %edi
                cmp %ecx, (%edi)
                jle label15
                cmp $0, %ecx
                jge label14
label15:
                push $3
                call _exit
label14:
                pop %edi
                add $2, %ecx
                imul $4, %ecx
                add %ecx, %edi
                movl 0(%edi), %edi
              push %edi
                # Emitting data[kk]
                # Emitting value of var data
                mov 12(%ebp), %ecx
                add $8, %ecx
                movl 0(%ecx), %edi
                # Emitting value of var kk
                mov %ebp, %ebx
                add $-8, %ebx
                movl 0(%ebx), %ecx
                push %edi
                add $4, %edi
                cmp %ecx, (%edi)
                jle label19
                cmp $0, %ecx
                jge label18
label19:
                push $3
                call _exit
label18:
                pop %edi
                add $2, %ecx
                imul $4, %ecx
                add %ecx, %edi
                movl 0(%edi), %edi
              pop %ecx
              cmp %ecx, %edi
              jl label21
              mov $0, %edi
              jmp label20
label21:
              mov $1, %edi
label20:
              push %edi
label12:
              pop %edi
            cmp $1, %edi
            jne label22
              # Emitting (...)
                # Emitting tmp = data[kk]
                
                # Emitting assign
                # Emitting address of var tmp
                mov %ebp, %edi
                add $-12, %edi
                # Emitting value of var data
                mov 12(%ebp), %ebx
                add $8, %ebx
                movl 0(%ebx), %ecx
                # Emitting value of var kk
                mov %ebp, %eax
                add $-8, %eax
                movl 0(%eax), %ebx
                push %ecx
                add $4, %ecx
                cmp %ebx, (%ecx)
                jle label25
                cmp $0, %ebx
                jge label24
label25:
                push $3
                call _exit
label24:
                pop %ecx
                add $2, %ebx
                imul $4, %ebx
                add %ebx, %ecx
                movl 0(%ecx), %ecx
                mov %ecx, (%edi)
                
                # Emitting data[kk] = data[(kk - 1)]
                
                # Emitting assign
                # Emitting value of var data
                mov 12(%ebp), %edi
                add $8, %edi
                movl 0(%edi), %ecx
                # Emitting value of var kk
                mov %ebp, %ebx
                add $-8, %ebx
                movl 0(%ebx), %edi
                push %ecx
                add $4, %ecx
                cmp %edi, (%ecx)
                jle label27
                cmp $0, %edi
                jge label26
label27:
                push $3
                call _exit
label26:
                pop %ecx
                add $2, %edi
                imul $4, %edi
                add %edi, %ecx
                # Emitting value of var data
                mov 12(%ebp), %ebx
                add $8, %ebx
                movl 0(%ebx), %edi
                  # Emitting 1
                  movl $1, %ebx
                push %ebx
                  # Emitting kk
                  # Emitting value of var kk
                  mov %ebp, %eax
                  add $-8, %eax
                  movl 0(%eax), %ebx
                pop %eax
                sub %eax, %ebx
                push %ebx
label30:
                pop %ebx
                push %edi
                add $4, %edi
                cmp %ebx, (%edi)
                jle label29
                cmp $0, %ebx
                jge label28
label29:
                push $3
                call _exit
label28:
                pop %edi
                add $2, %ebx
                imul $4, %ebx
                add %ebx, %edi
                movl 0(%edi), %edi
                mov %edi, (%ecx)
                
                # Emitting data[(kk - 1)] = tmp
                
                # Emitting assign
                # Emitting value of var data
                mov 12(%ebp), %ecx
                add $8, %ecx
                movl 0(%ecx), %edi
                  # Emitting 1
                  movl $1, %ecx
                push %ecx
                  # Emitting kk
                  # Emitting value of var kk
                  mov %ebp, %ebx
                  add $-8, %ebx
                  movl 0(%ebx), %ecx
                pop %ebx
                sub %ebx, %ecx
                push %ecx
label34:
                pop %ecx
                push %edi
                add $4, %edi
                cmp %ecx, (%edi)
                jle label33
                cmp $0, %ecx
                jge label32
label33:
                push $3
                call _exit
label32:
                pop %edi
                add $2, %ecx
                imul $4, %ecx
                add %ecx, %edi
                # Emitting value of var tmp
                mov %ebp, %ebx
                add $-12, %ebx
                movl 0(%ebx), %ecx
                mov %ecx, (%edi)
                
                # Emitting kk = (kk - 1)
                
                # Emitting assign
                # Emitting address of var kk
                mov %ebp, %ecx
                add $-8, %ecx
                  # Emitting 1
                  movl $1, %edi
                push %edi
                  # Emitting kk
                  # Emitting value of var kk
                  mov %ebp, %ebx
                  add $-8, %ebx
                  movl 0(%ebx), %edi
                pop %ebx
                sub %ebx, %edi
                push %edi
label36:
                pop %edi
                mov %edi, (%ecx)
                
            jmp label23
label22:
              # Emitting nop
label23:
        jmp label6
label7:
        # Emitting ii = (ii + 1)
        
        # Emitting assign
        # Emitting address of var ii
        mov %ebp, %edx
        add $-4, %edx
          # Emitting 1
          movl $1, %edi
        push %edi
          # Emitting ii
          # Emitting value of var ii
          mov %ebp, %ecx
          add $-4, %ecx
          movl 0(%ecx), %edi
        pop %ecx
        add %ecx, %edi
        push %edi
label38:
        pop %edi
        mov %edi, (%edx)
        
    jmp label0
label1:
    # Emitting value of var data
    mov 12(%ebp), %edi
    add $8, %edi
    movl 0(%edi), %esi
    mov %ebp, %edi
    add $16, %edi
    mov %esi, (%edi)
    movl 0(%ebp), %esp
    add $-28, %esp
    jmp ret.InsertSort.sort
ret.InsertSort.sort:
    pop %esi
    pop %edi
    pop %ebx
    add $12, %esp
    pop %esp
    pop %ebp
    ret
    
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    # Declaration of method 'main'.
decl.Main.main:
    push %ebp
    push %esp
    mov %esp, %ebp
    sub $8, %esp
    push %ebx
    push %edi
    push %esi
    
    # Emitting assign
    # Emitting address of var data
    mov %ebp, %edi
    add $-4, %edi
      # Emitting 4
      movl $4, %esi
    cmp $0, %esi
    jge label40
    push $5
    call _exit
label40:
    push %esi
    add $2, %esi
    imul $4, %esi
    push %esi
    call _malloc
    add $4, %esp
    mov %eax, %edx
    pop %esi
    push %edx
    mov $vTable.int.arr, %ecx
    movl %ecx, 0(%edx)
    add $4, %edx
    mov %esi, (%edx)
    pop %edx
    mov %edx, (%edi)
    
    
    # Emitting assign
    # Emitting value of var data
    mov %ebp, %edi
    add $-4, %edi
    movl 0(%edi), %edx
    movl $0, %edi
    push %edx
    add $4, %edx
    cmp %edi, (%edx)
    jle label42
    cmp $0, %edi
    jge label41
label42:
    push $3
    call _exit
label41:
    pop %edx
    add $2, %edi
    imul $4, %edi
    add %edi, %edx
    movl $8, %edi
    mov %edi, (%edx)
    
    
    # Emitting assign
    # Emitting value of var data
    mov %ebp, %edx
    add $-4, %edx
    movl 0(%edx), %edi
    movl $1, %edx
    push %edi
    add $4, %edi
    cmp %edx, (%edi)
    jle label44
    cmp $0, %edx
    jge label43
label44:
    push $3
    call _exit
label43:
    pop %edi
    add $2, %edx
    imul $4, %edx
    add %edx, %edi
    movl $7, %edx
    mov %edx, (%edi)
    
    
    # Emitting assign
    # Emitting value of var data
    mov %ebp, %edi
    add $-4, %edi
    movl 0(%edi), %edx
    movl $2, %edi
    push %edx
    add $4, %edx
    cmp %edi, (%edx)
    jle label46
    cmp $0, %edi
    jge label45
label46:
    push $3
    call _exit
label45:
    pop %edx
    add $2, %edi
    imul $4, %edi
    add %edi, %edx
    movl $6, %edi
    mov %edi, (%edx)
    
    
    # Emitting assign
    # Emitting value of var data
    mov %ebp, %edx
    add $-4, %edx
    movl 0(%edx), %edi
    movl $3, %edx
    push %edi
    add $4, %edi
    cmp %edx, (%edi)
    jle label48
    cmp $0, %edx
    jge label47
label48:
    push $3
    call _exit
label47:
    pop %edi
    add $2, %edx
    imul $4, %edx
    add %edx, %edi
    movl $5, %edx
    mov %edx, (%edi)
    
    
    # Emitting assign
    # Emitting address of var is
    mov %ebp, %edx
    add $-8, %edx
    push %edx
    push $12
    call _malloc
    add $4, %esp
    mov %eax, %edi
    mov $vTable.InsertSort, %esi
    movl %esi, 0(%eax)
    pop %edx
    mov %edi, (%edx)
    
    
    # Emitting assign
    
    # Emitting value of var is
    mov %ebp, %edx
    add $-8, %edx
    movl 0(%edx), %edi
    cmp $0, %edi
    jne label49
    push $4
    call _exit
label49:
    add $8, %edi
    
    # Emitting value of var data
    mov %ebp, %esi
    add $-4, %esi
    movl 0(%esi), %edx
    mov %edx, (%edi)
    
    
    # Emitting assign
    
    # Emitting value of var is
    mov %ebp, %edi
    add $-8, %edi
    movl 0(%edi), %edx
    cmp $0, %edx
    jne label50
    push $4
    call _exit
label50:
    add $4, %edx
    
    movl $4, %edi
    mov %edi, (%edx)
    
    
    # Emitting assign
    # Emitting address of var data
    mov %ebp, %edi
    add $-4, %edi
    # Emitting call to method sort
    sub $4, %esp
      # Emitting is
      # Emitting value of var is
      mov %ebp, %esi
      add $-8, %esi
      movl 0(%esi), %edx
    cmp $0, %edx
    jne label51
    push $4
    call _exit
label51:
    push %edx
    movl 0(%edx), %edx
    movl 4(%edx), %edx
    call *%edx
    add $4, %esp
    pop %edx
    mov %edx, (%edi)
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting data[0]
      # Emitting value of var data
      mov %ebp, %edi
      add $-4, %edi
      movl 0(%edi), %edx
      movl $0, %edi
      push %edx
      add $4, %edx
      cmp %edi, (%edx)
      jle label53
      cmp $0, %edi
      jge label52
label53:
      push $3
      call _exit
label52:
      pop %edx
      add $2, %edi
      imul $4, %edi
      add %edi, %edx
      movl 0(%edx), %edx
    push %edx
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting data[1]
      # Emitting value of var data
      mov %ebp, %edi
      add $-4, %edi
      movl 0(%edi), %edx
      movl $1, %edi
      push %edx
      add $4, %edx
      cmp %edi, (%edx)
      jle label55
      cmp $0, %edi
      jge label54
label55:
      push $3
      call _exit
label54:
      pop %edx
      add $2, %edi
      imul $4, %edi
      add %edi, %edx
      movl 0(%edx), %edx
    push %edx
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting data[2]
      # Emitting value of var data
      mov %ebp, %edi
      add $-4, %edi
      movl 0(%edi), %edx
      movl $2, %edi
      push %edx
      add $4, %edx
      cmp %edi, (%edx)
      jle label57
      cmp $0, %edi
      jge label56
label57:
      push $3
      call _exit
label56:
      pop %edx
      add $2, %edi
      imul $4, %edi
      add %edi, %edx
      movl 0(%edx), %edx
    push %edx
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
    
    # Builtin Write
    push %eax
    push %ecx
    push %edx
      # Emitting data[3]
      # Emitting value of var data
      mov %ebp, %edi
      add $-4, %edi
      movl 0(%edi), %edx
      movl $3, %edi
      push %edx
      add $4, %edx
      cmp %edi, (%edx)
      jle label59
      cmp $0, %edi
      jge label58
label59:
      push $3
      call _exit
label58:
      pop %edx
      add $2, %edi
      imul $4, %edi
      add %edi, %edx
      movl 0(%edx), %edx
    push %edx
    push $int_format
    call _printf
    add $8, %esp
    pop %edx
    pop %ecx
    pop %eax
    
ret.Main.main:
    pop %esi
    pop %edi
    pop %ebx
    add $8, %esp
    pop %esp
    pop %ebp
    ret
    
# Emitting entry point
_main:
  push %esi
  push %edi
  push %ebx
  
  finit
  
    # Emitting new Main()
    push $4
    call _malloc
    add $4, %esp
    mov %eax, %edx
    mov $vTable.Main, %edi
    movl %edi, 0(%eax)
  
  sub $4, %esp
  # Call Main.main()
  mov $decl.Main.main, %edi
  push %edx
  call *%edi
  add $8, %esp
  
  pop %ebx
  pop %edi
  pop %esi
  xor %eax, %eax
  ret

.cstring
int_format:
  .asciz "%d"
flt_format:
  .asciz "%f"
newline:
  .asciz "\n"
scanfbuf:
  .long 0

# Emitting epilogue
vTable.int.arr:
  .long vTable.Object
vTable.Main:
  .long vTable.Object
  .long decl.Main.main
vTable.Main.arr:
  .long vTable.Object.arr
vTable.boolean.arr:
  .long vTable.Object
vTable.InsertSort.arr:
  .long vTable.Object.arr
vTable.int:
  .long -1
vTable.float.arr:
  .long vTable.Object
vTable.boolean:
  .long -1
vTable.Object.arr:
  .long -1
vTable.InsertSort:
  .long vTable.Object
  .long decl.InsertSort.sort
vTable.float:
  .long -1
vTable.Object:
  .long -1

# Emitting float constants
