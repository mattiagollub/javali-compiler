package cd.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.sun.org.apache.xml.internal.serializer.ToUnknownStream;

import cd.debug.AstDump;
import cd.ir.Ast;
import cd.ir.Ast.BinaryOp.BOp;
import cd.ir.Ast.Var;
import cd.ir.AstVisitor;
import cd.ir.ExprVisitor;
import cd.ir.Ast.Assign;
import cd.ir.Ast.BinaryOp;
import cd.ir.Ast.BooleanConst;
import cd.ir.Ast.BuiltInRead;
import cd.ir.Ast.BuiltInReadFloat;
import cd.ir.Ast.BuiltInWrite;
import cd.ir.Ast.BuiltInWriteFloat;
import cd.ir.Ast.BuiltInWriteln;
import cd.ir.Ast.Cast;
import cd.ir.Ast.ClassDecl;
import cd.ir.Ast.Field;
import cd.ir.Ast.FloatConst;
import cd.ir.Ast.IfElse;
import cd.ir.Ast.Index;
import cd.ir.Ast.IntConst;
import cd.ir.Ast.MethodCall;
import cd.ir.Ast.MethodCallExpr;
import cd.ir.Ast.MethodDecl;
import cd.ir.Ast.NewArray;
import cd.ir.Ast.NewObject;
import cd.ir.Ast.NullConst;
import cd.ir.Ast.ReturnStmt;
import cd.ir.Ast.ThisRef;
import cd.ir.Ast.UnaryOp;
import cd.ir.Ast.WhileLoop;
import cd.ir.Symbol.VariableSymbol.Kind;
import cd.util.Tuple;

public class DeadAssignments {
	
	protected final DeadAssignmentVisitor dav = new DeadAssignmentVisitor();
	
	public boolean go(List<? extends ClassDecl> astRoots) {
		
		// AST before optimization
		String predump = "";
		for (Ast ast : astRoots)
			predump += AstDump.toString(ast);
		
		boolean passChanged = true;
		while (passChanged) {
			String prepassDump = "";
			for (Ast ast : astRoots)
				prepassDump += AstDump.toString(ast);
			
			// Optimize
			for (ClassDecl ast : astRoots) {
				dav.optimize(ast);
			}
			
			String postpassDump = "";
			for (Ast ast : astRoots)
				postpassDump += AstDump.toString(ast);
			
			if (prepassDump.contentEquals(postpassDump))
				passChanged = false;
			else
				passChanged = true; // Content has changed
		}
		
		// AST after optimization
		String postdump = "";
		for (Ast ast : astRoots)
			postdump += AstDump.toString(ast);
		
		if (predump.contentEquals(postdump))
			return false;
		else
			return true; // Content has changed
	}
	
	protected class LiveVariableList {
		
		public Set<String> variables;
		
		LiveVariableList() {
			variables = new HashSet<String>();
		}
		
		public void add(String varName) {
			if (!variables.contains(varName))
				variables.add(varName);
		}
		
		public void remove(String varName) {
			if (variables.contains(varName))
				variables.remove(varName);
		}
		
		public Boolean isLive(String varName) {
			return variables.contains(varName);
		}
		
		public LiveVariableList clone() {
			LiveVariableList vars = new LiveVariableList();
			
			vars.variables.addAll(variables);
			
			return vars;
		}
		
		public LiveVariableList merge(LiveVariableList other) {
		
			for (String var : other.variables) {
				if (!isLive(var))
					variables.add(var);
			}
			
			return this;
		}
		
		public void dump() {
			System.out.println("DUMP:");
			
			for (String var : variables) {
				System.out.println(var);
			}
			
			System.out.println("ENDDUMP");
		}
	}
	
	protected class DeadAssignmentVisitor extends AstVisitor<Boolean, LiveVariableList> {
		
		public Boolean optimize(ClassDecl c) {
			return visit(c, null);
		}
		
		@Override
		public Boolean visit(Ast ast, LiveVariableList arg) {
			return super.visit(ast, arg);
		}
		
		@Override
		public Boolean classDecl(ClassDecl ast, LiveVariableList arg) {
			for (Ast m : ast.methods()) {
				visit(m, arg);
			}
			
			return false;
		}
		
		@Override
		public Boolean methodDecl(MethodDecl ast, LiveVariableList arg) {
			LiveVariableList liveVars = new LiveVariableList();
			
			// Update live variable list and remove unnecessary assignments
			for (int i = ast.body().children().size() - 1; i >= 0; i--) {				
				Ast stmt = ast.body().rwChildren().get(i);
				
				if (stmt != null) {
					Boolean remove = visit(stmt, liveVars);
				
					if (remove) {
						ast.body().rwChildren().remove(i);
					}
				}
			}
			
			return false;
		}
		
		@Override
		public Boolean assign(Assign ast, LiveVariableList arg) {
			
			if (ast.left() instanceof Var) {
				
				if (((Var)ast.left()).sym.kind == Kind.FIELD)
				{
					visit(ast.right(), arg);
					return false;
				}
				
				String varName = ((Var)ast.left()).name;
				LiveVariableList liveVars = arg.clone();
			
				if (!arg.isLive(varName)) {
					boolean canRemove = visit(ast.right(), liveVars);
					canRemove = visit(ast.left(), liveVars) && canRemove;
					
					if (canRemove)
					{
						return true;
					}
				}
				
				arg.remove(varName);
			}
			
			
			visit(ast.right(), arg);
			
			return false;
		}
		
		@Override
		public Boolean binaryOp(BinaryOp ast, LiveVariableList arg) {
			boolean removeLeft = visit(ast.left(), arg);
			boolean removeRight = visit(ast.right(), arg);
			
			return removeLeft
				&& removeRight
				&& !(ast.type.name.contentEquals("int") && ast.operator ==  BOp.B_DIV)
				&& !(ast.type.name.contentEquals("int") && ast.operator ==  BOp.B_MOD);
		}
		
		@Override
		public Boolean booleanConst(BooleanConst ast, LiveVariableList arg) {
			return true;
		}
		
		@Override
		public Boolean builtInRead(BuiltInRead ast, LiveVariableList arg) {
			return false;
		}
		
		@Override
		public Boolean builtInReadFloat(BuiltInReadFloat ast, LiveVariableList arg) {
			return false;
		}
		
		@Override
		public Boolean cast(Cast ast, LiveVariableList arg) {
			visit(ast.arg(), arg);
			return false;
		}
		
		@Override
		public Boolean index(Index ast, LiveVariableList arg) {
			visit(ast.left(), arg);
			visit(ast.right(), arg);
			return false;
		}
		
		@Override
		public Boolean intConst(IntConst ast, LiveVariableList arg) {
			return true;
		}
		
		@Override
		public Boolean floatConst(FloatConst ast, LiveVariableList arg) {
			return true;
		}
		
		@Override
		public Boolean field(Field ast, LiveVariableList arg) {
			visit(ast.arg(), arg);
			return false;
		}
		
		@Override
		public Boolean newArray(NewArray ast, LiveVariableList arg) {
			visit(ast.arg(), arg);
			return false;
		}
		
		@Override
		public Boolean newObject(NewObject ast, LiveVariableList arg) {
			return true;
		}
		
		@Override
		public Boolean nullConst(NullConst ast, LiveVariableList arg) {
			return true;
		}
		
		@Override
		public Boolean thisRef(ThisRef ast, LiveVariableList arg) { 
			return true;
		}
		
		@Override
		public Boolean methodCall(MethodCall ast, LiveVariableList arg) {
			visitChildren(ast, arg);
			return false;
		}
		
		@Override
		public Boolean methodCall(MethodCallExpr ast, LiveVariableList arg) {
			visitChildren(ast, arg);
			return false;
		}
		
		@Override
		public Boolean unaryOp(UnaryOp ast, LiveVariableList arg) {
			return visit(ast.arg(), arg);
		}
		
		@Override
		public Boolean var(Var ast, LiveVariableList arg) {
			arg.add(ast.name);
			return true;
		}
		
		@Override
		public Boolean ifElse(IfElse ast, LiveVariableList arg) {
			
			LiveVariableList ifList = arg;
			LiveVariableList elseList = arg.clone();
			
			// Update live variable list and remove unnecessary assignments
			// in then block
			for (int i = ast.then().children().size() - 1; i >= 0; i--) {				
				Ast stmt = ast.then().rwChildren.get(i);
				
				if (stmt != null) {
					Boolean remove = visit(stmt, ifList);
				
					if (remove) {
						ast.then().rwChildren.remove(i);
					}
				}
			}
			
			// Update live variable list and remove unnecessary assignments
			// in else block
			for (int i = ast.otherwise().children().size() - 1; i >= 0; i--) {				
				Ast stmt = ast.otherwise().rwChildren.get(i);
				
				if (stmt != null) {
					Boolean remove = visit(stmt, elseList);
							
					if (remove) {
						ast.otherwise().rwChildren.remove(i);
					}
				}
			}
			
			ifList.merge(elseList);
			
			visit(ast.condition(), arg);
			
			return false;
		}
		
		@Override
		public Boolean whileLoop(WhileLoop ast, LiveVariableList arg) {
			
			List<Tuple<Integer, Boolean>> removeCandidates = new ArrayList<Tuple<Integer, Boolean>>();
			
			visit(ast.condition(), arg);
			
			// Update live variable list and build remove candidates list
			for (int i = ast.body().children().size() - 1; i >= 0; i--) {				
				Ast stmt = ast.body().rwChildren.get(i);
				
				if (stmt != null) {
					Boolean remove = visit(stmt, arg);
				
					if (remove) {
						removeCandidates.add(new Tuple<Integer, Boolean>(i, true));
					}
				}
			}
			
			// Process remove candidates
			for (Tuple<Integer, Boolean> j : removeCandidates) {
				LiveVariableList dummy = arg.clone();
				String varName = ((Var)((Assign)(ast.body().rwChildren.get(j.a))).left()).name;
				dummy.remove(varName);
				
				for (Integer i = j.a - 1; i >= 0; i--) {
					visit(ast.body(), dummy);
					if (dummy.isLive(varName)) {
						j.b = false;
					}
				}
			}
			
			// Remove candidates
			for (Tuple<Integer, Boolean> i : removeCandidates) {
				if (i.b == true) {
					ast.body().rwChildren.remove(i.a.intValue());
				}
			}
			
			return false;
		}
		
		@Override
		public Boolean builtInWrite(BuiltInWrite ast, LiveVariableList arg) {
			visit(ast.arg(), arg);
			return false;
		}
		
		@Override
		public Boolean builtInWriteFloat(BuiltInWriteFloat ast, LiveVariableList arg) {
			visit(ast.arg(), arg);
			return false;
		}
		
		@Override
		public Boolean builtInWriteln(BuiltInWriteln ast, LiveVariableList arg) {
			return false;
		}
		
		@Override
		public Boolean returnStmt(ReturnStmt ast, LiveVariableList arg) {
			if (ast.arg() != null)
				visit(ast.arg(), arg);
			
			return false;
		}
	}
}
