package cd.codegen;

import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cd.Main;
import cd.ir.Ast;
import cd.ir.Ast.Assign;
import cd.ir.Ast.BinaryOp;
import cd.ir.Ast.BuiltInRead;
import cd.ir.Ast.BuiltInReadFloat;
import cd.ir.Ast.ClassDecl;
import cd.ir.Ast.Expr;
import cd.ir.Ast.FloatConst;
import cd.ir.Ast.IfElse;
import cd.ir.Ast.IntConst;
import cd.ir.Ast.BooleanConst;
import cd.ir.Ast.MethodDecl;
import cd.ir.Ast.UnaryOp;
import cd.ir.Ast.WhileLoop;
import cd.ir.Ast.UnaryOp.UOp;
import cd.ir.Ast.Var;
import cd.ir.Ast.VarDecl;
import cd.ir.AstVisitor;
import cd.ir.ExprVisitor;
import cd.ir.Symbol.ClassSymbol;
import cd.ir.Symbol.MethodSymbol;


public class ConstFolding {

	class CodegenArgs {
		HashMap<String, IntConst> constMap;
		HashMap<String, IntConst> toRemMap;
		boolean branch;
		boolean loop;
	}

	protected final StmtConstFolding scf = new StmtConstFolding();
	protected final ExprConstFolding ecf = new ExprConstFolding();

	public void go(List<? extends ClassDecl> astRoots) {
		for (ClassDecl ast : astRoots) {
			scf.gen(ast);
		}

	}

	
	/**
	 * Generates code to evaluate expressions. After emitting the code, returns
	 * a String which indicates the register where the result can be found.
	 */
	protected class ExprConstFolding extends ExprVisitor<Expr, CodegenArgs> {

		public Expr gen(Expr ast) {
			return visit(ast, new CodegenArgs());
		}

		@Override
		public Expr visit(Expr ast, CodegenArgs arg) {
			return super.visit(ast, arg);

		}
		
		@Override
		public Expr var(Var ast, CodegenArgs arg) {
			if (arg.toRemMap.containsKey(ast.name)) {
				arg.loop = true;
				return arg.toRemMap.get(ast.name);
			}
			if (arg.constMap.containsKey(ast.name)) {
				arg.loop = true;
				return arg.constMap.get(ast.name);
			}
			return ast;
		}
		
		@Override
		public Expr intConst(IntConst ast, CodegenArgs arg) {
			return ast;
		}
		
		@Override
		public Expr binaryOp(BinaryOp ast, CodegenArgs arg) {
			if ((ast.left() instanceof IntConst) && (ast.right() instanceof IntConst)) {
				arg.loop = true;
				switch(ast.operator) {
				case B_PLUS: 
					return new IntConst(((IntConst)ast.left()).value + ((IntConst)ast.right()).value);
				case B_MINUS:
					return new IntConst(((IntConst)ast.left()).value - ((IntConst)ast.right()).value);
				case B_TIMES:
					return new IntConst(((IntConst)ast.left()).value * ((IntConst)ast.right()).value);
				case B_DIV:
					return new IntConst(((IntConst)ast.left()).value / ((IntConst)ast.right()).value);
				case B_MOD:
					return new IntConst(((IntConst)ast.left()).value % ((IntConst)ast.right()).value);
				case B_EQUAL:
					return new BooleanConst(((IntConst)ast.left()).value == ((IntConst)ast.right()).value);
				case B_GREATER_OR_EQUAL:
					return new BooleanConst(((IntConst)ast.left()).value >= ((IntConst)ast.right()).value);
				case B_GREATER_THAN:
					return new BooleanConst(((IntConst)ast.left()).value > ((IntConst)ast.right()).value);
				case B_LESS_OR_EQUAL:
					return new BooleanConst(((IntConst)ast.left()).value <= ((IntConst)ast.right()).value);
				case B_LESS_THAN:
					return new BooleanConst(((IntConst)ast.left()).value < ((IntConst)ast.right()).value);
				case B_NOT_EQUAL:
					return new BooleanConst(((IntConst)ast.left()).value != ((IntConst)ast.right()).value);
				default: 
					arg.loop = false;
				}
			} else if ((ast.left() instanceof FloatConst) && (ast.right() instanceof FloatConst)) {
				arg.loop = true;
				switch(ast.operator) {
				case B_PLUS: 
					return new FloatConst(((FloatConst)ast.left()).value + ((FloatConst)ast.right()).value);
				case B_MINUS:
					return new FloatConst(((FloatConst)ast.left()).value - ((FloatConst)ast.right()).value);
				case B_TIMES:
					return new FloatConst(((FloatConst)ast.left()).value * ((FloatConst)ast.right()).value);
				case B_DIV:
					return new FloatConst(((FloatConst)ast.left()).value / ((FloatConst)ast.right()).value);
				case B_MOD:
					return new FloatConst(((FloatConst)ast.left()).value % ((FloatConst)ast.right()).value);
				case B_EQUAL:
					return new BooleanConst(((FloatConst)ast.left()).value == ((FloatConst)ast.right()).value);
				case B_GREATER_OR_EQUAL:
					return new BooleanConst(((FloatConst)ast.left()).value >= ((FloatConst)ast.right()).value);
				case B_GREATER_THAN:
					return new BooleanConst(((FloatConst)ast.left()).value > ((FloatConst)ast.right()).value);
				case B_LESS_OR_EQUAL:
					return new BooleanConst(((FloatConst)ast.left()).value <= ((FloatConst)ast.right()).value);
				case B_LESS_THAN:
					return new BooleanConst(((FloatConst)ast.left()).value < ((FloatConst)ast.right()).value);
				case B_NOT_EQUAL:
					return new BooleanConst(((FloatConst)ast.left()).value != ((FloatConst)ast.right()).value);
				default: 
					arg.loop = false;
				}
			} else if ((ast.left() instanceof BooleanConst) && (ast.right() instanceof BooleanConst)) {
				arg.loop = true;
				switch(ast.operator) {
				case B_AND:
					return new BooleanConst(((BooleanConst)ast.left()).value && ((BooleanConst)ast.right()).value);
				case B_OR:
					return new BooleanConst(((BooleanConst)ast.left()).value || ((BooleanConst)ast.right()).value);
				case B_EQUAL:
					return new BooleanConst(((BooleanConst)ast.left()).value == ((BooleanConst)ast.right()).value);
				case B_NOT_EQUAL:
					return new BooleanConst(((BooleanConst)ast.left()).value != ((BooleanConst)ast.right()).value);
				default:
					arg.loop = false;
				}
			} 
			ast.setLeft(visit(ast.left(), arg));
			ast.setRight(visit(ast.right(),arg));
			return ast;
		}
		
		@Override
		public Expr unaryOp(UnaryOp ast, CodegenArgs arg) {
			visit(ast.arg(), arg);
			if (ast.arg() instanceof IntConst) {
				arg.loop = true;
				if (ast.operator == UOp.U_MINUS) {
					return new IntConst(-((IntConst)ast.arg()).value);
				} else if (ast.operator == UOp.U_PLUS) {
					return new IntConst(((IntConst)ast.arg()).value);
				} else {
					arg.loop = false;
				}
			} else if (ast.arg() instanceof FloatConst) {
				arg.loop = true;
				if (ast.operator == UOp.U_MINUS) {
					return new FloatConst(-((FloatConst)ast.arg()).value);
				} else if (ast.operator == UOp.U_PLUS) {
					return new FloatConst(((FloatConst)ast.arg()).value);
				} else {
					arg.loop = false;
				}
			} else if (ast.arg() instanceof BooleanConst) {
				arg.loop = true;
				if (ast.operator == UOp.U_BOOL_NOT) {
					return new BooleanConst(!((BooleanConst)ast.arg()).value);
				} else {
					arg.loop = false;
				}
				
			}
			return ast;
				
			
		}
		
		@Override
		public Expr builtInRead(BuiltInRead ast, CodegenArgs arg) {
			return ast;
		}
		
		@Override
		public Expr builtInReadFloat(BuiltInReadFloat ast, CodegenArgs arg) {
			return ast;
		}

	}

	/**
	 * Generates code to process statements and declarations.
	 */
	public class StmtConstFolding extends AstVisitor<Ast, CodegenArgs> {

		public void gen(Ast ast) {
			CodegenArgs arg = new CodegenArgs();
			visit(ast, arg);
		}

		
		@Override
		public Ast visit(Ast ast, CodegenArgs arg) {
			return super.visit(ast, arg);
		}
		
		@Override
		public Ast classDecl(ClassDecl ast, CodegenArgs arg) {
			for (MethodDecl method : ast.methods()) {
				arg.loop = true;
				arg.branch = false;
				arg.constMap = new HashMap<String, IntConst>();
				while (arg.loop) {
					arg.loop = false;
					visit(method, arg);
				}

			}
			return null;
		}
		
		@Override
		public Ast methodDecl(MethodDecl ast, CodegenArgs arg) {
			
			for (Ast statement : ast.body().children()) {
				statement.accept(this, arg);
			}
			
			return null;
		}
		

		@Override
		public Ast assign(Assign ast, CodegenArgs arg) {
			ast.setRight(ecf.visit(ast.right(), arg));
			if (ast.left() instanceof Var) {
				if (ast.right() instanceof IntConst) {
					if (arg.branch) {
						arg.toRemMap.put(((Var)ast.left()).name, (IntConst)ast.right());
					} else {
						arg.constMap.put(((Var)ast.left()).name, (IntConst)ast.right());
					}
					
				} else {
					arg.constMap.remove(((Var)ast.left()).name);
				}
	
			}
			return null;
		}
		
		@Override
		public Ast ifElse(IfElse ast, CodegenArgs arg) {
			//ecf.visit(ast.condition(), arg);
			
			if (!arg.branch){
				HashMap<String, IntConst> tempMap = arg.constMap;
				arg.toRemMap = new HashMap<String, IntConst>();
				arg.branch = true;
				visit(ast.then(), arg);
				for (String key : arg.toRemMap.keySet()) {
					tempMap.remove(key);
				}
				arg.toRemMap = new HashMap<String, IntConst>();
				visit(ast.otherwise(), arg);
				for (String key : arg.toRemMap.keySet()) {
					tempMap.remove(key);
				}
				arg.branch = false;
				arg.constMap = tempMap;
				
			} else {
				HashMap<String, IntConst> tempMap = new HashMap<String, IntConst>();
				HashMap<String, IntConst> tempToRemMap = new HashMap<String, IntConst>();
				
				tempMap = arg.constMap;
				tempToRemMap = arg.toRemMap;
				
				arg.constMap.putAll(arg.toRemMap);
				arg.toRemMap = new HashMap<String, IntConst>();
				visit(ast.then(), arg);
				for (String key : arg.toRemMap.keySet()) {
					tempMap.remove(key);
					tempToRemMap.remove(key);
				}
				arg.toRemMap = new HashMap<String, IntConst>();
				visit(ast.then(), arg);
				for (String key : arg.toRemMap.keySet()) {
					tempMap.remove(key);
					tempToRemMap.remove(key);
				}
				arg.constMap = tempMap;
				arg.toRemMap = tempToRemMap;
			}

			return null;

		}
		
		@Override
		public Ast whileLoop(WhileLoop ast, CodegenArgs arg) {
			
			if (!arg.branch){
				HashMap<String, IntConst> tempMap = new HashMap<String, IntConst>();
				tempMap.putAll(arg.constMap);
				
				arg.toRemMap = new HashMap<String, IntConst>();
				arg.branch = true;
				List<Ast> stmts = new ArrayList<Ast>();
				for (Ast child : ast.body().children()) {
					stmts.add(child);
				}
				Ast body = new Ast.Seq(stmts);
				
				System.out.println("HE");
				System.out.println(body.rwChildren);
				visit(ast.body(), arg);
				System.out.println(body.rwChildren);
				
				
				HashMap<String, IntConst> firstMap = arg.toRemMap;
				arg.constMap = tempMap;
				arg.constMap.putAll(arg.toRemMap);
				arg.toRemMap = new HashMap<String, IntConst>();
				ast.setBody(body);
				
				visit(ast.body(), arg);
				HashMap<String, IntConst> secondMap = arg.toRemMap;

				
				
				for (String key : firstMap.keySet()) {
					if (secondMap.containsKey(key)) {
						arg.constMap.remove(key);
						arg.toRemMap.remove(key);
						ast.setBody(body);
					}
				}
				
				arg.branch = false;
				arg.constMap = tempMap;
				
			} else {
				HashMap<String, IntConst> tempMap = new HashMap<String, IntConst>();
				HashMap<String, IntConst> tempToRemMap = new HashMap<String, IntConst>();
				
				tempMap = arg.constMap;
				tempToRemMap = arg.toRemMap;
				
				arg.constMap.putAll(arg.toRemMap);
				arg.toRemMap = new HashMap<String, IntConst>();
				visit(ast.body(), arg);
				for (String key : arg.toRemMap.keySet()) {
					tempMap.remove(key);
					tempToRemMap.remove(key);
				}
				arg.constMap = tempMap;
				arg.toRemMap = tempToRemMap;
			}

			return null;
		}


	}

}
