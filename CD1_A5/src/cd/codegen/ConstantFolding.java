package cd.codegen;

import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cd.Main;
import cd.codegen.ConstFolding.CodegenArgs;
import cd.ir.Ast;
import cd.ir.Ast.Assign;
import cd.ir.Ast.BinaryOp;
import cd.ir.Ast.BuiltInRead;
import cd.ir.Ast.BuiltInReadFloat;
import cd.ir.Ast.BuiltInWrite;
import cd.ir.Ast.BuiltInWriteFloat;
import cd.ir.Ast.Cast;
import cd.ir.Ast.ClassDecl;
import cd.ir.Ast.Expr;
import cd.ir.Ast.Field;
import cd.ir.Ast.FloatConst;
import cd.ir.Ast.IfElse;
import cd.ir.Ast.Index;
import cd.ir.Ast.IntConst;
import cd.ir.Ast.BooleanConst;
import cd.ir.Ast.MethodCallExpr;
import cd.ir.Ast.MethodDecl;
import cd.ir.Ast.NewArray;
import cd.ir.Ast.NewObject;
import cd.ir.Ast.ThisRef;
import cd.ir.Ast.UnaryOp;
import cd.ir.Ast.WhileLoop;
import cd.ir.Ast.UnaryOp.UOp;
import cd.ir.Ast.Var;
import cd.ir.Ast.VarDecl;
import cd.ir.AstVisitor;
import cd.ir.ExprVisitor;
import cd.ir.Symbol;
import cd.ir.Symbol.ClassSymbol;
import cd.ir.Symbol.MethodSymbol;
import cd.util.Pair;
import cd.util.Tuple;


public class ConstantFolding {

	class CodegenArgs {
		HashMap<Integer, HashMap<String, Const>> transferMap;
		public int index;
		public int precessor;
		public boolean loop;
	}
	
	class Const {
		String type;
		String value;
		
		public Const(String type, String value) {
			this.type = type;
			this.value = value;
			
		}
		
		@Override
		public String toString() {
			return "TYPE: "+this.type+", VALUE: " + this.value;
		}
	}

	protected final StmtConstantFolding scf = new StmtConstantFolding();
	protected final ExprConstantFolding ecf = new ExprConstantFolding();
	
	
	public boolean go(List<? extends ClassDecl> astRoots) {
		boolean loop = false;
		for (ClassDecl ast : astRoots) {
			boolean classloop = scf.gen(ast);
			if (classloop) {
				loop = true;
			}
		}
		return loop;

	}

	
	/**
	 * Generates code to evaluate expressions. After emitting the code, returns
	 * a String which indicates the register where the result can be found.
	 */
	protected class ExprConstantFolding extends ExprVisitor<Expr, CodegenArgs> {

		public Expr gen(Expr ast) {
			return visit(ast, new CodegenArgs());
		}

		@Override
		public Expr visit(Expr ast, CodegenArgs arg) {
			return super.visit(ast, arg);

		}
		
		@Override
		public Expr var(Var ast, CodegenArgs arg) {
			if (arg.index > 0) {
				if (arg.transferMap.get(arg.index).containsKey(ast.name)) {
					if (!arg.transferMap.get(arg.index).get(ast.name).type.equals("TOP")) {
						arg.loop = true;
						switch (arg.transferMap.get(arg.index).get(ast.name).type) {
						case "int":
							IntConst result = new IntConst(Integer.valueOf(arg.transferMap.get(arg.index).get(ast.name).value));
							result.type = new Symbol.PrimitiveTypeSymbol("int");
							return result;

						case "float":
							FloatConst resultf = new FloatConst(Float.valueOf(arg.transferMap.get(arg.index).get(ast.name).value));
							resultf.type = new Symbol.PrimitiveTypeSymbol("float");
							return resultf;

						case "bool":
							BooleanConst resultb = new BooleanConst(Boolean.valueOf(arg.transferMap.get(arg.index).get(ast.name).value));
							resultb.type = new Symbol.PrimitiveTypeSymbol("bool");
							return resultb;

						}
						
					}
					
				} 
			}
			
			return ast;
		}
		
		@Override
		public Expr intConst(IntConst ast, CodegenArgs arg) {
			return ast;
		}
		
		@Override
		public Expr thisRef(ThisRef ast, CodegenArgs arg) {
			return ast;
		}
		
		@Override
		public Expr booleanConst(BooleanConst ast, CodegenArgs arg) {
			return ast;
		}
		
		@Override
		public Expr floatConst(FloatConst ast, CodegenArgs arg) {
			return ast;
		}
		
		@Override
		public Expr newArray(NewArray ast, CodegenArgs arg) {
			return ast;
		}
		
		@Override
		public Expr newObject(NewObject ast, CodegenArgs arg) {
			return ast;
		}
		
		@Override
		public Expr index(Index ast, CodegenArgs arg) {
			ast.setLeft(ast.left().accept(ecf, arg));
			ast.setRight(ast.right().accept(ecf, arg));
			return ast;
		}
		
		@Override
		public Expr binaryOp(BinaryOp ast, CodegenArgs arg) {
			boolean loop = arg.loop;
			arg.loop = true;
			if ((ast.left() instanceof IntConst) && (ast.right() instanceof IntConst)) {
				switch(ast.operator) {
				case B_PLUS: 
					IntConst a = new IntConst(((IntConst)ast.left()).value + ((IntConst)ast.right()).value);
					return a;
				case B_MINUS:
					return new IntConst(((IntConst)ast.left()).value - ((IntConst)ast.right()).value);
				case B_TIMES:
					return new IntConst(((IntConst)ast.left()).value * ((IntConst)ast.right()).value);
				case B_DIV:
					if (((IntConst)ast.right()).value != 0)
						return new IntConst(((IntConst)ast.left()).value / ((IntConst)ast.right()).value);
					else {
						arg.loop = loop;
						return ast;
					}
				case B_MOD:
					return new IntConst(((IntConst)ast.left()).value % ((IntConst)ast.right()).value);
				case B_EQUAL:
					return new BooleanConst(((IntConst)ast.left()).value == ((IntConst)ast.right()).value);
				case B_GREATER_OR_EQUAL:
					return new BooleanConst(((IntConst)ast.left()).value >= ((IntConst)ast.right()).value);
				case B_GREATER_THAN:
					return new BooleanConst(((IntConst)ast.left()).value > ((IntConst)ast.right()).value);
				case B_LESS_OR_EQUAL:
					return new BooleanConst(((IntConst)ast.left()).value <= ((IntConst)ast.right()).value);
				case B_LESS_THAN:
					return new BooleanConst(((IntConst)ast.left()).value < ((IntConst)ast.right()).value);
				case B_NOT_EQUAL:
					return new BooleanConst(((IntConst)ast.left()).value != ((IntConst)ast.right()).value);
				default: 
					arg.loop = loop;
				}
			} else if ((ast.left() instanceof FloatConst) && (ast.right() instanceof FloatConst)) {
				switch(ast.operator) {
				case B_PLUS: 
					return new FloatConst(((FloatConst)ast.left()).value + ((FloatConst)ast.right()).value);
				case B_MINUS:
					return new FloatConst(((FloatConst)ast.left()).value - ((FloatConst)ast.right()).value);
				case B_TIMES:
					return new FloatConst(((FloatConst)ast.left()).value * ((FloatConst)ast.right()).value);
				case B_DIV:
					return new FloatConst(((FloatConst)ast.left()).value / ((FloatConst)ast.right()).value);
				case B_MOD:
					return new FloatConst(((FloatConst)ast.left()).value % ((FloatConst)ast.right()).value);
				case B_EQUAL:
					return new BooleanConst(((FloatConst)ast.left()).value == ((FloatConst)ast.right()).value);
				case B_GREATER_OR_EQUAL:
					return new BooleanConst(((FloatConst)ast.left()).value >= ((FloatConst)ast.right()).value);
				case B_GREATER_THAN:
					return new BooleanConst(((FloatConst)ast.left()).value > ((FloatConst)ast.right()).value);
				case B_LESS_OR_EQUAL:
					return new BooleanConst(((FloatConst)ast.left()).value <= ((FloatConst)ast.right()).value);
				case B_LESS_THAN:
					return new BooleanConst(((FloatConst)ast.left()).value < ((FloatConst)ast.right()).value);
				case B_NOT_EQUAL:
					return new BooleanConst(((FloatConst)ast.left()).value != ((FloatConst)ast.right()).value);
				default: 
					arg.loop = loop;
				}
			} else if ((ast.left() instanceof BooleanConst) && (ast.right() instanceof BooleanConst)) {
				switch(ast.operator) {
				case B_AND:
					return new BooleanConst(((BooleanConst)ast.left()).value && ((BooleanConst)ast.right()).value);
				case B_OR:
					return new BooleanConst(((BooleanConst)ast.left()).value || ((BooleanConst)ast.right()).value);
				case B_EQUAL:
					return new BooleanConst(((BooleanConst)ast.left()).value == ((BooleanConst)ast.right()).value);
				case B_NOT_EQUAL:
					return new BooleanConst(((BooleanConst)ast.left()).value != ((BooleanConst)ast.right()).value);
				default:
					arg.loop = loop;
				}
			} 
			arg.loop = loop;
			ast.setLeft(visit(ast.left(), arg));
			ast.setRight(visit(ast.right(),arg));
			return ast;
		}
		
		
		@Override
		public Expr unaryOp(UnaryOp ast, CodegenArgs arg) {
			ast.setArg(visit(ast.arg(), arg));
			boolean loop = arg.loop;
			arg.loop = true;
			if (ast.arg() instanceof IntConst) {
				if (ast.operator == UOp.U_MINUS) {
					return new IntConst(-((IntConst)ast.arg()).value);
				} else if (ast.operator == UOp.U_PLUS) {
					return new IntConst(((IntConst)ast.arg()).value);
				} else {
					arg.loop = loop;
				}
			} else if (ast.arg() instanceof FloatConst) {
				if (ast.operator == UOp.U_MINUS) {
					return new FloatConst(-((FloatConst)ast.arg()).value);
				} else if (ast.operator == UOp.U_PLUS) {
					return new FloatConst(((FloatConst)ast.arg()).value);
				} else {
					arg.loop = loop;
				}
			} else if (ast.arg() instanceof BooleanConst) {
				if (ast.operator == UOp.U_BOOL_NOT) {
					return new BooleanConst(!((BooleanConst)ast.arg()).value);
				} else {
					arg.loop = loop;
				}
				
			}
			return ast;
				
			
		}
		
		@Override
		public Expr methodCall(MethodCallExpr ast, CodegenArgs arg) {
			return ast;
		}
		

		@Override
		public Expr cast(Cast ast, CodegenArgs arg) {
			return ast;
		}
	
		@Override
		public Expr builtInRead(BuiltInRead ast, CodegenArgs arg) {
			return ast;
		}

		
		@Override
		public Expr builtInReadFloat(BuiltInReadFloat ast, CodegenArgs arg) {
			return ast;
		}
		
		@Override
		public Expr field(Field ast, CodegenArgs arg) {
			ast.setArg(ast.arg().accept(ecf, arg));
			if (ast.arg() instanceof ThisRef) {
				if (arg.index > 0) {
					if (arg.transferMap.get(arg.index).containsKey(ast.fieldName)) {
						if (!arg.transferMap.get(arg.index).get(ast.fieldName).type.equals("TOP")) {
							arg.loop = true;
							switch (arg.transferMap.get(arg.index).get(ast.fieldName).type) {
							case "int":
								IntConst result = new IntConst(Integer.valueOf(arg.transferMap.get(arg.index).get(ast.fieldName).value));
								result.type = new Symbol.PrimitiveTypeSymbol("int");
								return result;

							case "float":
								FloatConst resultf = new FloatConst(Float.valueOf(arg.transferMap.get(arg.index).get(ast.fieldName).value));
								resultf.type = new Symbol.PrimitiveTypeSymbol("float");
								return resultf;

							case "bool":
								BooleanConst resultb = new BooleanConst(Boolean.valueOf(arg.transferMap.get(arg.index).get(ast.fieldName).value));
								resultb.type = new Symbol.PrimitiveTypeSymbol("bool");
								return resultb;

							}
							
						}
						
					} 
				}
				
				
			}
			return ast;
		}

	}

	/**
	 * Generates code to process statements and declarations.
	 */
	public class StmtConstantFolding extends AstVisitor<Ast, CodegenArgs> {

		public boolean gen(Ast ast) {
			CodegenArgs arg = new CodegenArgs();
			visit(ast, arg);
			return arg.loop;
		}

		
		@Override
		public Ast visit(Ast ast, CodegenArgs arg) {
			return super.visit(ast, arg);
		}
		
		@Override
		public Ast classDecl(ClassDecl ast, CodegenArgs arg) {
			boolean loop = false;
			for (MethodDecl method : ast.methods()) {
				arg.transferMap = new HashMap <Integer, HashMap<String, Const>>();
				visit(method, arg);
				if (arg.loop) {
					loop = true;
				}
			}
			arg.loop = loop;
			return null;
		}
		
		@Override
		public Ast methodDecl(MethodDecl ast, CodegenArgs arg) {
			boolean loop = false;
			arg.loop = true;
			while (arg.loop) {
				arg.index = 0;
				arg.loop = false;
				for (Ast statement : ast.body().children()) {
					statement.accept(this, arg);
				}
				if (arg.loop) {
					loop = true;
				}
				//System.out.println(arg.transferMap);
			}
			arg.loop = loop;
			
			
			return null;
		}
		

		@Override
		public Ast assign(Assign ast, CodegenArgs arg) {
			
			
			if (arg.transferMap.size() <= arg.index+1) {
				arg.transferMap.put(arg.index, new HashMap<String, Const>());
			}
			
			if (arg.index > 1) {

				HashMap<String, Const> a = arg.transferMap.get(arg.index);
				a.putAll(arg.transferMap.get(arg.precessor));
			}
			arg.index++;
			if (arg.transferMap.size() <= arg.index ) {
				arg.transferMap.put(arg.index, new HashMap<String, Const>());
				
			}
			if (arg.index > 1) {
				HashMap<String, Const> a = arg.transferMap.get(arg.index);
				a.putAll(arg.transferMap.get(arg.precessor));
			}
			
			if (ast.left() instanceof Index) {
				ast.setLeft(ecf.visit(ast.left(), arg));
			}
			if (ast.left() instanceof Field) {
				((Field)ast.left()).setArg(ecf.visit(((Field)ast.left()).arg(), arg));
			}
			ast.setRight(ecf.visit(ast.right(), arg));
			if (ast.left() instanceof Var) {
				if (ast.right() instanceof IntConst) {
					if (arg.transferMap.get(arg.index).containsKey(((Var)ast.left()).name)) {
						if (arg.transferMap.get(arg.index).get(((Var)ast.left()).name).type.equals("TOP")) {
							arg.loop = true;
							arg.transferMap.get(arg.index).put(((Var)ast.left()).name, new Const("int", String.valueOf(((IntConst)ast.right()).value)));
						} else {
							arg.transferMap.get(arg.index).put(((Var)ast.left()).name, new Const("int", String.valueOf(((IntConst)ast.right()).value)));
						}
						/*if (!String.valueOf(((IntConst)ast.right()).value).equals(arg.transferMap.get(arg.index).get(((Var)ast.left()).name))) {
							arg.loop = true;
							arg.transferMap.get(arg.index).put(((Var)ast.left()).name, String.valueOf(((IntConst)ast.right()).value));
						}*/
					} else {
						arg.loop = true;
						arg.transferMap.get(arg.index).put(((Var)ast.left()).name, new Const("int", String.valueOf(((IntConst)ast.right()).value)));
					}
				} else {
					if (ast.right() instanceof FloatConst) {
						if (arg.transferMap.get(arg.index).containsKey(((Var)ast.left()).name)) {
							if (arg.transferMap.get(arg.index).get(((Var)ast.left()).name).type.equals("TOP")) {
								arg.loop = true;
								arg.transferMap.get(arg.index).put(((Var)ast.left()).name, new Const("float", String.valueOf(((FloatConst)ast.right()).value)));
							} else {
								arg.transferMap.get(arg.index).put(((Var)ast.left()).name, new Const("float", String.valueOf(((FloatConst)ast.right()).value)));
							}
							/*if (!String.valueOf(((IntConst)ast.right()).value).equals(arg.transferMap.get(arg.index).get(((Var)ast.left()).name))) {
								arg.loop = true;
								arg.transferMap.get(arg.index).put(((Var)ast.left()).name, String.valueOf(((IntConst)ast.right()).value));
							}*/
						} else {
							arg.loop = true;
							arg.transferMap.get(arg.index).put(((Var)ast.left()).name, new Const("float", String.valueOf(((FloatConst)ast.right()).value)));
						}
					} else {
						if (ast.right() instanceof BooleanConst) {
							if (arg.transferMap.get(arg.index).containsKey(((Var)ast.left()).name)) {
								if (arg.transferMap.get(arg.index).get(((Var)ast.left()).name).type.equals("TOP")) {
									arg.loop = true;
									arg.transferMap.get(arg.index).put(((Var)ast.left()).name, new Const("bool", String.valueOf(((BooleanConst)ast.right()).value)));
								} else {
									arg.transferMap.get(arg.index).put(((Var)ast.left()).name, new Const("bool", String.valueOf(((BooleanConst)ast.right()).value)));
								}
								/*if (!String.valueOf(((IntConst)ast.right()).value).equals(arg.transferMap.get(arg.index).get(((Var)ast.left()).name))) {
									arg.loop = true;
									arg.transferMap.get(arg.index).put(((Var)ast.left()).name, String.valueOf(((IntConst)ast.right()).value));
								}*/
							} else {
								arg.loop = true;
								arg.transferMap.get(arg.index).put(((Var)ast.left()).name, new Const("bool", String.valueOf(((BooleanConst)ast.right()).value)));
							}
						} else {
							arg.transferMap.get(arg.index).put(((Var)ast.left()).name, new Const("TOP", null));
						}
					}
					
					
				}
			} else {
				if (ast.left() instanceof Field && ((Field)ast.left()).arg() instanceof ThisRef) {
					if (ast.right() instanceof IntConst) {
						if (arg.transferMap.get(arg.index).containsKey(((Field)ast.left()).fieldName)) {
							if (arg.transferMap.get(arg.index).get(((Field)ast.left()).fieldName).type.equals("TOP")) {
								arg.loop = true;
								arg.transferMap.get(arg.index).put(((Field)ast.left()).fieldName, new Const("int", String.valueOf(((IntConst)ast.right()).value)));
							} else {
								arg.transferMap.get(arg.index).put(((Field)ast.left()).fieldName, new Const("int", String.valueOf(((IntConst)ast.right()).value)));
							}
							/*if (!String.valueOf(((IntConst)ast.right()).value).equals(arg.transferMap.get(arg.index).get(((Var)ast.left()).name))) {
								arg.loop = true;
								arg.transferMap.get(arg.index).put(((Var)ast.left()).name, String.valueOf(((IntConst)ast.right()).value));
							}*/
						} else {
							arg.loop = true;
							arg.transferMap.get(arg.index).put(((Field)ast.left()).fieldName, new Const("int", String.valueOf(((IntConst)ast.right()).value)));
						}
					} else {
						if (ast.right() instanceof FloatConst) {
							if (arg.transferMap.get(arg.index).containsKey(((Field)ast.left()).fieldName)) {
								if (arg.transferMap.get(arg.index).get(((Field)ast.left()).fieldName).type.equals("TOP")) {
									arg.loop = true;
									arg.transferMap.get(arg.index).put(((Field)ast.left()).fieldName, new Const("float", String.valueOf(((FloatConst)ast.right()).value)));
								} else {
									arg.transferMap.get(arg.index).put(((Field)ast.left()).fieldName, new Const("float", String.valueOf(((FloatConst)ast.right()).value)));
								}
								/*if (!String.valueOf(((IntConst)ast.right()).value).equals(arg.transferMap.get(arg.index).get(((Var)ast.left()).name))) {
									arg.loop = true;
									arg.transferMap.get(arg.index).put(((Var)ast.left()).name, String.valueOf(((IntConst)ast.right()).value));
								}*/
							} else {
								arg.loop = true;
								arg.transferMap.get(arg.index).put(((Field)ast.left()).fieldName, new Const("float", String.valueOf(((FloatConst)ast.right()).value)));
							}
						} else {
							if (ast.right() instanceof BooleanConst) {
								if (arg.transferMap.get(arg.index).containsKey(((Field)ast.left()).fieldName)) {
									if (arg.transferMap.get(arg.index).get(((Field)ast.left()).fieldName).type.equals("TOP")) {
										arg.loop = true;
										arg.transferMap.get(arg.index).put(((Field)ast.left()).fieldName, new Const("bool", String.valueOf(((BooleanConst)ast.right()).value)));
									} else {
										arg.transferMap.get(arg.index).put(((Field)ast.left()).fieldName, new Const("bool", String.valueOf(((BooleanConst)ast.right()).value)));
									}
									/*if (!String.valueOf(((IntConst)ast.right()).value).equals(arg.transferMap.get(arg.index).get(((Var)ast.left()).name))) {
										arg.loop = true;
										arg.transferMap.get(arg.index).put(((Var)ast.left()).name, String.valueOf(((IntConst)ast.right()).value));
									}*/
								} else {
									arg.loop = true;
									arg.transferMap.get(arg.index).put(((Field)ast.left()).fieldName, new Const("bool", String.valueOf(((BooleanConst)ast.right()).value)));
								}
							} else {
								arg.transferMap.get(arg.index).put(((Field)ast.left()).fieldName, new Const("TOP", null));
							}
						}
						
						
					}
				} 
				
			}
			arg.precessor = arg.index;
			arg.index++;
			return null;
		}
		
		@Override
		public Ast ifElse(IfElse ast, CodegenArgs arg) {
			int otherwisepre = arg.precessor;
			arg.transferMap.put(arg.index, new HashMap<String, Const>());
			if (arg.index > 0) {
				arg.transferMap.get(arg.index).putAll(arg.transferMap.get(arg.precessor));
			}
			
			for (Ast statement : ast.then().children()) {
				statement.accept(this, arg);
			}
			int firstprecessor = arg.index - 1;
			arg.precessor = otherwisepre;
			for (Ast statement : ast.otherwise().children()) {
				statement.accept(this, arg);
			}
			int secondprecessor = arg.index - 1;
			HashMap<String, Const> first;
			if (firstprecessor >= 0) {
				 first = arg.transferMap.get(firstprecessor);
			} else {
				first = new HashMap<String, Const>();
			}
			HashMap<String, Const> second;
			if (secondprecessor >= 0) {
				second = arg.transferMap.get(secondprecessor);
			} else {
				second = new HashMap<String, Const>();
			}
			arg.transferMap.put(arg.index, new HashMap<String, Const>());
			Const result = new Const("TOP", null);
			for (String key: first.keySet()) {
				result = new Const("TOP", null);
				if (first.get(key).type.equals("TOP")) {
					result.type = "TOP";
				} else {
					if (second.containsKey(key)) {
						if (second.get(key).type.equals("TOP")) {
							result.type = "TOP";
						} else {
							if (first.get(key).value.equals(second.get(key).value)) {
								result.type = first.get(key).type;
								result.value = first.get(key).value;
							} else {
								result.type = "TOP";
							}
						}
					} else {
						result.type = first.get(key).type;
						result.value = first.get(key).value;
					}
				}
				arg.transferMap.get(arg.index).put(key, result);
				
			}
			
			for (String key: second.keySet()) {
				if (!first.containsKey(key)) {
					result = second.get(key);	
					arg.transferMap.get(arg.index).put(key, result);
				}
			}
			arg.precessor = arg.index;
			arg.index++;
			return null;
			
		}
		
		
		@Override
		public Ast whileLoop(WhileLoop ast, CodegenArgs arg) {
			return null;
			
			
		}
		
		@Override
		public Ast builtInWrite(BuiltInWrite ast, CodegenArgs arg) {
			arg.index--;
			ast.setArg(ast.arg().accept(ecf,arg));
			arg.index++;
			return ast;
		}
		
		@Override
		public Ast builtInWriteFloat(BuiltInWriteFloat ast, CodegenArgs arg) {
			arg.index--;
			ast.setArg(ast.arg().accept(ecf,arg));
			arg.index++;
			return ast;
		}
		
		


	}

}
