package cd.ir;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Symbol {
	
	public final String name;
	
	public static abstract class TypeSymbol extends Symbol {
		
		public TypeSymbol(String name) {
			super(name);
		}

		public abstract boolean isReferenceType();
		
		public String toString() {
			return name;
		}
		
	}
	
	public static class PrimitiveTypeSymbol extends TypeSymbol {
		public PrimitiveTypeSymbol(String name) {
			super(name);
		}		
		
		public boolean isReferenceType() {
			return false;
		}
	}
	
	public static class ArrayTypeSymbol extends TypeSymbol {
		public final TypeSymbol elementType;
		
		public ArrayTypeSymbol(TypeSymbol elementType) {
			super(elementType.name+"[]");
			this.elementType = elementType;
		}
		
		public boolean isReferenceType() {
			return true;
		}
	}
	
	public static class ClassSymbol extends TypeSymbol {
		public final Ast.ClassDecl ast;
		public ClassSymbol superClass;
		public final VariableSymbol thisSymbol =
			new VariableSymbol("this", this);
		public final Map<String, VariableSymbol> fields = 
			new HashMap<String, VariableSymbol>();
		public final Map<String, MethodSymbol> methods =
			new HashMap<String, MethodSymbol>();

		public int sizeInBytes;
		
		public ClassSymbol(Ast.ClassDecl ast) {
			super(ast.name);
			sizeInBytes = -1;
			this.ast = ast;
		}
		
		/** Used to create the default {@code Object} 
		 *  and {@code <null>} types */
		public ClassSymbol(String name) {
			super(name);
			sizeInBytes = -1;
			this.ast = null;
		}
		
		public boolean isReferenceType() {
			return true;
		}
		
		public VariableSymbol getField(String name) {
			VariableSymbol fsym = fields.get(name);
			if (fsym == null && superClass != null)
				return superClass.getField(name);
			return fsym;
		}
		
		public MethodSymbol getMethod(String name) {
			MethodSymbol msym = methods.get(name);
			if (msym == null && superClass != null)
				return superClass.getMethod(name);
			return msym;
		}
	}

	public static class MethodSymbol extends Symbol {
		
		public final Ast.MethodDecl ast;
		public final Map<String, VariableSymbol> locals =
			new HashMap<String, VariableSymbol>();
		public final List<VariableSymbol> parameters =
			new ArrayList<VariableSymbol>();
		public final HashMap<String, Integer> varOffsets = new HashMap<String, Integer>();
		public Integer returnOffset = new Integer(-1);
		public Integer returnSPOffset = new Integer(-1);
		
		public TypeSymbol returnType;
		public int offset;
		
		public MethodSymbol(Ast.MethodDecl ast) {
			super(ast.name);
			offset = -1;
			this.ast = ast;
		}
		
		public String toString() {
			return name + "(...)";
		}
		
	}
	
	public static class VariableSymbol extends Symbol {
		
		public static enum Kind { PARAM, LOCAL, FIELD };
		public final TypeSymbol type;
		public final Kind kind;
		public int offset;
		
		public VariableSymbol(String name, TypeSymbol type) {
			this(name, type, Kind.PARAM);
			offset = -1;
		}

		public VariableSymbol(String name, TypeSymbol type, Kind kind) {
			super(name);
			offset = -1;
			this.type = type;
			this.kind = kind;		
		}
		
		public String toString() {
			return name;
		}
	}

	protected Symbol(String name) {
		this.name = name;
	}

}
