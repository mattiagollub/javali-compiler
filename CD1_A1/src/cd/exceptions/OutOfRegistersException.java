package cd.exceptions;

public class OutOfRegistersException extends RuntimeException {
	private static final long serialVersionUID = 1269853489215673216L;
	
	public OutOfRegistersException() {
		super("Requested a free register but the register pool is empty."); 
	}
}
