package cd.codegen;

import java.util.LinkedList;

import cd.exceptions.OutOfRegistersException;

public final class RegisterPool {
	
	protected final LinkedList<String> freeRegisters = new LinkedList<>();
	protected final String[] gpr = {"%edi", "%esi", "%eax", "%ebx", "%ecx", "%edx"};
	
	public RegisterPool() {
		for (String reg:gpr) {
			freeRegisters.add(reg);
		}
	}
	
	/**
	 * Get a free register from the pool. Throws a {@link OutOfRegistersException}
	 * if all registers are in use.
	 * @return Name of a free register.
	 */
	public String Get()
	{
		if (freeRegisters.size() == 0)
			throw new OutOfRegistersException();
		
		return freeRegisters.removeLast();
	}
	
	/**
	 * Free a register. Throws if the name of the register is invalid or if the
	 * register is already free in the pool.
	 * @param reg Register to free.
	 */
	public void Free(String reg) {
		if (!isValidRegisterName(reg) && freeRegisters.contains(reg))
			throw new RuntimeException("Tried to free an invalid or already free register.");
		
		freeRegisters.add(reg);
	}
	
	/**
	 * Checks whether reg is a valid register name.
	 * @param reg Register name to check.
	 * @return True if the name is valid, false otherwise.
	 */
	private final Boolean isValidRegisterName(String reg) {
		Boolean valid = false;
		
		for (String r:gpr) {
			if (r.contentEquals(reg))
				valid = true;
		}
		
		return valid;
	}
}
