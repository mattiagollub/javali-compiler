package cd.codegen;

import cd.ir.Ast;
import cd.ir.Ast.Assign;
import cd.ir.Ast.BinaryOp;
import cd.ir.Ast.BooleanConst;
import cd.ir.Ast.BuiltInRead;
import cd.ir.Ast.BuiltInWrite;
import cd.ir.Ast.BuiltInWriteln;
import cd.ir.Ast.Cast;
import cd.ir.Ast.Expr;
import cd.ir.Ast.Field;
import cd.ir.Ast.IfElse;
import cd.ir.Ast.Index;
import cd.ir.Ast.IntConst;
import cd.ir.Ast.MethodCall;
import cd.ir.Ast.MethodDecl;
import cd.ir.Ast.NewArray;
import cd.ir.Ast.NewObject;
import cd.ir.Ast.NullConst;
import cd.ir.Ast.ThisRef;
import cd.ir.Ast.UnaryOp;
import cd.ir.Ast.Var;
import cd.ir.Ast.VarDecl;
import cd.ir.Ast.WhileLoop;
import cd.ir.AstVisitor;
import cd.ir.ExprVisitor;

/**
 * Generates code to process statements and declarations.
 */
public class StmtDeclRegisterCount extends AstVisitor<Integer, Void> {

	public ExprRegisterCount ec = new ExprRegisterCount();

	public void count(Ast ast) {
		visit(ast, null);
	}

	@Override
	public Integer visit(Ast ast, Void arg) {
		return super.visit(ast, arg);
	}

	@Override
	public Integer methodCall(MethodCall ast, Void dummy) {
		throw new RuntimeException("Not required");

	}

	@Override
	public Integer varDecl(VarDecl ast, Void arg) {
		ast.setRegCount(0);
		return 0;
	}

	@Override
	public Integer methodDecl(MethodDecl ast, Void arg) {
		visit(ast.body(), arg);
		ast.decls().accept(this, arg);
		return null;

	}

	@Override
	public Integer ifElse(IfElse ast, Void arg) {
		throw new RuntimeException("Not required");

	}

	@Override
	public Integer whileLoop(WhileLoop ast, Void arg) {

		throw new RuntimeException("Not required");

	}

	@Override
	public Integer assign(Assign ast, Void arg) {
		ast.left().accept(ec, arg);
		ast.right().accept(ec, arg);
		if (ast.left().getRegCount() == ast.right().getRegCount()) {
			ast.setRegCount(ast.left().getRegCount() + 1);
		} else {
			ast.setRegCount(Math.max(ast.left().getRegCount(), ast.right()
					.getRegCount()));
		}
		return ast.getRegCount();
	}

	@Override
	public Integer builtInWrite(BuiltInWrite ast, Void arg) {
		ast.arg().accept(ec, arg);
		ast.setRegCount(ast.arg().getRegCount());
		return ast.getRegCount();
	}

	@Override
	public Integer builtInWriteln(BuiltInWriteln ast, Void arg) {
		ast.setRegCount(0);
		return 0;
	}

	public class ExprRegisterCount extends ExprVisitor<Integer, Void> {

		public Integer count(Expr ast) {
			return visit(ast, null);
		}

		@Override
		public Integer visit(Expr ast, Void arg) {
			return super.visit(ast, null);
		}

		@Override
		public Integer binaryOp(BinaryOp ast, Void arg) {
			ast.left().accept(this, arg);
			ast.right().accept(this, arg);
			if (ast.left().getRegCount() == ast.right().getRegCount()) {
				ast.setRegCount(ast.left().getRegCount() + 1);
			} else {
				ast.setRegCount(Math.max(ast.left().getRegCount(), ast.right()
						.getRegCount()));
			}
			return ast.getRegCount();
		}

		@Override
		public Integer booleanConst(BooleanConst ast, Void arg) {
			throw new RuntimeException("Not required");
		}

		@Override
		public Integer builtInRead(BuiltInRead ast, Void arg) {
			return null;
		}

		@Override
		public Integer cast(Cast ast, Void arg) {
			throw new RuntimeException("Not required");
		}

		@Override
		public Integer index(Index ast, Void arg) {
			throw new RuntimeException("Not required");
		}

		@Override
		public Integer intConst(IntConst ast, Void arg) {
			ast.setRegCount(1);
			return 1;
		}

		@Override
		public Integer field(Field ast, Void arg) {
			throw new RuntimeException("Not required");
		}

		@Override
		public Integer newArray(NewArray ast, Void arg) {
			throw new RuntimeException("Not required");
		}

		@Override
		public Integer newObject(NewObject ast, Void arg) {
			throw new RuntimeException("Not required");
		}

		@Override
		public Integer nullConst(NullConst ast, Void arg) {
			throw new RuntimeException("Not required");
		}

		@Override
		public Integer thisRef(ThisRef ast, Void arg) {
			throw new RuntimeException("Not required");
		}

		@Override
		public Integer unaryOp(UnaryOp ast, Void arg) {
			ast.arg().accept(this, arg);
			ast.setRegCount(ast.arg().getRegCount());
			return ast.getRegCount();
		}

		@Override
		public Integer var(Var ast, Void arg) {
			ast.setRegCount(1);
			return 1;
		}

	}

}
